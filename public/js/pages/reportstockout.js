/**
 * Created by Jean Omar L. Ch. on 02/10/2018.
 */
var btns = [
    {
        extend: 'copyHtml5',
        text: "<i class='fa fa-files-o'></>",
        titleAttr: 'Copiar'
    },
    {
        extend: 'excelHtml5',
        text: "<i class='fa fa-file-excel-o'></>",
        titleAttr: 'Excel'
    },
    {
        extend: 'csvHtml5',
        text: "<i class='fa fa-table'></>",
        titleAttr: 'CSV'
    },
    {
        extend: 'pdfHtml5',
        text: "<i class='fa fa-file-pdf-o'></>",
        titleAttr: 'PDF'
    }
];

var cols =  [
        {"data": "tanggal"},
        {"data": "state"},
        {"data": "kode"},
        {"data": "nama"},
        {"data": "detail"},
        {"data": "stok"},
        {"data": "satuan"}
];

var table = $("#table").DataTable({
    "processing": true,
    //"serverSide": true,
    "paging": true,
    "deferRender": true,
    "lengthChange": false,
    "searching": false,
    "ordering": false,
    "info": true,
    "autoWidth": true,
    "pageLength": 30,
    "ajax": {
        "url" : "/report/stockout/data-report",
        "dataSrc" : "data"
    },
    createdRow:function(row, data, dataindex){
        if (data.state=="Anulado") {
            $(row).css({'color':"red"});
        }
    },
    "columns" : cols
});


function onClickButton(){

    table.destroy();
    
    table = $('#table').DataTable({
        dom: 'Bfrtip',
        "processing": true,
        //"serverSide": true,
        "paging": true,
        "deferRender": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "pageLength": 30,
        "ajax": {
            "url"    : "/report/stockout/data-report",
            "dataSrc": "data",
            "type"   : "GET",
            "data": function (row, type, val, meta ) {
                row._token = $('input[name=_token]').val();
                row.tanggal = $('input[name=tanggal]').val();
                row.produk = $('input[name=produk]').val();
                row.detail = $('input[name=detail]').val();
                row.kode = $('input[name=kode]').val();
            }
        },
        "columns" : cols,
        buttons: btns
    });
    
    table.buttons().container().appendTo( '#table_wrapper .col-sm-6:eq(0)' );
    $('table#table tbody tr  td.details-control').css('padding', '0px');

}

$(function(){

    new $.fn.dataTable.Buttons( table, {
        buttons: btns
    } );
    table.buttons().container().appendTo( '#table_wrapper .col-sm-6:eq(0)' );
    $('table#table tbody tr  td.details-control').css('padding', '0px');
    $('#table_filter label').addClass('pull-right');

    //var date = moment().format()
    $('input[name=tanggal]').daterangepicker({
        "autoUpdateInput" : false,
        "startDate"       : moment().subtract(1, 'month'),
        "endDate"         : moment(),
        "maxDate"         : moment(),
        locale: {
            format: 'DD/MM/YYYY',
            cancelLabel: 'Limpiar',
            applyLabel: 'Aceptar',
            //firstDay: 1,
            daysOfWeek: [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            monthNames:[
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Setiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ]
        }
    });

    $('input[name="tanggal"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD') + '#' + picker.endDate.format('YYYY-MM-DD'));
    });

    $('input[name="tanggal"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $("#btn-search").on("click", function(e){
        onClickButton();
    });
});