/**
 * Created by Jean Omar L. Ch. on 02/10/2018.
 */

var btns = [
        {
            extend: 'copyHtml5',
            text: "<i class='fa fa-files-o'></>",
            titleAttr: 'Copiar'
        },
        {
            extend: 'excelHtml5',
            text: "<i class='fa fa-file-excel-o'></>",
            titleAttr: 'Excel'
        },
        {
            extend: 'csvHtml5',
            text: "<i class='fa fa-table'></>",
            titleAttr: 'CSV'
        },
        {
            extend: 'pdfHtml5',
            text: "<i class='fa fa-file-pdf-o'></>",
            titleAttr: 'PDF'
        }
    ];

var cols = [
        {
            "class":          "details-control",
            "orderable":      false,
            "data":           null,
            "defaultContent": "<button title='Ver' class='btn btn-info btn-xs'><i class='fa fa-plus-circle'></i></button>"
        },
        {"data": "kode", "name" : "kode"},
        {"data": "state_tran", "name" : "state_tran"},
        {"data": "tanggal", "name" : "tanggal"},
        {"data": "sub_harga", "name" : "sub_harga"},
        {"data": "pelanggan", "name" : "pelanggan"},
        {"data": "kasir", "name" : "kasir"},
        {"data": "action"}
    ];

var table = $("#table").DataTable({
    //dom: 'Bfrtip',
    "processing": true,
    "paging": true,
    "deferRender": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "pageLength": 30,
    "ajax": {
        "url"    : "/report/sales/data-report",
        "dataSrc": "data",
        "type"   : "POST",
        "data": function (row, type, val, meta ) {
            row._token = $('input[name=_token]').val();
        }
    },
    createdRow:function(row, data, dataindex){
        if (data.state_tran=="IN") {
            $(row).css({'color':"red"});
        }
    },
    "columns" : cols
    /*,
    buttons: [
        'copy', 'excel', 'pdf',
        {
            extend: 'pdf',
            text: 'Guardar página actual',
            exportOptions: {
                modifier: {
                    page: 'current'
                }
            }
        }
    ]*/

});
//table.buttons().container().appendTo( '#table_wrapper .col-sm-6:eq(0)' );
function onClickButton(){
    //console.log($('input[name=tanggal]').val());
    table.destroy();
    //$('#table tbody').empty();
    table = $('#table').DataTable({
        dom: 'Bfrtip',
        "processing": true,
        //"serverSide": true,
        "paging": true,
        "deferRender": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "pageLength": 30,
        "ajax": {
            "url"    : "/report/sales/data-report",
            "dataSrc": "data",
            "type"   : "POST",
            "data": function (row, type, val, meta ) {
                row._token = $('input[name=_token]').val();
                row.tanggal = $('input[name=tanggal]').val();
                row.cliente = $('input[name=cliente]').val();
                row.user = $('input[name=cajero]').val();
                row.transac = $('input[name=nota]').val();
            }
        },
        createdRow:function(row, data, dataindex){
            if (data.state_tran=="IN") {
                $(row).css({'color':"red"});
            }
        },
        "columns" : cols,
        buttons: btns
    });
    
    table.buttons().container().appendTo( '#table_wrapper .col-sm-6:eq(0)' );
    $('table#table tbody tr  td.details-control').css('padding', '0px');
/*
    var detailRows = [];
    $('#table tbody').on( 'click', 'tr td.details-control', function () {

        var tr = $(this).closest('tr');
        var row = table.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );

        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            tr.find('button').removeClass('btn-danger');
            tr.find('button').addClass('btn-info');
            tr.find('i').removeClass('fa-minus-circle');
            tr.find('i').addClass('fa-plus-circle');
            row.child.hide();

            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );

            tr.find('button').removeClass('btn-info');
            tr.find('button').addClass('btn-danger');
            tr.find('i').removeClass('fa-plus-circle');
            tr.find('i').addClass('fa-minus-circle');

            row.child( format( row.data() ) ).show();

            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
            $(this).parent('tr').next('tr').children('td').css('padding', '0px');
        }
    });

    table.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );*/
}
function format(d){
    var items = '';
    $.each(d.items.data, function(key, val){
        //items.push("<tr><td>"+val.nama_item+"</td><td>"+val.harga_item+"</td><td>"+val.qty_item+"</td></tr>");
        items += "<tr><td>"+val.nama_item+"</td><td>"+val.harga_item+"</td><td>"+val.qty_item+"</td></tr>";
    });
    ///console.log(items);
    return '<table class="table table-hover" style="margin: 0px">' +
        '<thead><tr><th>Producto</th><th>Precio unitario</th><th>Cantidad</th></tr></thead>' +
        '<tbody>' +
            items +
        '</tbody>' +
        '</table>';
}

var formdelete = "" +
    "<div class='modal-body'>"+
    "<input type='hidden' name='id'>"+
    "<input type='hidden' name='total'>"+
    "<input type='hidden' name='kode'>"+
    "<div class='form-group'>"+
    "<table class='table table-bordered'>"+
    "<thead>"+
    "<tr>"+
    "<td>Código transacción</td>"+
    "<td>Total</td>"+
    "</tr>"+
    "</thead>"+
    "<tbody>"+
    "<tr>"+
    "<td data='kode'></td>"+
    "<td data='total'></td>"+
    "</tr>"+
    "</tbody>"+
    "</table>"+
    "</dib>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>P. U. Venta S/.</label>"+
    "<div class='col-sm-9'>"+
    "<select class='form-control' name='motiv'>"+
    "<option val='' disabled selected>seleccione un motivo</option>"+
    "<option val='vencido'>Vencido</option>"+
    "<option val='equivocacion'>Equivocación</option>"+
    "<option val='otro'>otro</option>"+
    "</select>"+
    "</div>"+
    "</div>"+
    "<br>"+
    "</div>"+
    "<div class='modal-footer'>"+
    "<input type='hidden' name='kode'>"+
    "<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>"+
    "<button type='button' class='btn btn-flat bg-aqua' id='btnSimpan'>Anular</button>"+
    "</div>";

$(function(){
    new $.fn.dataTable.Buttons( table, {
        buttons: btns
    } );
    table.buttons().container().appendTo( '#table_wrapper .col-sm-6:eq(0)' );
    $('table#table tbody tr  td.details-control').css('padding', '0px');
    
    var detailRows = [];
    $('#table tbody').on( 'click', 'tr td.details-control', function () {

        var tr = $(this).closest('tr');
        var row = table.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );

        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            tr.find('button').removeClass('btn-danger');
            tr.find('button').addClass('btn-info');
            tr.find('i').removeClass('fa-minus-circle');
            tr.find('i').addClass('fa-plus-circle');
            row.child.hide();

            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );

            tr.find('button').removeClass('btn-info');
            tr.find('button').addClass('btn-danger');
            tr.find('i').removeClass('fa-plus-circle');
            tr.find('i').addClass('fa-minus-circle');

            row.child( format( row.data() ) ).show();

            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
            $(this).parent('tr').next('tr').children('td').css('padding', '0px');
        }
    });

    table.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );

    //var date = moment().format()
    $('input[name=tanggal]').daterangepicker({
        "autoUpdateInput" : false,
        "startDate"       : moment().subtract(1, 'month'),
        "endDate"         : moment(),
        "maxDate"         : moment(),
        locale: {
            format: 'DD/MM/YYYY',
            cancelLabel: 'Limpiar',
            applyLabel: 'Aceptar',
            //firstDay: 1,
            daysOfWeek: [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            monthNames:[
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Setiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ]
        }
    });

    $('input[name="tanggal"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD') + '#' + picker.endDate.format('YYYY-MM-DD'));
    });

    $('input[name="tanggal"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $("#btn-search").on("click", function(e){
        //e.preventDefault();
        onClickButton();
    });

    //PRINT
    /*$("#table tbody").on("click", "tr td button#btn-print", function(e){
        e.preventDefault();
       
        var id = $(this).data('id');
        var kode = $(this).data('nama');
        $.ajax({
            type: 'GET',
            url : "/report/sales/salesexcel",
            dataType: 'json',
            cache: false,
            data: {
                '_token'    : $("input[name='_token']").val(),
                'id'        : id,
                'kode'      : kode
            },
            success: function (data) {
                console.log(data);
            }
        });

    });*/
    //DELETE
    $("#table tbody").on("click", "tr td button#btn-delete", function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var kode = $(this).data('nama');
        var total = $(this).data('total');

        $('body').append(modal);
        $('.modal-title').text("Anular Transacción");
        $('.modal-content').append(formdelete);

        $("#modal").find("input[name=id]").val(id);
        $("#modal").find("input[name=kode]").val(kode);
        $("#modal").find("input[name=total]").val(total);
        $("#modal").find("td[data=kode]").text(kode);
        $("#modal").find("td[data=total]").text("S/. "+total.toFixed(2));
        $('#modal').modal({keyboard: false, backdrop: 'static'});

        $('#modal').on('click', '#btnSimpan', function(e){
            $.ajax({
                type     : 'post',
                url      : "/report/sales/hapus",
                dataType : 'json',
                cache    : false,
                data     : {
                    '_token' : $("input[name='_token']").val(),
                    'id'     : $('input[name="id"]').val(),
                    'kode'   : $('input[name="kode"]').val(),
                    'nota'   : $('select[name="motiv"]').val()
                },
                complete: function(){
                    table.ajax.reload();
                }
            });
        });

        $("#modal").on('hidden.bs.modal', function(e){
            $('.modal').remove();
        });
    });

});

