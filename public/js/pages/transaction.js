/**
 * Created by Jean Omar L. Ch. on 02/10/2018.
 */
var kode_transaksi = $("input[name=kode_transaksi]");
var tanggal_transaksi = $("input[name=tanggal_transaksi]");

var kode = $("input[name=kode]");
var nama_item = $("input[name=nama_item]");
var harga_item = $("input[name=harga_item]");
var qty_item = $("input[name=qty_item]");
var total_harga_item = $("input[name=total_harga_item]");
var v_token = $("input[name=_token]");
var unit = ($("label[name=unit]"));
var tagihan = $("input[name=tagihan]");
var redondeo = $("input[name=redondeo]");
var final = $("input[name=final]");
var bayar = $("input[name=bayar]");
var kembalian = $("input[name=kembalian]");
var discount = $("#discount");
var discountv = $("#discountv");
var pmid = $("#pmid");
var myarray = [];

var modalCariItem = $('#modalCariItem');
var modalStokKosong = $('#modal-stok-kosong');
var modalkodeKosong = $('#modal-kode-kosong');
var modalPrint = $('#modalPrint');
var modalnewclient = $('#modalnewclient');

Inputmask.extendAliases({
    'kode': {
        regex: "^[a-zA-Z0-9]+$"
    }
});
Inputmask.extendAliases({
    rupiah: {
        groupSeparator: ".",
        alias: "numeric",
        placeholder: "0",
        autoGroup: !0,
        digits: 2,
        digitsOptional: !1,
        clearMaskOnLostFocus: !1
    }
});

Inputmask.extendAliases({
    qty: {
        groupSeparator: ".",
        alias: "numeric",
        placeholder: "0",
        autoGroup: !0,
        digits: 3,
        digitsOptional: !1,
        clearMaskOnLostFocus: !1
    }
});

var opt_cliente = {
    ajax   : {
        url    : "customer/select-customer",
        type   : "POST",
        dataType : "json",
        data     : {
            '_token' : $("input[name='_token']").val(),
            q        : "{{{q}}}"
        }
    },
    log: 3,
    preprocessData: function (data) {
        var i, l = data.length, array = [];
        if (l) {
            for(i = 0; i < l; i++){
                array.push($.extend(true, data[i], {
                    text: data[i].nama,
                    value: data[i].id,
                    data: {
                        subtext: data[i].kode
                    }
                }));
            }
        }
        return array;
    }
}

function nota_tgl(){
    $.getJSON('/transaction/nota-tgl', function(response){
        kode_transaksi.val(response.kode);
        tanggal_transaksi.val(response.tgl);
    });
}

var callbackDataTable = function(setting, json){
    $.getJSON('/transaction/totalbayar', function(data){
        $("#tagihan").text("A pagar S/. "+data);
        tagihan.val(data);
        redondeo.val((parseFloat(data).toFixed(1)-data).toFixed(2));
        final.val(parseFloat(data).toFixed(1)+"0");
    });
};

var infoitemtable =  $('#info-item').DataTable({
    processing: true,
    "deferRender": true,
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": false,
    "info": true,
    "autoWidth": true,
    "pageLength": 10,
    "ajax": {
        "url" : "/product/product-pilih",
        "dataSrc" : "data",
    },
    "columns" : [
        {"data": "kode"},
        {"data": "nama"},
        {"data": "harga"},
        {"data": "stok"},
        {"data": "pilih"}
    ]
});

var carttable = $('#table-cart').DataTable({
    "paging": false,
    "lengthChange": false,
    "searching": false,
    "ordering": false,
    "info": false,
    "autoWidth": false,
    //"scrollX": '50vh',
    //scrollCollapse: true,
    "ajax": {
        "url" : "/transaction/data-cart",
        "dataSrc" : "data"
    },
    "columns" : [
        {"data": "kode"},
        {"data": "nama"},
        {"data": "qty"},
        {"data": "harga"},
        {"data": "subharga"},
        {"data": "action"}
    ],
    "initComplete": callbackDataTable
});

//button cari
function _caribarang(){
    if(!kode.val()){//empty
        caribarang();
    }else{//not empty
        carikode();
        //console.log('NOT EMPTY');
    }
}

function caribarang(){
    modalCariItem.modal({keyboard: false, backdrop: 'static'});
}

function pilih(kode_item, nama, harga, stok){
    if(!parseInt(stok)){
        var notif = {
            type: "error",
            title: "Stock 0 para este producto",
            text: ""
        }
        show_stack_context('error', true, notif);
    }else{
        kode.val(kode_item);
        modalCariItem.modal('hide');
    }
    _caribarang();
    qty_item.focus();
}

$('#swatches')
    .find('img')
    .hover(
    function(e){
        var $this = $(this),
            _color = $this.attr('src');
        if (_color != 'img/default.png') {
            $('<div id="hoverThumb"></div>').css({
                'left': ($this.offset().left + 25)+'px' ,
                'top': ($this.offset().top - 25)+'px',
                'background-image': "url("+_color+")",
                'z-index': '100000px',
                'background-repeat': 'no-repeat',
                'background-size': '100px 100px'
            }).appendTo('body');
        }
        
    },
    function(e){
        $('div#hoverThumb').remove();
    }
);

function carikode(){
    $.ajax({
        type: 'post',
        url : "/transaction/cari-id",
        dataType: 'json',
        cache: false,
        data: {
            '_token'    : v_token.val(),
            'kode'      : kode.val()
        },
        success: function(request) {
            var notif = {
                type  : 'success',
                title : '',
                text  : "Código : "+request.data.kode+", Producto : "+request.data.nama+", Precio : "+request.data.harga
            }
            $('#my_image').attr('src',(request.data.url)?request.data.url:'img/default.png');
            nama_item.val(request.data.nama);
            harga_item.val(request.data.harga);
            total_harga_item.val((parseFloat(request.data.harga) * parseInt(qty_item.val())).toFixed(2));
            unit.text(request.data.satuan);
            if (request.data.discount>0) {
                discount.text("Desc.: S/. "+request.data.discount);
            }
            discountv.val(request.data.discount);
            pmid.val(request.data.produk_masuk_id);
            show_notif(notif);
            $('#btnaddcart').prop("disabled", false);
        },
        error: function(request){
            if(typeof request.status == 422){
                show_notif(request);
            }
            var errors = request.responseJSON;
            validate(errors);
        }
    })
}

function plusqty(){
    var qty = qty_item.val();
    var qty_ =  parseInt(qty)+1;
    qty_item.val(qty_);
    total_harga();
}

function minusqty(){
    var qty = qty_item.val();
    var qty_ =  parseInt(qty)-1;
    if(qty_ < 1) qty_ = 1;
    qty_item.val(qty_);
    total_harga();
}

function addcart(){
    harga_item.inputmask("remove");
    total_harga_item.inputmask("remove");
    $.ajax({
        type: 'post',
        url : "/transaction/add-cart",
        dataType: 'json',
        cache: false,
        data: {
            '_token'    : v_token.val(),
            'kode'      : kode.val(),
            'pmid'      : pmid.val(),
            'nama'      : nama_item.val(),
            'harga'     : (harga_item.val()-discountv.val()),
            'qty'       : qty_item.val(),
            'total'     : total_harga_item.val()
        },
        success: function(request) {
            //show_notif(request);
            carttable.ajax.reload(callbackDataTable);
            harga_item.inputmask({alias: "rupiah"});
            total_harga_item.inputmask({alias: "rupiah"});
            bayar.val("0.00");
            kembalian.val("0.00");
            discount.text("");
            discountv.val(0);
            pmid.val(0);
            resetform();
        },
        error: function(request){
            if(typeof request.status == 422){
                show_notif(request);
            }
            var errors = request.responseJSON;
            validate(errors);
            harga_item.inputmask({alias: "rupiah"});
            total_harga_item.inputmask({alias: "rupiah"});
        }
    });
}

function resetcart(){
    $.ajax({
        type: 'post',
        url : "/transaction/delete-cart",
        dataType: 'json',
        cache: false,
        data: {
            '_token'    : v_token.val(),
        },
        success: function(request) {
            //show_notif(request);
            $("input[name=tagihan]").val(0);
            $("[name=bayar]").val(0);
            $("[name=kembalian]").val(0);
            discount.text("");
            discountv.val(0);
            pmid.val(0);
            carttable.ajax.reload(callbackDataTable);
        },
        error: function(request){
            show_notif(request);
        }
    });
    resetform();
}

function removeitem(rowId, nama){
    $.ajax({
        type: 'post',
        url : "/transaction/remove-item",
        dataType: 'json',
        cache: false,
        data: {
            '_token'    : v_token.val(),
            'rowId'     : rowId,
            'nama'     : nama,
        },
        success: function(request) {
            //show_notif(request);
            carttable.ajax.reload(callbackDataTable);
        },
        error: function(){
            show_notif(request);
        }
    });
}

function resetform(){
    $("input[type][name!=_token][name!=qty_produk][name!=tanggal_transaksi][name!=kode_transaksi][name!=tagihan][name!=bayar][name!=kembalian][name!=common-radio-name]").val("");
    qty_item.val(1);
    $("#btnaddcart").attr('disabled', 'disabled');
    $('#my_image').attr('src','img/default.png');
    discount.text("");
    discountv.val(0);
    pmid.val(0);
}

$(function(){
    //inisialisasi
    nota_tgl();
    kode.focus();
    qty_item.val(1);
    qty_item.inputmask({ alias : "qty" });
    harga_item.inputmask({ alias : "rupiah" });
    total_harga_item.inputmask({ alias : "rupiah" });
    tagihan.inputmask({ alias : "rupiah" });
    redondeo.inputmask({ alias : "rupiah" });
    final.inputmask({ alias : "rupiah" });
    bayar.inputmask({ alias : "rupiah" });
    kembalian.inputmask({ alias : "rupiah" });
    $('select[name=cliente].selectpicker').selectpicker().ajaxSelectPicker(opt_cliente);

    //Form-Kode-Item
    kode.on('keydown', function(e) {
        if (e.which == 13) {//kode enter
            e.preventDefault();
            if(!$(this).val()){//empty
                caribarang();
            }else{//not empty
                carikode();
            }
        }
    });//Form-Nama
    nama_item.on('change', function(e) {
        if(this.value.length == 0){
            $("#btnaddcart").attr('disabled');
        }else{
            $('#btnaddcart').prop("disabled", false);
        }
    });
    qty_item.on('keydown', function(e) {
        if(e.which == 107){
            e.preventDefault();
            plusqty();
        }else if(e.which == 109){
            e.preventDefault();
            minusqty();
        }
    });
    kode.on('keyup',function() {
        if(this.value.length == 0){
            nama_item.val("");
            qty_item.val(1);
            harga_item.val(0);
            total_harga_item.val(0);
        }
    });

    bayar.on('keyup',function(e) {
        e.preventDefault();
        var _tagihan = tagihan.val().replace(',', '').replace('.00', '');//parseInt();
        var _bayar = bayar.val().replace(',', '').replace('.00', '');//;parseFloat();
        kembalian.val((_bayar - parseFloat(_tagihan).toFixed(1)).toFixed(1));
        //console.log(_bayar);
        if(_bayar > _tagihan){
            $('#info').children('div.box-body').removeClass('bg-yellow');
            $('#info').children('div.box-body').addClass('bg-green');
            $('#tagihan').text("Vuelto S/. "+kembalian.val());
        }else{
            $('#info').children('div.box-body').removeClass('bg-green');
            $('#info').children('div.box-body').addClass('bg-yellow');
            $('#tagihan').text("A pagar S/. "+tagihan.val());
        }
    });

    //info-item
    modalCariItem.on('show.bs.modal', function(){
        //console.log($('#modalCariItem input'));
    });
    modalCariItem.on('hidden.bs.modal', function(){
        kode.focus();
    });

    $("input[name=qty_item]").on('change', function(){
        total_harga();
    });

});


$('#modal').modal({keyboard: false, backdrop: 'static'});
        $('input[name=kodec]').inputmask({mask: "99999999999"});
 //TAMBAH
    $("#btnNewClient").on("click", function(e){
        e.preventDefault();
        $("#formclient")[0].reset();
        
        modalnewclient.modal({keyboard: false, backdrop: 'static'});
    });
    $("#btnSimpan").on('click', function(e){
        e.preventDefault();
        $.ajax({
            type     : 'POST',
            url      : "/customer/tambah",
            dataType : 'json',
            cache    : false,
            data: {
                '_token'        : $("input[name='_token']").val(),
                'nama'          : $('input[name="namac"]').val(),
                'kode'          : $('input[name="kodec"]').val().split('_')[0],
                'jenis_kelamin' : $('select[name="jenis_kelamin"]').val(),
                'telepon'       : $('input[name="telepon"]').val(),
                'alamat'        : $('textarea[name="alamat"]').val(),
            },
            complete: function(){
                modalnewclient.modal('hide');
            }
        });
    });

function total_harga(){
    var _qty = parseInt(qty_item.val());
    harga_item.inputmask("remove");
    var _harga = (parseFloat(harga_item.val())) ? parseFloat(harga_item.val()) : 0 ;
    harga_item.inputmask({alias: "rupiah"});
    total_harga_item.val((_qty*_harga).toFixed(2));
}

function bayarTunai(){
    bayar.inputmask('remove');
    kembalian.inputmask('remove');
    if (parseFloat(final.val())<=parseFloat(bayar.val()) && parseFloat(tagihan.val())>0 && parseFloat(bayar.val())>0 && parseFloat(kembalian.val())>=0) {
        $.ajax({
            type: "post",
            url: "transaction/bayar",
            dataType: 'json',
            data: {
                '_token'    : v_token.val(),
                'kode_transaksi' : kode_transaksi.val(),
                'bayar'     : bayar.val(),
                'kembalian' : kembalian.val(),
                'customer'  : $('select[name="cliente"]').val()
            },
            success: function(request) {
                //show_notif(request);
                nota_tgl();
                carttable.ajax.reload(callbackDataTable);

                $("[name=bayar]").val(0);
                $("[name=kembalian]").val(0);
                $('#info').children('div.box-body').removeClass('bg-green');
                $('#info').children('div.box-body').addClass('bg-yellow');

                var imprimir=$("input:radio[name=common-radio-name]:checked").val();

                if (imprimir == 1) {
                    modalPrint.modal({keyboard: false, backdrop: 'static'});
                    $('#transac').text(request.resumen.kode);
                    $('#id_transaction').val(request.resumen.id);
                    $('#fechav').text(request.resumen.tanggal);
                    $('#comprador').text((request.comprador)?request.comprador:"Sin registro");
                    $('#vtotal').text((request.resumen.bayar - request.resumen.kembalian).toFixed(2));
                    $('#vcueltoc').text(request.resumen.kembalian);
                    $('#vpagoc').text(request.resumen.bayar);
                    var det ='';
                    for (var i = 0; i < request.detalle.data.length; i++) {
                        det += '<tr><td>'+request.detalle.data[i].kode+'</td><td>'+request.detalle.data[i].nama+'</td><td>'+request.detalle.data[i].puc+'</td><td>'+request.detalle.data[i].qty+'</td></tr>';
                    }
                    $('#info-venta tbody').append(det);
                }else if(imprimir == 0){
                    print(request.resumen.id);
                }
                
            },
            complete: function(){
                infoitemtable.ajax.reload();    
            },
            error: function(response){
                //show_notif(response);
            }
        });
    }else{
        var opts = {
                type: "error",
                title: "Error",
                text: "Verifique si agregó productos o ingreso el monto con el cual pagó el cliente."
            };
        show_notif(opts);
    }
    bayar.inputmask({alias: 'rupiah'});
    kembalian.inputmask({alias: 'rupiah'});
}

function print(id){
    $.ajax({
        type: "post",
        url: "transaction/printt",
        dataType: 'json',
        data: {
            '_token'    : v_token.val(),
            'id'        : id
        },
        success: function(request) {
        },
        complete: function(){
            infoitemtable.ajax.reload();    
        },
        error: function(response){
            //show_notif(response);
        }
    });
}

function show_stack_context(type, modal, notif) {
    if (typeof stack_context === "undefined") stack_context = {
        "dir1": "down",
        "dir2": "left",
        "context": $("#modalCariItem")
    };
    if (typeof stack_context_modal === "undefined") stack_context_modal = {
        "dir1": "down",
        "dir2": "left",
        "context": $("#modalCariItem"),
        "modal": true,
        "overlay_close": true
    };
    var opts = {
        type: notif.type,
        title: notif.title,
        text: notif.text,
        stack: modal ? stack_context_modal : stack_context,
        addclass: modal ? "stack-modal" : ""
    };
    new PNotify(opts);
}

function validate(errors){
    if (typeof errors !== 'undefined') {
        $.each(errors, function(key, val){
            var opts = {
                type: "error",
                title: "Error",
                text: val
            };
            //show_notif(opts);
        });
    }
}




function bayarRfid(event){
    // $('body').append(modal);
    // $('.modal-title').text("Tambah Kategori");
    // $('.modal-content').append(rfidform);

    // $('#modal').modal({keyboard: false, backdrop: 'static'});
    RFID.Request({
        context : {
            event   : event,
            context : 'rfid',
            title   : "Pago con tarjeta"
        },
        title   : "Guardar",
        async   : true,
        //url     : '/asd',
        waktu   : 5000,
        berhasil: function (data) {
            console.log(data);
        },
        selalu  : function(event){
            console.log(event);
        }
    });
}