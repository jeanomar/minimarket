/**
 * Created by Jean Omar L. Ch. on 02/10/2018.
 */
Inputmask.extendAliases({
    'numeric': {
        allowPlus: false,
        allowMinus: false
    }
});

Inputmask.extendAliases({
    rupiah: {
        //prefix: "S/. ",
        groupSeparator: ".",
        alias: "numeric",
        placeholder: "0",
        autoGroup: !0,
        digits: 2,
        digitsOptional: !1,
        clearMaskOnLostFocus: !1
    }
});

var table = $("#table").DataTable({
    "paging": false,
    "lengthChange": false,
    "searching": false,
    "ordering": false,
    "info": true,
    "autoWidth": false,
    "ajax": {
        "url" : "/discount/data",
        "dataSrc" : "data"
    },
    "columns" : [
        {"data": "tanggal"},
        {"data": "nama"},
        {"data": "detail"},
        {"data": "harga"},
        {"data": "discount"},
        {"data": "stok"},
        {"data": "satuan"},
        {"data": "supplier"},
        {"data": "f_venc"},
        {"data": "quit"}
    ]
});

var form = "" +
    "<form class='form-horizontal'>"+
    "<div class='modal-body'>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Producto</label>"+
    "<div class='col-sm-9'>"+
    "<div class='input-group'>"+
    "<label class='form-control' name='kode'></label>"+
    "<input type='hidden' class='form-control' name='id'>"+
    "<span class='input-group-btn'>"+
    "<button type='button' title='Buscar' class='btn btn-info btn-flat' id='cari-item'><i class='fa fa-search'></i></button>"+
    "</span>"+
    "</div>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>P. U. Venta S/.</label>"+
    "<div class='col-sm-9'>"+
    "<input type='text' class='form-control' name='harga' disabled>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Cantidad</label>"+
    "<div class='col-sm-9'>"+
    '<div class="input-group">'+
        '<div class="input-group-addon">'+
            '<label name="satuan">UND</label>'+
        '</div>'+
        "<input type='text' class='form-control' name='stok' disabled>"+
    '</div>'+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Fecha de vencimiento</label>"+
    "<div class='col-sm-9'>"+
    "<input type='date' class='form-control' name='f_venc' disabled>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Descuento S/.</label>"+
    "<div class='col-sm-9'>"+
    "<input type='text' class='form-control' name='discount'>"+
    "</div>"+
    "</div>"+
    "</div>"+
    "<div class='modal-footer'>"+
    "<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>"+
    "<button type='button' class='btn btn-flat bg-aqua' id='btnSimpan'>Aceptar</button>"+
    "</div>"+
    "</form>";

var formdelete = "" +
    "<form class='form-horizontal'>"+
    "<div class='modal-body'>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Producto</label>"+
    "<div class='col-sm-9'>"+
    "<label class='form-control' name='kode'></label>"+
    "<input type='hidden' class='form-control' name='id'>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>P. U. Venta S/.</label>"+
    "<div class='col-sm-9'>"+
    "<label class='form-control' name='harga'></label>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Cantidad</label>"+
    "<div class='col-sm-9'>"+
    '<div class="input-group">'+
        '<div class="input-group-addon">'+
            '<label name="satuan">UND</label>'+
        '</div>'+
        "<label class='form-control' name='stok'></label>"+
    '</div>'+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Fecha de vencimiento</label>"+
    "<div class='col-sm-9'>"+
    "<label class='form-control' name='f_venc'></label>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Descuento S/.</label>"+
    "<div class='col-sm-9'>"+
    "<label class='form-control' name='discount'></label>"+
    "</div>"+
    "</div>"+
    "</div>"+
    "<div class='modal-footer'>"+
    "<button type='button' class='btn btn-default' data-dismiss='modal'>Cancelar</button>"+
    "<button type='button' class='btn btn-flat bg-aqua' id='btnSimpan'>Aceptar</button>"+
    "</div>"+
    "</form>";

var infoitemtable =  $('#info-item').DataTable({
    "processing"    : true,
    "deferRender"   : true,
    "paging"        : true,
    "lengthChange"  : false,
    "searching"     : true,
    "ordering"      : false,
    "info"          : true,
    "autoWidth"     : true,
    "ajax": {
        "url" : "discount/stok-pilih",
        "dataSrc" : "data"
    },
    "columns" : [
        {"data": "tanggal"},
        {"data": "f_venc"},
        {"data": "kode"},
        {"data": "nama"},
        {"data": "harga"},
        {"data": "discount"},
        {"data": "stok"},
        {"data": "satuan"},
        {"data": "supplier"},
        {"data": "pilih"}
    ]
});

var opt_detailitem = {
    ajax   : {
        url      : "/stock/in/select-detailitem",
        type     : "POST",
        dataType : "json",
        dataSrc  : "data",
        data     : {
            '_token' : $("input[name='_token']").val(),
            q        : "{{{q}}}"
        }
    },
    log: 3,
    preprocessData: function (data) {
        var i, l = data.length, array = [];
        if (l) {
            for(i = 0; i < l; i++){
                array.push($.extend(true, data[i], {
                    text: data[i].nama,
                    value: data[i].id,
                    data: {
                        subtext: data[i].keterangan
                    }
                }));
            }
        }
        return array;
    }
}
var opt_supplier = {
    ajax   : {
        url      : "/stock/in/select-supplier",
        type     : "POST",
        dataType : "json",
        dataSrc  : "data",
        data     : {
            '_token' : $("input[name='_token']").val(),
            q        : "{{{q}}}"
        }
    },
    /*locale : {
        emptyTitle : 'Select and Begin Typing'
    },*/
    log: 3,
    preprocessData: function (data) {
        var i, l = data.length, array = [];
        if (l) {
            for(i = 0; i < l; i++){
                array.push($.extend(true, data[i], {
                    text: data[i].nama,
                    value: data[i].id,
                    data: {
                        subtext: data[i].keterangan
                    }
                }));
            }
        }
        return array;
    }
}

function _caribarang(){
    if(!$('input[name=kode]').val()){//empty
        caribarang();
    }else{//not empty
        //console.log('NOT EMPTY');
    }
}

function caribarang(){
    $("#modalCariItem").modal({keyboard: false, backdrop: 'static'});
}

function pilih(kode, nama, harga, stok,satuan, venc){
    $("#modalCariItem").modal('hide');
    $('label[name=kode]').text(nama);
    $('label[name=satuan]').text( satuan);
    $('input[name=stok]').val(stok);
    $('input[name=id]').val(kode);
    $('input[name=harga]').val(harga);
    $('input[name=f_venc]').val(venc);

}

function cari(){
    //$('section.content').append(infoitem);
    $('#modalCariItem').modal({keyboard: true, backdrop: 'static'});
}

$(function(){
    $("#btn-tambah").on("click", function(e){
        e.preventDefault();
        $('div#tambah-modal-query').prepend(modal);

        $('.modal-title').text("Agregar descuento");
        $('#modal .modal-content').append(form);

        $('input[name=stok]').inputmask({ alias : "numeric" });

        $('select[name=detailitem].selectpicker').selectpicker().ajaxSelectPicker(opt_detailitem);
        $('select[name=supplier].selectpicker').selectpicker().ajaxSelectPicker(opt_supplier);

        $('#modal').modal({keyboard: false, backdrop: 'static'});

        var harga = $("input[name=harga]");
        harga.inputmask({ alias : "rupiah" });
        var discount = $("input[name=discount]");
        discount.inputmask({ alias : "rupiah" });

        $('input[name=kode]').on('keyup', function(e){
            e.preventDefault();
            $('label[name=producto]').text('');
            cari();
        });

        $("button#cari-item").click(function(e){
            e.preventDefault();
            //cari();
            _caribarang();
        });

        $("#btnSimpan").on('click', function(e){
            e.preventDefault();
            
            $.ajax({
                type: 'post',
                url : "/discount/tambah",
                dataType: 'json',
                cache: false,
                data: {
                    '_token'     : $("input[name='_token']").val(),
                    'id'         : $('input[name="id"]').val(),
                    'discount'   : $('input[name="discount"]').val()
                },
                complete: function(){
                    table.ajax.reload();
                    infoitemtable.ajax.reload();
                }
            });
        });

        $("#modal").on('hidden.bs.modal', function(e){
            $('#modal').remove();
        });
    });

    //DELETE
    $("#table tbody").on("click", "tr td button#btn-quit", function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var tanggal = $(this).data('tanggal');
        var discount = $(this).data('discount');
        var harga = $(this).data('harga');
        var venc = $(this).data('venc');
        var stok = $(this).data('stokf');
        var kode = $(this).data('produk');
        var satuan = $(this).data('satuan');

        $('body').append(modal);
        $('.modal-title').text("Eliminar Descuento");
        $('.modal-content').append(formdelete);

        $("#modal").find("input[name=id]").val(id);
        $("#modal").find("label[name=kode]").text(kode);
        $("#modal").find("label[name=tanggal]").text(tanggal);
        $("#modal").find("label[name=discount]").text(discount);
        $("#modal").find("label[name=f_venc]").text(venc);
        $("#modal").find("label[name=satuan]").text(satuan);
        $("#modal").find("label[name=harga]").text(harga);

        $('#modal').modal({keyboard: false, backdrop: 'static'});

        $('#modal').on('click', '#btnSimpan', function(e){
            $.ajax({
                type     : 'post',
                url      : "/discount/delete",
                dataType : 'json',
                cache    : false,
                data     : {
                    '_token' : $("input[name='_token']").val(),
                    'id'     : id
                },
                complete: function(){
                    table.ajax.reload();
                }
            });
        });

        $("#modal").on('hidden.bs.modal', function(e){
            $('.modal').remove();
        });
    });
});