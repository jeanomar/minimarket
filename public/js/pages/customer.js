/**
 * Created by Jean Omar L. Ch. on 02/10/2016.
 */
var table = $("#table").DataTable({
    "dom": '<"toolbar">frtip',
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "pageLength": 20,
    "ajax": {
        "url" : "/customer/data-customer",
        "dataSrc" : "data"
    },
    "columns" : [
        {"data": "nama"},
        {"data": "kode"},
        {"data": "jenis_kelamin"},
        {"data": "telepon"},
        {"data": "alamat"},
        {"data": "action"}
    ]
});

$("div.toolbar").addClass('col-md-6 col-xs-6 col-sm-6').css({'padding-left': '0px'}).html('<button class="btn btn-flat bg-aqua" id="btn-tambah" title="Nuevo registro"><i class="fa fa-plus"></i></button>');

var form = "" +
    "<form class='form-horizontal'>"+
    "<div class='modal-body'>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Nombres</label>"+
    "<div class='col-sm-9'>"+
    "<input class='form-control' placeholder='Nombres' type='text' name='nama'>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>DNI/RUC/CE</label>"+
    "<div class='col-sm-9'>"+
    "<input class='form-control' placeholder='DNI/RUC/CE' type='text' name='kode'>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Sexo</label>"+
    "<div class='col-sm-9'>"+
    "<select name='jenis_kelamin' class='form-control'>"+
    "<option value='' disabled selected>Seleccione una opción</option>"+
    "<option value='M'>Masculino</option>"+
    "<option value='F'>Femenino</option>"+
    "</select>"+
    "</div>"+
    "</div>"+
    '<div class="panel-group">'+
    '<div class="panel panel-default">'+
    '<div class="panel-heading">'+
    '<h4 class="panel-title">'+
    '<a data-toggle="collapse" href="#collapse1">Informacón adicional<i class="fa fa-angle-down pull-right"></i></a>'+
    '</h4>'+
    '</div>'+
    '<div id="collapse1" class="panel-collapse collapse" style="padding-top: 15px;">'+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Teléfono</label>"+
    "<div class='col-sm-9'>"+
    "<input class='form-control' placeholder='Teléfono' name='telepon'>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Dirección</label>"+
    "<div class='col-sm-9'>"+
    "<textarea class='form-control' name='alamat'>"+
    "</textarea>"+
    "</div>"+
    "</div>"+
    '</div>'+
    '</div>'+
    '</div>'+
    "</div>"+
    "<div class='modal-footer'>"+
    "<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>"+
    "<button type='button' class='btn btn-flat bg-aqua' id='btnSimpan'>Aceptar</button>"+
    "</div>"+
    "</form>";
var formdelete = "" +
    "<div class='modal-body'>"+
    "<input type='hidden' name='id'>"+
    "<table class='table table-bordered'>"+
    "<thead>"+
    "<tr>"+
    "<td>Nombres</td>"+
    "<td>Teléfono</td>"+
    "</tr>"+
    "</thead>"+
    "<tbody>"+
    "<tr>"+
    "<td data='nama'></td>"+
    "<td data='telepon'></td>"+
    "</tr>"+
    "</tbody>"+
    "</table>"+
    "</div>"+
    "<div class='modal-footer'>"+
    "<input type='hidden' name='id'>"+
    "<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>"+
    "<button type='button' class='btn btn-flat bg-aqua' id='btnSimpan'>Aceptar</button>"+
    "</div>";

$(function(){
    //TAMBAH
    $("#btn-tambah").on("click", function(e){

        e.preventDefault();
        $('body').append(modal);
        $('.modal-title').text("Nuevo cliente");
        $('.modal-content').append(form);
        $('#modal').modal({keyboard: false, backdrop: 'static'});
         $('input[name=kode]').inputmask({mask: "99999999999"});
        $("#btnSimpan").on('click', function(e){
            e.preventDefault();
            $.ajax({
                type     : 'POST',
                url      : "/customer/tambah",
                dataType : 'json',
                cache    : false,
                data: {
                    '_token'        : $("input[name='_token']").val(),
                    'nama'          : $('input[name="nama"]').val(),
                    'kode'          : $('input[name="kode"]').val().split('_')[0],
                    'jenis_kelamin' : $('select[name="jenis_kelamin"]').val(),
                    'telepon'       : $('input[name="telepon"]').val(),
                    'alamat'        : $('textarea[name="alamat"]').val(),
                },
                complete: function(){
                    table.ajax.reload();
                }
            });
        });

        $("#modal").on('hidden.bs.modal', function(e){
            $('.modal').remove();
        });
    });
    //EDIT
    $('#table tbody').on('click', 'tr td button#btn-edit', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var nama = $(this).data('nama');
        var kode = $(this).data('kode');
        var jk = $(this).data('jk');
        var tlp = $(this).data('tlp');
        var alamat  = $(this).data('alamat');

        $('body').append(modal);
        $('.modal-title').text("Editar cliente");
        $('.modal-content').append(form);
        $('#modal').modal({keyboard: false, backdrop: 'static'});

        $("#modal").find("form.form-horizontal").append('<input type="hidden" name="id">');

        $("#modal").find("input[name=id]").val(id);
        $("#modal").find("input[name=nama]").val(nama);
        $("#modal").find("input[name=kode]").val(kode);
        $("#modal").find("select[name=jenis_kelamin]").val(jk);
        $("#modal").find("input[name=telepon]").val(tlp);
        $("#modal").find("textarea[name=alamat]").val(alamat);

        $("#modal").on('click', '#btnSimpan', function (e) {
            $.ajax({
                type     : 'POST',
                url      : "/customer/edit",
                dataType : 'json',
                cache    : false,
                data: {
                    '_token'        : $("input[name='_token']").val(),
                    'id'            : $('input[name="id"]').val(),
                    'nama'          : $('input[name="nama"]').val(),
                    'kode'          : $('input[name="kode"]').val(),
                    'jenis_kelamin' : $('select[name="jenis_kelamin"]').val(),
                    'telepon'       : $('input[name="telepon"]').val(),
                    'alamat'        : $('textarea[name="alamat"]').val(),
                },
                complete: function () {
                    table.ajax.reload();
                }
            });
        });
        $("#modal").on('hidden.bs.modal', function(e){
            $('.modal').remove();
        });
    });
    //HAPUS
    $('#table tbody').on('click', 'tr td button#btn-delete', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var nama = $(this).data('nama');
        var tlp = $(this).data('tlp');

        $('body').append(modal);
        $('.modal-title').text("Eliminar cliente");
        $('.modal-content').append(formdelete);

        $("#modal").find("input[name=id]").val(id);
        $("#modal").find("td[data=nama]").text(nama);
        $("#modal").find("td[data=telepon]").text(tlp);

        $('#modal').modal({keyboard: false, backdrop: 'static'});

        $('#modal').on('click', '#btnSimpan', function(e){
            $.ajax({
                type: 'post',
                url : "/customer/hapus",
                dataType: 'json',
                cache: false,
                data: {
                    '_token' : $("input[name='_token']").val(),
                    'id'     : $('input[name="id"]').val(),
                    'nama'   : $('input[name="nama"]').val()
                },
                complete: function(){
                    table.ajax.reload();
                }
            });
        });

        $("#modal").on('hidden.bs.modal', function(e){
            $('.modal').remove();
        });
    });
});