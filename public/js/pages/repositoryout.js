/**
 * Created by Jean Omar L. Ch. on 02/10/2018.
 */
Inputmask.extendAliases({
    'numeric': {
        allowPlus: false,
        allowMinus: false
    }
});
var table = $("#table").DataTable({
    "paging": false,
    "lengthChange": false,
    "searching": false,
    "ordering": false,
    "info": true,
    "autoWidth": false,
    "ajax": {
        "url" : "/repository/out/data",
        "dataSrc" : "data"
    },
    "columns" : [
        {"data": "tanggal"},
        {"data": "kode"},
        {"data": "nama"},
        {"data": "detail"},
        {"data": "stok"},
        {"data": "satuan"},
        {"data": "user"}
    ]
});

var form = "" +
    "<form class='form-horizontal'>"+
    "<div class='modal-body'>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Código</label>"+
    "<div class='col-sm-9'>"+
    "<div class='input-group'>"+
    "<input class='form-control' name='kode' type='text'>"+
    "<span class='input-group-btn'>"+
    "<button type='button' class='btn btn-info btn-flat' id='cari-item'><i class='fa fa-search'></i></button>"+
    "</span>"+
    "</div>"+
    "<label name='producto'></label>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'></label>"+
    "<div class='col-sm-9'>"+
    "<label class='form-control'><input type='checkbox' checked id='movertienda' onclick='myFunction()' style='display:block;float:left;margin-right:5px;'> Mover a tienda</label>"+
    "</div>"+
    "</div>"+
    "<div class='form-group' style='display:none' id='det'>"+
    "<label class='col-sm-3 control-label'>Detalle</label>"+
    "<div class='col-sm-9'>"+
    "<select class='selectpicker form-control' data-live-search='true' name='detailitem'>"+
    "</select>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Cantidad</label>"+
    "<div class='col-sm-9'>"+
    '<div class="input-group">'+
        '<div class="input-group-addon">'+
            '<label name="satuan">UND</label>'+
        '</div>'+
        "<input type='text' class='form-control' name='stok'>"+
    '</div>'+
    "</div>"+
    "<div>"+
    '<div class="col-sm-12">'+
        '<div><br></div>'+
        '<table class="table table-bordered" id="table2">'+
           '<thead>'+
                '<tr>'+
                    '<th>Vencimiento</th>'+
                    '<th>Código</th>'+
                    '<th>Cantidad</th>'+
                    '<th>Acción</th>'+
                '</tr>'+
            '</thead>'+
        '</table>'+
    '</div>'+
    "</div>"+
    "</div>"+
    "</div>"+
    "<div class='modal-footer'>"+
    "<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>"+
    "<button type='button' class='btn btn-flat bg-aqua' id='btnSimpan'>Aceptar</button>"+
    "</div>"+
    "</form>";

var infoitemtable =  $('#info-item').DataTable({
    "processing"    : true,
    "deferRender"   : true,
    "paging"        : true,
    "lengthChange"  : false,
    "searching"     : true,
    "ordering"      : false,
    "info"          : true,
    "autoWidth"     : true,
    "ajax": {
        "url" : "/product/product-pilihrep",
        "dataSrc" : "data"
    },
    "columns" : [
        {"data": "kode"},
        {"data": "nama"},
        {"data": "harga"},
        {"data": "stok"},
        {"data": "satuan"},
        {"data": "pilih"}
    ]
});

var info =  $('#table2').DataTable();

var opt_detailitem = {
    ajax   : {
        url      : "/stock/out/select-detailitem",
        type     : "POST",
        dataType : "json",
        dataSrc  : "data",
        data     : {
            '_token' : $("input[name='_token']").val(),
            q        : "{{{q}}}"
        }
    },
    /*locale : {
        emptyTitle : 'Select and Begin Typing'
    },*/
    log: 3,
    preprocessData: function (data) {
        var i, l = data.length, array = [];
        if (l) {
            for(i = 0; i < l; i++){
                array.push($.extend(true, data[i], {
                    text: data[i].nama,
                    value: data[i].id,
                    data: {
                        subtext: data[i].keterangan
                    }
                }));
            }
        }
        return array;
    }
}

function myFunction() {
    var checkBox = document.getElementById("movertienda");
    var text = document.getElementById("det");
    if (checkBox.checked == false){
       text.style.display = "block";
    } else {
       text.style.display = "none";
    }
}

function _caribarang(){
    if(!$('input[name=kode]').val()){//empty
        caribarang();
    }else{//not empty
        carikode();
        //console.log('NOT EMPTY');
    }
}

function caribarang(){
    $("#modalCariItem").modal({keyboard: false, backdrop: 'static'});
}

function cari(){
    //$('#modalCariItem').modal({keyboard: false, backdrop: 'static'});
    $("#modalCariItem").modal('show');
}

function pilih(kode, nama, harga, stok){
    $("#modalCariItem").modal('hide');
    $('input[name=kode]').val(kode);
    carikode();
}

function carikode(){
    $.ajax({
        type: 'post',
        url : "/product/cari-item",
        dataType: 'json',
        cache: false,
        data: {
            '_token'    : $('input[name=_token]').val(),
            'kode'      : $('input[name=kode]').val()
        },
        success: function(request) {
            $('label[name=satuan]').text( request.val);
            $('label[name=producto]').text("Producto: "+request.produk);
            show_notif(request);
            if ( $.fn.dataTable.isDataTable( '#table2' ) ) {
                info.destroy();
                 //info.ajax.reload();
            }

            info =  $('#table2').DataTable({
                "processing"    : true,
                "deferRender"   : true,
                "paging"        : true,
                "lengthChange"  : false,
                "searching"     : false,
                "ordering"      : true,
                "info"          : true,
                "autoWidth"     : true,
                "pageLength"    : 5,
                retrieve        : true,
                "order": [[ 3, "desc" ], [ 1, "desc" ]],
                "ajax": {
                    "url" : "/product/product-alm?kode="+$('input[name=kode]').val(),
                    "dataSrc" : "data"
                },
                "columns" : [
                    {"data": "f_venc"},
                    {"data": "kode"},
                    {"data": "stok"},
                    {"data": "action"}
                ]
            });



        },
        error: function(request){
            show_notif(request);
        }
    })
}

$(function(){
    $("#btn-tambah").on("click", function(e){
        e.preventDefault();
        $('div#tambah-modal-query').prepend(modal);

        $('.modal-title').text("Quitar stock");
        $('#modal .modal-content').append(form);

        $('input[name=stok]').inputmask({ alias : "numeric" });

        $('select[name=detailitem].selectpicker').selectpicker().ajaxSelectPicker(opt_detailitem);

        $('#modal').modal({keyboard: false, backdrop: 'static'});

        $('input[name=kode]').on('keyup', function(e){
            $('label[name=producto]').text('');
            e.preventDefault();
            cari();
        });

        $("button#cari-item").click(function(e){
            e.preventDefault();
            //cari();
            _caribarang();
        });

        $("#btnSimpan").on('click', function(e){
            e.preventDefault();
            var t = document.getElementById("movertienda");
            
            $.ajax({
                type: 'post',
                url : "/repository/out/tambah",
                dataType: 'json',
                cache: false,
                data: {
                    '_token'    : $("input[name='_token']").val(),
                    'kode'      : $('input[name="kode"]').val(),
                    'detailitem': (t.checked == true)?6:$('select[name="detailitem"]').val(),
                    'stok'      : $('input[name="stok"]').val(),
                    'tanggal'   : new Date(),
                    'mov'       : t.checked,
                    'id_in'     : $('input[name=reposin]:checked').val()
                },
                complete: function(){
                    table.ajax.reload();
                    infoitemtable.ajax.reload();
                }
            });
        });

        $("#modal").on('hidden.bs.modal', function(e){
            $('#modal').remove();
        });
    });
});
