/**
 * Created by Jean Omar L. Ch. on 02/10/2018.
 */
Inputmask.extendAliases({
    'numeric': {
        allowPlus: false,
        allowMinus: false
    }
});

Inputmask.extendAliases({
    rupiah: {
        //prefix: "S/. ",
        groupSeparator: ".",
        alias: "numeric",
        placeholder: "0",
        autoGroup: !0,
        digits: 2,
        digitsOptional: !1,
        clearMaskOnLostFocus: !1
    }
});

var table = $("#table").DataTable({
    "paging": false,
    "lengthChange": false,
    "searching": false,
    "ordering": false,
    "info": true,
    "autoWidth": false,
    "ajax": {
        "url" : "/stock/in/data",
        "dataSrc" : "data"
    },
    "columns" : [
        {"data": "tanggal"},
        {"data": "kode"},
        {"data": "nama"},
        {"data": "detail"},
        {"data": "harga"},
        {"data": "stok"},
        {"data": "satuan"},
        {"data": "supplier"},
        {"data": "f_venc"}
    ]
});

var form = "" +
    "<form class='form-horizontal'>"+
    "<div class='modal-body'>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Código</label>"+
    "<div class='col-sm-9'>"+
    "<div class='input-group'>"+
    "<input class='form-control' name='kode' type='text'>"+
    "<span class='input-group-btn'>"+
    "<button type='button' title='Buscar' class='btn btn-info btn-flat' id='cari-item'><i class='fa fa-search'></i></button>"+
    "</span>"+
    "</div>"+
    "<label name='producto'></label>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Detalle</label>"+
    "<div class='col-sm-9'>"+
    "<select class='selectpicker form-control' data-live-search='true' name='detailitem'>"+
    "</select>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Proveedor</label>"+
    "<div class='col-sm-9'>"+
    "<select class='selectpicker form-control' data-live-search='true' name='supplier'>"+
    "</select>"+
    "<span class='help-block'><strong>Proveedor puede ser opcional</strong></span>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>P. U. Compra S/.</label>"+
    "<div class='col-sm-9'>"+
    "<input type='text' class='form-control' name='harga'>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Cantidad</label>"+
    "<div class='col-sm-9'>"+
    '<div class="input-group">'+
        '<div class="input-group-addon">'+
            '<label name="satuan">UND</label>'+
        '</div>'+
        "<input type='text' class='form-control' name='stok'>"+
    '</div>'+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Fecha de vencimiento</label>"+
    "<div class='col-sm-9'>"+
    "<input type='date' class='form-control' name='f_venc'>"+
    "</div>"+
    "</div>"+
    "</div>"+
    "<div class='modal-footer'>"+
    "<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>"+
    "<button type='button' class='btn btn-flat bg-aqua' id='btnSimpan'>Aceptar</button>"+
    "</div>"+
    "</form>";

var infoitemtable =  $('#info-item').DataTable({
    "processing"    : true,
    "deferRender"   : true,
    "paging"        : true,
    "lengthChange"  : false,
    "searching"     : true,
    "ordering"      : false,
    "info"          : true,
    "autoWidth"     : true,
    "ajax": {
        "url" : "/product/product-pilih",
        "dataSrc" : "data"
    },
    "columns" : [
        {"data": "kode"},
        {"data": "nama"},
        {"data": "harga"},
        {"data": "stok"},
        {"data": "satuan"},
        {"data": "pilih"}
    ]
});

var opt_detailitem = {
    ajax   : {
        url      : "/stock/in/select-detailitem",
        type     : "POST",
        dataType : "json",
        dataSrc  : "data",
        data     : {
            '_token' : $("input[name='_token']").val(),
            q        : "{{{q}}}"
        }
    },
    log: 3,
    preprocessData: function (data) {
        var i, l = data.length, array = [];
        if (l) {
            for(i = 0; i < l; i++){
                array.push($.extend(true, data[i], {
                    text: data[i].nama,
                    value: data[i].id,
                    data: {
                        subtext: data[i].keterangan
                    }
                }));
            }
        }
        return array;
    }
}
var opt_supplier = {
    ajax   : {
        url      : "/stock/in/select-supplier",
        type     : "POST",
        dataType : "json",
        dataSrc  : "data",
        data     : {
            '_token' : $("input[name='_token']").val(),
            q        : "{{{q}}}"
        }
    },
    /*locale : {
        emptyTitle : 'Select and Begin Typing'
    },*/
    log: 3,
    preprocessData: function (data) {
        var i, l = data.length, array = [];
        if (l) {
            for(i = 0; i < l; i++){
                array.push($.extend(true, data[i], {
                    text: data[i].nama,
                    value: data[i].id,
                    data: {
                        subtext: data[i].keterangan
                    }
                }));
            }
        }
        return array;
    }
}

function _caribarang(){
    if(!$('input[name=kode]').val()){//empty
        caribarang();
    }else{//not empty
        carikode();
        //console.log('NOT EMPTY');
    }
}

function caribarang(){
    $("#modalCariItem").modal({keyboard: false, backdrop: 'static'});
}

function pilih(kode, nama, harga, stok){
    $("#modalCariItem").modal('hide');
    $('input[name=kode]').val(kode);
    carikode();
}

function carikode(){
    $.ajax({
        type: 'post',
        url : "/product/cari-item",
        dataType: 'json',
        cache: false,
        data: {
            '_token'    : $('input[name=_token]').val(),
            'kode'      : $('input[name=kode]').val()
        },
        success: function(request) {
            $('label[name=satuan]').text( request.val);
            $('label[name=producto]').text( "Producto: "+request.produk);
            show_notif(request);
        },
        error: function(request){
            show_notif(request);
        }
    })
}

function cari(){
    //$('section.content').append(infoitem);
    $('#modalCariItem').modal({keyboard: true, backdrop: 'static'});
}

$(function(){
    $("#btn-tambah").on("click", function(e){
        e.preventDefault();
        $('div#tambah-modal-query').prepend(modal);

        $('.modal-title').text("Añadir stock");
        $('#modal .modal-content').append(form);

        $('input[name=stok]').inputmask({ alias : "numeric" });

        $('select[name=detailitem].selectpicker').selectpicker().ajaxSelectPicker(opt_detailitem);
        $('select[name=supplier].selectpicker').selectpicker().ajaxSelectPicker(opt_supplier);

        $('#modal').modal({keyboard: false, backdrop: 'static'});

        var harga = $("input[name=harga]");
        harga.inputmask({ alias : "rupiah" });

        $('input[name=kode]').on('keyup', function(e){
            e.preventDefault();
            $('label[name=producto]').text('');
            cari();
        });

        $("button#cari-item").click(function(e){
            e.preventDefault();
            //cari();
            _caribarang();
        });

        $("#btnSimpan").on('click', function(e){
            e.preventDefault();
            
            $.ajax({
                type: 'post',
                url : "/stock/in/tambah",
                dataType: 'json',
                cache: false,
                data: {
                    '_token'     : $("input[name='_token']").val(),
                    'kode'       : $('input[name="kode"]').val(),
                    'supplier'   : $('select[name="supplier"]').val(),
                    'detailitem' : $('select[name="detailitem"]').val(),
                    'harga'      : $('input[name="harga"]').val(),
                    'f_venc'      : $('input[name="f_venc"]').val(),
                    'stok'       : $('input[name="stok"]').val(),
                    'tanggal'    : new Date(),
                },
                complete: function(){
                    table.ajax.reload();
                    infoitemtable.ajax.reload();
                }
            });
        });

        $("#modal").on('hidden.bs.modal', function(e){
            $('#modal').remove();
        });
    });
});