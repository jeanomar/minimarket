/**
 * Created by Jean Omar L. Ch. on 02/10/2018.
 */

 Inputmask.extendAliases({
    'kode': {
        regex: "^[a-zA-Z0-9]+$"
    }
});

Inputmask.extendAliases({
    'numeric': {
        allowPlus: false,
        allowMinus: false
    }
});

Inputmask.extendAliases({
    rupiah: {
        //prefix: "S/. ",
        groupSeparator: ".",
        alias: "numeric",
        placeholder: "0",
        autoGroup: !0,
        digits: 2,
        digitsOptional: !1,
        clearMaskOnLostFocus: !1
    }
});

var table = $("#table").DataTable({
    "paging": true,
    "deferRender": true,
    "lengthChange": false,
    "searching": true,
    "ordering": true,
    "order": [[1,"desc"]],
    "info": true,
    "autoWidth": false,
    "pageLength": 20,
    "ajax": {
        "url" : "/product/data-product",
        "dataSrc" : "data"
    },
    "columns" : [
        {"data": "kode"},
        {"data": "nama"},
        {"data": "harga"},
        {"data": "stok"},
        {"data": "unit"},
        {"data": "category"},
        {"data": "stok_min"},
        {"data": "venc_min"},
        {"data": "action"}
    ]
});

var fileimg = "";

var form = "" +
    "<form class='form-horizontal' enctype='multipart/form-data'>"+
    "<div class='modal-body'>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Código</label>"+
    "<div class='col-sm-9'>"+
    "<input class='form-control' placeholder='Código de barras/Código' name='kode'>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Nombre</label>"+
    "<div class='col-sm-9'>"+
    "<input class='form-control' placeholder='Nombre' name='nama'>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>P. U. Venta S/.</label>"+
    "<div class='col-sm-9'>"+
    "<input class='form-control' placeholder='0,00' name='harga'>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Categoría</label>"+
    "<div class='col-sm-9'>"+
    "<select class='selectpicker form-control' data-live-search='true' name='kategori'>"+
    "</select>"+

    '<div name="categ">'+
    '<div class="input-group">'+
    '<input name="categinp" disabled type="text" class="form-control">'+
    '<span class="input-group-btn">'+
    '<button name="editKategory" type="button" class="btn btn-flat bg-aqua"><i class="fa fa-edit"></i></button>'+
    '</span>'+
    '</div>'+
    '</div>'+

    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Unidad</label>"+
    "<div class='col-sm-9'>"+
    "<select class='selectpicker form-control' data-live-search='true' name='satuan'>"+
    "</select>"+

    '<div name="unid">'+
    '<div class="input-group">'+
    '<input name="unidinp" disabled type="text" class="form-control">'+
    '<span class="input-group-btn">'+
    '<button name="editUnidad" type="button" class="btn btn-flat bg-aqua"><i class="fa fa-edit"></i></button>'+
    '</span>'+
    '</div>'+
    '</div>'+
    "</div>"+
    "</div>"+

    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Notificación Vencimiento</label>"+
    "<div class='col-sm-9'>"+
    '<div class="input-group">'+
        '<div class="input-group-addon">'+
            '<label>Días antes</label>'+
        '</div>'+
        "<input type='text' class='form-control' name='venci'>"+
    '</div>'+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Notificación Stock</label>"+
    "<div class='col-sm-9'>"+
    '<div class="input-group">'+
        '<div class="input-group-addon">'+
            '<label>Cant. min.</label>'+
        '</div>'+
        "<input type='text' class='form-control' name='stok'>"+
    '</div>'+
    "</div>"+
    "</div>"+
    
    "<div class='form-group'>"+
        "<label class='col-sm-3 control-label'>Descripción</label>"+
        "<div class='col-sm-9'>"+
            "<textarea class='form-control' name='deskripsi' style='resize: none; height: 80px'></textarea>"+
        "</div>"+
    "</div>"+
    "</div>"+
    "<div class='modal-footer'>"+
    "<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>"+
    "<button type='button' class='btn btn-flat bg-aqua' id='btnSimpan'>Aceptar</button>"+
    "</div>"+
    "</form>";

var formImg = "" +
    "<form class='form-horizontal' enctype='multipart/form-data'>"+
    "<div class='modal-body'>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Nombre</label>"+
    "<div class='col-sm-9'>"+
    "<input class='form-control' placeholder='Nombre' name='nama' disabled>"+
    "</div>"+
    "</div>"+
        "<div class='form-group'>"+
        '<div class="file-upload col-md-12" style="width:100% !important;">'+
            '<button class="file-upload-btn" type="button" onclick="'+"$('.file-upload-input').trigger( 'click' )"+'">Agregar Imagen</button>'+

          '<div class="image-upload-wrap">'+
            '<input class="file-upload-input" type="file" onchange="readURL(this);" accept="image/*" />'+
            '<div class="drag-text">'+
              '<h3>Arrastra y suelta o selecciona Agregar imagen</h3>'+
            '</div>'+
          '</div>'+
          '<div class="file-upload-content">'+
            '<img id="img_prod" class="file-upload-image" src="#" alt="your image" />'+
            '<div class="image-title-wrap">'+
              '<button type="button" onclick="removeUpload()" class="remove-image">Quitar <span class="image-title">Imagen cargada</span></button>'+
           ' </div>'+
          '</div>'+
        '</div>'+
        "</div>"+
    "</div>"+
    "<div class='modal-footer'>"+
    "<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>"+
    "<button type='button' class='btn btn-flat bg-aqua' id='btnSimpan'>Aceptar</button>"+
    "</div>"+
    "</form>";

var formdelete = "" +
    "<div class='modal-body'>"+
    "<input type='hidden' name='id'>"+
    "<input type='hidden' name='nama'>"+
    "<input type='hidden' name='kode'>"+
    "<table class='table table-bordered'>"+
    "<thead>"+
    "<tr>"+
    "<td>Código</td>"+
    "<td>Nombre</td>"+
    "<td>Precio S/.</td>"+
    "</tr>"+
    "</thead>"+
    "<tbody>"+
    "<tr>"+
    "<td data='kode'></td>"+
    "<td data='nama'></td>"+
    "<td data='harga'></td>"+
    "</tr>"+
    "</tbody>"+
    "</table>"+
    "</div>"+
    "<div class='modal-footer'>"+
    "<input type='hidden' name='kode'>"+
    "<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>"+
    "<button type='button' class='btn btn-flat bg-aqua' id='btnSimpan'>Eliminar</button>"+
    "</div>";

var opt_satuan = {
    ajax   : {
        url      : "product/select-satuan",
        type     : "POST",
        dataType : "json",
        dataSrc  : "data",
        data     : {
            '_token' : $("input[name='_token']").val(),
            q        : "{{{q}}}"
        }
    },
    log: 3,
    preprocessData: function (data) {
        var i, l = data.length, array = [];
        if (l) {
            for(i = 0; i < l; i++){
                array.push($.extend(true, data[i], {
                    text: data[i].nama,
                    value: data[i].id,
                    data: {
                        subtext: data[i].keterangan
                    }
                }));
            }
        }
        return array;
    }
}

var opt_kategori = {
    ajax   : {
        url    : "product/select-kategori",
        type   : "POST",
        dataType : "json",
        data     : {
            '_token' : $("input[name='_token']").val(),
            q        : "{{{q}}}"
        }
    },
    log: 3,
    preprocessData: function (data) {
        var i, l = data.length, array = [];
        if (l) {
            for(i = 0; i < l; i++){
                array.push($.extend(true, data[i], {
                    text: data[i].nama,
                    value: data[i].id,
                    data: {
                        subtext: data[i].keterangan
                    }
                }));
            }
        }
        return array;
    }
}


$(function(){

    //TAMBAH
    $("#btn-tambah").on("click", function(e){
        e.preventDefault();

        $('body').append(modal);
        $('.modal-title').text("Nuevo Producto");
        $('.modal-content').append(form);
        $("div[name=categ]").hide();
        $("div[name=unid]").hide();

        $('input[name=stok]').inputmask({ alias : "numeric" });
        $('input[name=venci]').inputmask({ alias : "numeric" });
        
        $('select[name=satuan].selectpicker').selectpicker().ajaxSelectPicker(opt_satuan);
        $('select[name=kategori].selectpicker').selectpicker().ajaxSelectPicker(opt_kategori);

        $('#modal').modal({keyboard: false, backdrop: 'static'});
        var kode = $("input[name=kode]");
        var harga = $("input[name=harga]");
        harga.inputmask({ alias : "rupiah" });
        kode.inputmask({ alias : "kode" });
        kode.focus();
        $("#btnSimpan").on('click', function(e){
            e.preventDefault();
            $.ajax({
                type: 'post',
                url : "/product/tambah",
                dataType: 'json',
                cache: false,
                data: {
                    '_token'    : $("input[name='_token']").val(),
                    'kode'      : $('input[name="kode"]').val(),
                    'nama'      : $('input[name="nama"]').val(),
                    'harga'     : $('input[name="harga"]').val(),
                    'deskripsi' : $('textarea[name="deskripsi"]').val(),
                    'kategori'  : $('select[name="kategori"]').val(),
                    'satuan'    : $('select[name="satuan"]').val(),
                    'venc_min'  : $('input[name="venci"]').val(),
                    'stok_min'   : $('input[name="stok"]').val()
                },
                complete: function(){
                    table.ajax.reload();
                },
                error: function(request){
                    
                }
            });
        });

        $("#modal").on('hidden.bs.modal', function(e){
            $('.modal').remove();
        });
    });

    
    //Imagen
    $("#table tbody").on("click", "tr td button#btn-img", function(e){
        e.preventDefault();
        var nama = $(this).data('nama');
        var p_id = $(this).data('id');
        var img_url = $(this).data('url');

        $('body').append(modal);
        $('.modal-title').text("Imagen del producto");
        $('.modal-content').append(formImg);
        $("#modal").find("input[name=nama]").val(nama);
        $('#modal').modal({keyboard: false, backdrop: 'static'});
        var name_img = "";
        if (img_url != "") {
            $('.image-upload-wrap').hide();
            $('.file-upload-image').attr('src', img_url);
            $('.file-upload-content').show();
            var array = img_url.split("/");
            name_img = array[array.length-1];
            $('.image-title').html(name_img);
        }

        $("#modal").on('click', '#btnSimpan', function(e){
            if (fileimg) {
                var formData = new FormData();
                formData.append('img', fileimg);
                formData.append('id', p_id);
                $.ajax({
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $("input[name='_token']").val()},
                    url : "/product/imgupload",
                    dataType: 'json',
                    cache: false,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    complete: function(){
                        table.ajax.reload();
                        $('#modal').modal('toggle');
                        $('.modal').remove();
                        fileimg = "";
                    }
                });
            }else{
                $('#modal').modal('toggle');
                 $('.modal').remove();
            }
        });

        $("#modal").on('hidden.bs.modal', function(e){
            $('.modal').remove();
        });
    }); 
    //EDIT
    $("#table tbody").on("click", "tr td button#btn-edit", function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var kode = $(this).data('kode');
        var nama = $(this).data('nama');
        var harga = $(this).data('harga');
        var deskripsi = $(this).data('deskripsi');
        var kategori = $(this).data('kategori');
        var satuan = $(this).data('satuan');
        var vencimin = $(this).data('venc_min');
        var stokmin = $(this).data('stok_min');
        
        $('body').append(modal);
        $('.modal-title').text("Editar Producto");
        $('.modal-content').append(form);

        $('input[name=stok]').inputmask({ alias : "numeric" });
        $('input[name=venci]').inputmask({ alias : "numeric" });

        //$('select[name=kategori]').parents('div.form-group').css('margin-bottom', '0px');
        $('select[name=kategori]').parents('div.col-sm-9').append('<span class="help-block"><small>Borrar cuando no haya cambiado</small></span>');

        //$('select[name=satuan]').parents('div.form-group').css('margin-bottom', '0px');
        $('select[name=satuan]').parents('div.col-sm-9').append('<span class="help-block"><small>Borrar cuando no haya cambiado</small></span>');

        $('select[name=satuan].selectpicker').selectpicker().ajaxSelectPicker(opt_satuan);
        $('select[name=kategori].selectpicker').selectpicker().ajaxSelectPicker(opt_kategori);

        var kodes = $("input[name=kode]");
        var hargas = $("input[name=harga]");
        hargas.inputmask({ alias : "rupiah" });
        kodes.inputmask({ alias : "kode" });
        kodes.focus();

        $('#modal').modal({keyboard: false, backdrop: 'static'});

        $("#modal").find("form.form-horizontal").append('<input type="hidden" name="id">');

        $("div[name=categ]").show();
        $("div[name=unid]").show();

        $.get('product/select-kategori?q='+kategori, function(data) {$("input[name=categinp]").val(data);});
        $.get('product/select-satuan?q='+satuan, function(data) {$("input[name=unidinp]").val(data);});
        $('select[name=kategori].selectpicker').selectpicker('hide');
        $('select[name=satuan].selectpicker').selectpicker('hide');

        $("#modal").find("input[name=id]").val(id);
        $("#modal").find("input[name=kode]").val(kode);
        $("#modal").find("input[name=nama]").val(nama);
        $("#modal").find("input[name=harga]").val(harga);
        $("#modal").find("input[name=deskripsi]").val(deskripsi);
        $("#modal").find("input[name=venci]").val(vencimin);
        $("#modal").find("input[name=stok]").val(stokmin);
        //$("select[name=kategori]").load('product/select-kategori?q=a');
        //$("select[name=kategori]").val(kategori);
        //$('select[name=kategori]').trigger('change').data('AjaxBootstrapSelect').list.cache = {}
        $("#modal").find("select[name=satuan]").val(satuan);

        $("#modal").on('click', '#btnSimpan', function(e){
            $.ajax({
                type: 'POST',
                url : "/product/edit",
                dataType: 'json',
                cache: false,
                data: {
                    '_token'    : $("input[name='_token']").val(),
                    'id'        : $("input[name='id']").val(),
                    'kode'      : $('input[name="kode"]').val(),
                    'nama'      : $('input[name="nama"]').val(),
                    'harga'     : $('input[name="harga"]').val(),
                    'deskripsi' : $('textarea[name="deskripsi"]').val(),
                    'kategori'  : $('select[name="kategori"]').val(),
                    'satuan'    : $('select[name="satuan"]').val(),
                    'venc_min'  : $('input[name="venci"]').val(),
                    'stok_min'   : $('input[name="stok"]').val()
                },
                complete: function(){
                    table.ajax.reload();
                }
            });
        });

        $("button[name=editKategory]").click(function() {
            $("div[name=categ]").hide();
            $('select[name=kategori].selectpicker').selectpicker('show');
        });

        $("button[name=editUnidad]").click(function() {
            $("div[name=unid]").hide();
            $('select[name=satuan].selectpicker').selectpicker('show');
        });

        $("#modal").on('hidden.bs.modal', function(e){
            $('.modal').remove();
        });
    });
    //DELETE
    $("#table tbody").on("click", "tr td button#btn-delete", function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var kode = $(this).data('kode');
        var nama = $(this).data('nama');
        var harga = $(this).data('harga');

        $('body').append(modal);
        $('.modal-title').text("Eliminar producto");
        $('.modal-content').append(formdelete);

        $("#modal").find("input[name=id]").val(id);
        $("#modal").find("input[name=kode]").val(kode);
        $("#modal").find("input[name=nama]").val(nama);
        $("#modal").find("td[data=kode]").text(kode);
        $("#modal").find("td[data=nama]").text(nama);
        $("#modal").find("td[data=harga]").text(harga);

        $('#modal').modal({keyboard: false, backdrop: 'static'});

        $('#modal').on('click', '#btnSimpan', function(e){
            $.ajax({
                type     : 'post',
                url      : "/product/hapus",
                dataType : 'json',
                cache    : false,
                data     : {
                    '_token' : $("input[name='_token']").val(),
                    'id'     : $('input[name="id"]').val(),
                    'kode'   : $('input[name="kode"]').val(),
                    'nama'   : $('input[name="nama"]').val()
                },
                complete: function(){
                    table.ajax.reload();
                }
            });
        });

        $("#modal").on('hidden.bs.modal', function(e){
            $('.modal').remove();
        });
    });

   
            
});
