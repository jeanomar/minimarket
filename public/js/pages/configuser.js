/**
 * Created by Jean Omar L. Ch. on 02/10/2016.
 */
Inputmask.extendAliases({
    'numeric': {
        allowPlus: false,
        allowMinus: false
    }
});
var table = $("#table").DataTable({
    "dom": '<"toolbar">frtip',
    "paging": false,
    "lengthChange": false,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "ajax": {
        "url" : "/setting/data-user",
        "dataSrc" : "data"
    },
    createdRow:function(row, data, dataindex){
        if (data.activate=="Inactivo") {
            $(row).css({'color':"red"});
        }
    },
    "columns" : [
        {"data": "nama"},
        {"data": "username"},
        {"data": "access"},
        {"data": "activate"},
        {"data": "action"}
    ]
});

$("#table_wrapper div.toolbar").addClass('col-md-6 col-xs-6 col-sm-6').css({'padding-left': '0px'}).html('<button class="btn btn-flat bg-aqua" id="btn-tambah" title="Nuevo registro"><i class="fa fa-plus"></i></button>');

var table2 = $("#table2").DataTable({
    "dom": '<"toolbar">frtip',
    "paging": false,
    "lengthChange": false,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "ajax": {
        "url" : "/setting/variables",
        "dataSrc" : "data"
    },
    createdRow:function(row, data, dataindex){
        if (data.activate=="Inactivo") {
            $(row).css({'color':"red"});
        }
    },
    "columns" : [
        {"data": "nama"},
        {"data": "val"},
        {"data": "user"},
        {"data": "activate"},
        {"data": "action"}
    ]
});

//$("#table2_wrapper div.toolbar").addClass('col-md-6 col-xs-6 col-sm-6').css({'padding-left': '0px'}).html('<button class="btn btn-flat bg-aqua" id="btn-tambah2" title="Nuevo registro"><i class="fa fa-plus"></i></button>');

var form = "" +
    "<form class='form-horizontal'>"+
    "<div class='modal-body'>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Nombres</label>"+
    "<div class='col-sm-9'>"+
    "<input class='form-control' placeholder='Nombres' type='text' name='nama'>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label' for='user'>Usuario</label>"+
    "<div class='col-sm-9'>"+
    "<input class='form-control' placeholder='Usuario' type='text' name='user'>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Contraseña</label>"+
    "<div class='col-sm-9'>"+
    "<input class='form-control' placeholder='Contraseña' type='password' name='password'>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Confirmar contraseña</label>"+
    "<div class='col-sm-9'>"+
    "<input class='form-control' placeholder='Confirmar contraseña' type='password' name='password_confirmation'>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Acceso</label>"+
    "<div class='col-sm-9'>"+
    "<label class='col-sm-3'><input type='radio' name='access' value='3' checked> Cajero</label>"+
    "<label class='col-sm-3'><input type='radio' name='access' value='2'> Gerente</label>"+
    "<label class='col-sm-3'><input type='radio' name='access' value='1'> Admin</label>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Estado</label>"+
    "<div class='col-sm-9'>"+
    "<label class='form-control'><input type='checkbox' name='active' style='display:block; float: left;margin-right: 5px;' checked> <span>Activo</span></label>"+
    "</div>"+
    "</div>"+
    "</div>"+
    "</div>"+
    "<div class='modal-footer'>"+
    "<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>"+
    "<button type='button' class='btn btn-flat bg-aqua' id='btnSimpan'>Aceptar</button>"+
    "</div>"+
    "</form>";

var form2 = "" +
    "<form class='form-horizontal'>"+
    "<div class='modal-body'>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Nombre</label>"+
    "<div class='col-sm-9'>"+
    "<input class='form-control' placeholder='Nombre' type='text' name='name'>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Valor</label>"+
    "<div class='col-sm-9'>"+
    "<input class='form-control' placeholder='Valor' type='text' name='value'>"+
    "</div>"+
    "</div>"+
    "<div class='form-group'>"+
    "<label class='col-sm-3 control-label'>Estado</label>"+
    "<div class='col-sm-9'>"+
    "<label class='form-control'><input type='checkbox' name='state' style='display:block;float:left;margin-right:5px;' checked> <span>Activo</span></label>"+
    "</div>"+
    "</div>"+
    "</div>"+
    "</div>"+
    "<div class='modal-footer'>"+
    "<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>"+
    "<button type='button' class='btn btn-flat bg-aqua' id='btnSimpan2'>Aceptar</button>"+
    "</div>"+
    "</form>";


$(function(){
    $("#btn-tambah").on("click", function(e){
        e.preventDefault();

        $('div#tambah-modal-query').prepend(modal);

        $('.modal-title').text("Agregar Usuario");
        $('#modal .modal-content').append(form);

        $('#modal').modal({keyboard: false, backdrop: 'static'});

        $("#btnSimpan").on('click', function(e){
            e.preventDefault();
            $.ajax({
                type: 'post',
                url : "/setting/tambah-user",
                dataType: 'json',
                cache: false,
                data: {
                    '_token'    : $("input[name='_token']").val(),
                    'nama'      : $('input[name="nama"]').val(),
                    'username'      : $('input[name="user"]').val(),
                    'password'  : $('input[name="password"]').val(),
                    'password_confirmation'     : $('input[name="password_confirmation"]').val(),
                    'access'    : $('input:radio[name="access"]:checked').val(),
                    'active'    : $('input:checkbox[name="active"]:checked').val()
                },
                complete: function(){
                    table.ajax.reload();
                }
            });
        });

        $("#modal").on('hidden.bs.modal', function(e){
            $('#modal').remove();
        });
    });

        //EDIT
    $('#table tbody').on('click', 'tr td button#btn-edit', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var nama = $(this).data('nama');
        var user = $(this).data('user');
        var access = $(this).data('access');
        var active = ($(this).data('active') == "on"?true:false);
        access = (access==1)?2:(access==2)?1:0;
        $('body').append(modal);
        $('.modal-title').text("Editar Usuario");
        $('.modal-content').append(form);
        $('#modal').modal({keyboard: false, backdrop: 'static'});

        $("#modal").find("form.form-horizontal").append('<input type="hidden" name="id">');

        $("#modal").find("input[name=id]").val(id);
        $("#modal").find("input[name=nama]").val(nama);
        $("#modal").find("input[name=user]").val(user);

         $("#modal").find('input:radio[name="access"]')[access].checked=true;
         $("#modal").find('input:checkbox[name="active"]').prop("checked",active);

        $("#modal").on('click', '#btnSimpan', function (e) {
            $.ajax({
                type     : 'POST',
                url      : "/setting/edit-user",
                dataType : 'json',
                cache    : false,
                data: {
                    '_token'         : $("input[name='_token']").val(),
                    'id'             : $('input[name="id"]').val(),
                    'nama'           : $('input[name="nama"]').val(),
                    'username'       : $('input[name="user"]').val(),
                    'password'       : $('input[name="password"]').val(),
                    'password_confirmation'     : $('input[name="password_confirmation"]').val(),
                    'access'         : $('input:radio[name="access"]:checked').val(),
                    'active'         : $('input:checkbox[name="active"]:checked').val(),
                },
                complete: function () {
                    table.ajax.reload();
                }
            });
        });
        $("#modal").on('hidden.bs.modal', function(e){
            $('.modal').remove();
        });
    });

    $("#btn-tambah2").on("click", function(e){
        e.preventDefault();

        $('div#tambah-modal-query').prepend(modal);

        $('.modal-title').text("Agregar Variable");
        $('#modal .modal-content').append(form2);

        $('#modal').modal({keyboard: false, backdrop: 'static'});

        $("#btnSimpan2").on('click', function(e){
            e.preventDefault();
            $.ajax({
                type: 'post',
                url : "/setting/tambah-variable",
                dataType: 'json',
                cache: false,
                data: {
                    '_token'    : $("input[name='_token']").val(),
                    'name'      : $('input[name="name"]').val(),
                    'value'      : $('input[name="value"]').val(),
                    'state'    : $('input:checkbox[name="state"]:checked').val()
                },
                complete: function(){
                    table2.ajax.reload();
                }
            });
        });

        $("#modal").on('hidden.bs.modal', function(e){
            $('#modal').remove();
        });
    });

    //EDIT
    $('#table2 tbody').on('click', 'tr td button#btn-edit', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var name = $(this).data('nama');
        var value = $(this).data('value');
        var active = ($(this).data('active') == "on"?true:false);

        $('body').append(modal);
        $('.modal-title').text("Editar Variable");
        $('.modal-content').append(form2);
        $('#modal').modal({keyboard: false, backdrop: 'static'});

        $("#modal").find("form.form-horizontal").append('<input type="hidden" name="id">');

        $("#modal").find("input[name=id]").val(id);
        $("#modal").find("input[name=name]").val(name);
        $("#modal").find("input[name=value]").val(value);
        $("#modal").find('input:checkbox[name="state"]').prop("checked",active);

        $("#modal").on('click', '#btnSimpan2', function (e) {
            $.ajax({
                type     : 'POST',
                url      : "/setting/edit-variable",
                dataType : 'json',
                cache    : false,
                data: {
                    '_token'         : $("input[name='_token']").val(),
                    'id'             : $('input[name="id"]').val(),
                    'nama'           : $('input[name="name"]').val(),
                    'value'          : $('input[name="value"]').val(),
                    'state'          : $("#modal").find('input:checkbox[name="state"]:checked').val()
                },
                complete: function () {
                    table2.ajax.reload();
                }
            });
        });
        $("#modal").on('hidden.bs.modal', function(e){
            $('.modal').remove();
        });
    });
 
});
