
var vm1 = new Vue({
    el:"#sesion",
    data: {
        stack_bottomright: [{"dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25}],
        stack_bar_top: [{"dir1": "down", "dir2": "right", "push": "top", "spacing1": 0, "spacing2": 0}],
        stack_bar_bottom: [{"dir1": "up", "dir2": "right", "spacing1": 0, "spacing2": 0}]
        },
    methods:{
        login: function (){
            var vm = this;
            $.ajax({
                type    : "post",
                url     : "/login",
                dataType: 'json',
                cache : false,
                data: {
                    '_token'    : $("input[name='_token']").val(),
                    'username'  : $('input[name="username"]').val(),
                    'password'  : $('input[name="password"]').val()

                },
                success: function(request, textStatus, errorThrown) {
                    window.location.replace(request.intended);//location.reload(request);
                },
                error: function(request, textStatus, errorThrown){
                    switch (request.status){
                        case 422 :
                            $.each(request.responseJSON, function(key, val){
                                var opts = {
                                    type: textStatus,
                                    title: "",
                                    text: val,
                                    addclass: "stack-bar-bottom",
                                    cornerclass: "",
                                    width: "100%",
                                    stack: vm.stack_bar_bottom[0],
                                    delay: 1200
                                };
                                vm.show_notif(opts);
                            });
                            break;
                        case  401:
                            var opts = {
                                type: textStatus,
                                title: "Login Failed",
                                text: request.responseJSON,
                                addclass: "stack-bar-bottom",
                                cornerclass: "",
                                width: "100%",
                                stack: vm.stack_bar_bottom[0],
                                delay: 1200
                            };
                            vm.show_notif(opts);
                            break;
                    }
                }
            })
        },

        show_notif: function(notif) {
            new PNotify(notif);
        },

        enter: function(event){
            if (event.keyCode == 13) {
                var vm = this;
                vm.login();
            }
        }

    },
    created () {
            var vm = this;
            $('body').addClass('login-page');
            PNotify.prototype.options.styling = "bootstrap3";
        }

});


