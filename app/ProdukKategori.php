<?php

namespace App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class ProdukKategori extends Model
{
    protected $table = 'produk_kategori';
    public $timestamps = true;
    protected $fillable = ['nama', 'keterangan', 'user_id'];
    protected $appends = ['action'];
    //protected $hidden = ['id'];

    public function produks()
    {
        return $this->hasMany(Produk::class, 'id_kategori');
    }

    public function getActionAttribute()
    {    $str=(Auth::user()->id_status!=3)?'
            <div class="btn-group pull-right">
                <button type="button" title="Editar" class="btn btn-sm btn-primary" id="btn-edit" data-id="'.$this->id.'" data-nama="'.$this->nama.'" data-ket="'.$this->keterangan.'"><i class="fa fa-edit"></i></button>
                <button type="button" title="Eliminar" class="btn btn-sm btn-danger" id="btn-delete" data-id="'.$this->id.'" data-nama="'.$this->nama.'" data-ket="'.$this->keterangan.'"><i class="fa fa-trash"></i></button>
            </div>
        ':"";
        return $this->attributes['action'] = $str;
    }
}
