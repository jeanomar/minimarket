<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repository extends Model
{
   	protected $table = 'repository';
    public $timestamps = false;
    protected $fillable = ['tanggal', 'f_venc', 'stok', 'harga', 'satuan', 'id_produk', 'id_produk_detail', 'id_user', 'id_supplier', 'type', 'nota','id_repository_in'];
    protected $hidden = ['id'];

     public function repositorys()
    {
        return $this->hasMany(Repository::class, 'id_repository_in');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'id_produk');
    }

    public function user()
    {
        return $this->belongsTo(Users::class, 'id_user');
    }

    public function produkdetail()
    {
        return $this->belongsTo(ProdukDetail::class, 'id_produk_detail');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'id_supplier');
    }
}
