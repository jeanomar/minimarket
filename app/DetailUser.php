<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailUser extends Model
{
    protected $table = 'user_detail';
    protected $fillable = ['id', 'sex', 'f_nac', 'doc_identity','photo','user_id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo(Users::class, 'user_id');
    }
}
