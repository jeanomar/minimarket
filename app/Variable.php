<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    protected $table = 'variables';
    protected $fillable = ['*'];
    protected $appends = ['action'];

    public function user()
    {
        return $this->belongsTo(Users::class, 'user_id');
    }

    public function getActionAttribute()
    {
        return $this->attributes['action'] =
            '
            <div class="btn-group pull-right">
                <button type="button" class="btn btn-sm btn-primary" data-id="'.$this->id.'" data-nama="'.$this->name.'" data-value="'.$this->value.'" data-active="'.(($this->state == 'IN')?"of":"on").'" id="btn-edit"><i class="fa fa-edit"></i></button>
            </div>
            ';
    }
}
