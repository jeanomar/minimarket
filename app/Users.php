<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Users extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;
    //use SoftDeletes;
    protected $hidden = ["password", "remember_token"];
    protected $fillable = ['nama', 'username', 'password', 'id_status'];
    protected $appends = ['action'];
    //protected $dates =['deleted_at'];

    public function setPasswordAttribute($value){
        $this->attributes['password'] = bcrypt($value);
    }

    public function alerts()
    {
        return $this->hasMany(Alert::class, 'user_id');
    }

    public function details(){
        return $this->hasone(DetailUser::class, 'user_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'id_status');
    }

    public function boxs()
    {
        return $this->hasMany(Box::class, 'user_id');
    }

    public function produkmasuks()
    {
        return $this->hasMany(ProdukMasuk::class, 'id_user');
    }

    public function produkkeluars()
    {
        return $this->belongsTo(ProdukKeluar::class, 'id_user');
    }

    public function transaksis()
    {
        return $this->hasMany(Transaksi::class, 'id_user');
    }

    public function getActionAttribute()
    {
        return $this->attributes['action'] =
            '
            <div class="btn-group pull-right">
                <button type="button" class="btn btn-sm btn-primary" data-id="'.$this->id.'" data-nama="'.$this->nama.'" data-user="'.$this->username.'" data-access="'.$this->id_status.'" data-active="'.(($this->deleted_at)?"of":"on").'" id="btn-edit"><i class="fa fa-edit"></i></button>
            </div>
            ';
    }
}
