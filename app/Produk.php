<?php

namespace App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Produk extends Model
{
    protected $table = 'produk';
    public $timestamps = true;
    protected $fillable = ['kode', 'nama', 'venc_min', 'stok_min', 'harga', 'deskripsi', 'id_kategori', 'id_satuan', 'id', 'url'];
    public $hidden = ['id_kategori', 'id_satuan', 'deskripsi'];
    //protected $primaryKey = 'kode';
    protected $appends = ['action', 'pilih', 'stok', 'stokrep'];

    public function kategori()
    {
        return $this->belongsTo(ProdukKategori::class, 'id_kategori');
    }

    public function satuan()
    {
        return $this->belongsTo(ProdukSatuan::class, 'id_satuan');
    }

    public function produkmasuks()
    {
        return $this->hasMany(ProdukMasuk::class, 'id_produk');
    }

    public function produkkeluars()
    {
        return $this->hasMany(ProdukKeluar::class, 'id_produk');
    }

    public function produkrepositorys()
    {
        return $this->hasMany(Repository::class, 'id_produk');
    }

    public function transaksidetails()
    {
        return $this->hasMany(TransaksiDetail::class, 'id_produk');
    }

    public function getStokAttribute()
    {
        return doubleval($this->produkmasuks()->sum('stok') -  $this->produkkeluars()->where("state","=","AC")->sum('stok'));
    }

     public function getStokrepAttribute()
    {
        return doubleval($this->produkrepositorys()->where("type","INPUT")->sum('stok') -  $this->produkrepositorys()->where("type","OUTPUT")->sum('stok'));
    }

    public function getActionAttribute()
    {
        $str=(Auth::user()->id_status!=3)?'<div class="btn-group pull-right">
                    <button type="button" title="Imagen" class="btn btn-sm btn-default" data-id="'.$this->id.'" data-kode="'.$this->kode.'" data-nama="'.$this->nama.'" data-url="'.$this->url.'" id="btn-img"><i class="fa fa-file-image-o"></i></button>
                    <button type="button" title="Editar" class="btn btn-sm btn-primary" data-id="'.$this->id.'" data-kode="'.$this->kode.'" data-nama="'.$this->nama.'" data-harga="'.$this->harga.'" data-deskripsi="'.$this->deskripsi.'" data-kategori="'.$this->id_kategori.'" data-satuan="'.$this->id_satuan.'" data-stok_min="'.$this->stok_min.'" data-venc_min="'.$this->venc_min.'" id="btn-edit"><i class="fa fa-edit"></i></button>
                    <button type="button" title="Eliminar" class="btn btn-sm btn-danger" data-id="'.$this->id.'" data-kode="'.$this->kode.'" data-nama="'.$this->nama.'" data-harga="'.$this->harga.'" id="btn-delete"><i class="fa fa-trash"></i></button>
                </div>':"";
        return $this->attributes['action'] = $str;
    }

    public function getPilihAttribute()
    {
        return '
			<div class="btn-group pull-right">
				<button type="button" title="Seleccionar" class="btn btn-sm btn-primary" onclick="pilih('."'".$this->kode."'".', '."'".$this->nama."'".', '."'".$this->harga."'".', '."'".$this->stok."'".')"><i class="fa fa-arrow-down"></i></button>
			</div>
		';
    }

}
