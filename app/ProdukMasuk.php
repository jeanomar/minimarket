<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdukMasuk extends Model
{
    protected $table = 'produk_masuk';
    public $timestamps = false;
    protected $fillable = ['tanggal', 'f_venc', 'stok', 'harga', 'satuan', 'id_produk', 'id_produk_detail', 'id_user', 'id_supplier','discount'];
    protected $hidden = ['id', 'id_produk', 'id_produk_detail', 'id_user', 'id_supplier'];

    protected $appends = ['quit', 'pilih', 'stokf'];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'id_produk');
    }

    public function user()
    {
        return $this->belongsTo(Users::class, 'id_user');
    }

    public function produkdetail()
    {
        return $this->belongsTo(ProdukDetail::class, 'id_produk_detail');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'id_supplier');
    }

    public function produkkeluars()
    {
        return $this->hasMany(ProdukKeluar::class, 'produkmasuk_id');
    }

    public function scopeOrderByTanggalDesc($query)
    {
        return $query->orderBy('tanggal', 'DESC');
    }

    public function getStokfAttribute()
    {
        return doubleval($this->stok -  $this->produkkeluars()->where("state","=","AC")->sum('stok'));
    }

    public function getPilihAttribute()
    {
        return '<div class="btn-group pull-right">
                    <button type="button" title="Seleccionar" class="btn btn-sm btn-primary" onclick="pilih('."'".$this->id."'".', '."'".$this->produk()->get()->pluck('nama')->first()."'".', '."'".$this->harga."'".', '."'".$this->stokf."'".', '."'".$this->satuan."'".', '."'".$this->f_venc."'".')"><i class="fa fa-arrow-down"></i></button>
                </div>';
    }

    public function getQuitAttribute()
    {
        $str = '<div class="btn-group pull-right">
                    <button type="button" title="Quitar" class="btn btn-sm btn-danger" data-id="'.$this->id.'" data-tanggal="'.$this->tanggal.'" data-discount="'.$this->discount.'" data-venc="'.$this->f_venc.'" data-stok="'.$this->stok.'" data-harga="'.$this->harga.'" data-produk="'.$this->produk()->get()->pluck('nama')->first().'" data-satuan="'.$this->satuan.'" data-stokf="'.$this->stokf.'" id="btn-quit"><i class="fa fa-minus"></i></button>
                </div>';
        return $this->attributes['quit'] = $str;
    }

}
