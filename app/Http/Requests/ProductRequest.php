<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kode' => 'required|unique:produk',
            'nama' => 'required',
            'harga' => 'required:numeric|min:1',
            'venc_min' => 'required:numeric|min:1',
            'stok_min' => 'required:numeric|min:1',
            'deskripsi' => 'nullable',
            'kategori' => 'required',
            'satuan' => 'required'
        ];
    }

    public function messages(){
        return [
            'required' => ':attribute no puede estar vacío',
            'unique' => ':attribute ya existe en la base de datos'
        ];
    }

    public function attributes()
    {
        return [
            'kode' => 'Código',
            'nama' => 'Nombre',
            'harga' => 'Precio',
            'deskripsi' => 'Descripción',
            'kategori' => 'Categoria',
            'venc_min' => 'Notificación vencimiento',
            'stok_min' => 'Notificación stock',
            'satuan' => 'Unidad'
        ];
    }
}
