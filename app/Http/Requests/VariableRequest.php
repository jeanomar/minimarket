<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VariableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:50|unique:variables',
            'value' => 'required|string|max:50'
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute está vacío',
            'unique' => ':attribute ya existe en la database'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'value' => 'Valor'
        ];
    }
}
