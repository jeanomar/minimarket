<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|unique:produk_kategori|min:1|max:6',
            'keterangan' => 'required|max:20'
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute está vacío',
            'unique' => ':attribute existe en la database'
        ];
    }

    public function attributes()
    {
        return [
            'nama' => 'Símbolo',
            'keterangan' => 'Nombre'
        ];
    }
}
