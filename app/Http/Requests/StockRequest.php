<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StockRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal' => 'nullable',
            'kode' => 'required|exists:produk,kode',
            'detailitem' => 'required',
            'supplier' => 'nullable',
            'stok' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute está vacío',
            'numeric' => ':attribute debe ser un número',
            'exists' => ':attribute ya existe en la database',
        ];
    }

    public function attributes()
    {
        return [
            'tanggal' => 'Fecha',
            'kode' => 'Código',
            'detailitem' => 'Detalle',
            'supplier' => 'Proveedor',
            'stok' => 'Stock'
        ];
    }
}
