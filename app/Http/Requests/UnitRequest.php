<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required:unique:produk_satuan|min:1|max:6',
            'keterangan' => 'required|max:20'
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute no puede estar vacío',
            'unique' => ':attribute ya existe en la base de datos'
        ];
    }

    public function attributes()
    {
        return [
            'nama' => 'Abreviatura',
            'keterangan' => 'Nombre'
        ];
    }
}
