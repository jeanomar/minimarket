<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'kode' => 'nullable',
            'jenis_kelamin' => 'required',
            'alamat' => 'nullable',
            'telepon' => 'nullable'
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute está vacío',
            'unique' => ':attribute ya existe en la database'
        ];
    }

    public function attributes()
    {
        return [
            'nama' => 'Nombres',
            'kode' => 'Documento de identidad',
            'jenis_kelamin' => 'Sexo',
            'alamat' => 'Dirección',
            'telepon' => 'Teléfono',
        ];
    }
}
