<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConfigUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|string|max:50',
            'username' => 'required|string|max:50|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'access' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute está vacío',
            'unique' => ':attribute ya existe en la database'
        ];
    }

    public function attributes()
    {
        return [
            'nama' => 'Nombres',
            'username' => 'Usuario',
            'password' => 'Contraseña',
            'access' => 'Acceso'
        ];
    }
}
