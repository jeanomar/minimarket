<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\ProdukDetail;
use App\ProdukMasuk;
use App\Supplier;
use App\Repository;

use App\Transformers\ProdukMasukTransformer;
use App\Transformers\RepositoryinTransformer;
use App\Transformers\RepositoryoutTransformer;
use Illuminate\Database\QueryException;

use App\Http\Requests\StockRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Spatie\Fractal\ArraySerializer;

class RepositoryController extends Controller
{
   	public function input()
    {
        return view('repositoryin');
    }

    public function showin(Repository $pm)
    {
        $pm = $pm->where("type","INPUT")->get()->sortByDesc('id');
        return fractal()
            ->collection($pm)
            ->transformWith(new RepositoryinTransformer())
            ->toArray();
    }

    public function createin(StockRequest $request, Repository $masuk, Produk $produk)
    {
        $this->validate($request, [
                'harga' => 'required|numeric'
            ],
            [],
            [
                'harga' => 'P. U. Compra S/.'
            ]
        );
        try{
            $id_produk = $produk->where('kode', $request->kode)->pluck('id')->first();
            $prod = Produk::find($id_produk);

            $result = $masuk->create([
                'tanggal' => date("Y-m-d H:i:s"),
                'f_venc' => ($request->f_venc) ? $request->f_venc : null,
                'stok' => $request->stok,
                'satuan' => $prod->satuan->nama,
                'harga' => $request->harga,
                'id_produk' => $id_produk,
                'id_produk_detail' => $request->detailitem,
                'id_user' => Auth::user()->id,
                'id_supplier' => ($request->supplier) ? $request->supplier : null
            ]);
            if($result){
                return Response()
                    ->json([
                        'type'=> 'success',
                        'title' => 'Agregado con éxito',
                        'text' => $request->tanggal." ".$request->stok." ".$request->kode
                    ], 201);
            }else{
                return Response()
                    ->json([
                        'type'=> 'error',
                        'title' => 'No se pudo agregar',
                        'text' => $request->tanggal." ".$request->stok." ".$request->kode
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    'title' => 'No se pudo agregar',
                    'text' => $e->getMessage()
                ], 501);
        }

    }

    public function output()
    {
        return view('repositoryout');
    }

    public function showout(Repository $keluar)
    {
        $keluar = $keluar->where("type","OUTPUT")->get()->sortByDesc('id');
        return fractal()
            ->collection($keluar)
            ->transformWith(new RepositoryoutTransformer())
            ->toArray();
    }

    public function createout(Requests\StockRequest $request, Repository $keluar, Produk $produk, ProdukMasuk $masuk)
    {
    	$this->validate($request, [
                'id_in' => 'required'
            ],
            [
            	'required'=>':attribute No ha seleccionado ninguno'
        	],
            [
                'id_in' => 'Ingreso de almacén'
            ]
        );
        try{
        	$nota = ($request->mov)?"movido a tienda":null;
            $id_produk = $produk->where('kode', $request->kode)->pluck('id')->first();
            $prod = Produk::find($id_produk);
            $repositoryin = Repository::find($request->id_in);
            $keluar = $keluar->create([
                //'tanggal' => $request->tanggal,
                'type' => "OUTPUT",
                'tanggal' => date("Y-m-d H:i:s"),
                'stok' => $request->stok,
                'harga' => $repositoryin->harga,
                'f_venc' => $repositoryin->f_venc,
                'id_supplier' => $repositoryin->id_supplier,
                'id_repository_in' => $repositoryin->id,
                'satuan' => $repositoryin->satuan,
                'id_produk' => $id_produk,
                'id_produk_detail' =>$request->detailitem,
                'nota' => $nota,
                'id_user' => Auth::user()->id
            ]);

            $result = $masuk->create([
                //'tanggal' => $request->tanggal,
                'tanggal' => date("Y-m-d H:i:s"),
                'f_venc' => $repositoryin->f_venc,
                'stok' => $request->stok,
                'satuan' => $repositoryin->satuan,
                'harga' => $repositoryin->harga,
                'id_produk' => $id_produk,
                'id_produk_detail' => 1,
                'id_user' => Auth::user()->id,
                'id_supplier' => $repositoryin->id_supplier
            ]);

            if($result){
                return Response()
                    ->json([
                        'type' => 'success',
                        'title' => 'Agregado con éxito',
                        'text' => $request->kode.", ".$request->stok.', '.$request->detailitem
                    ], 201);
            }else{
                return Response()
                    ->json([
                        'type' => 'error',
                        'title' => 'No se pudo agregar',
                        'text' => $request->kode
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    //'title' => $e->getMessage(),
                    'title' => 'No se pudo agregar',
                    'text' => $e->getSql()
                ], 501);
        }
    }
}
