<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Users;
use App\Variable;
use App\Alert;
use App\DetailUser;
use App\Transformers\UserTransformer;
use App\Transformers\VariableTransformer;
use App\Http\Requests\ConfigUserRequest;
use App\Http\Requests\VariableRequest;
use Illuminate\Support\Facades\Auth;

class ConfigUserController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	if(Auth::user()->id == 1){
        	return view('configuser');
        }else{
            return redirect("/");
        }
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Users $users)
    {
        //$keluar = $keluar->orderByTanggalDesc()->get();
        $users = $users::get();

        return fractal()
            ->collection($users)
            ->transformWith(new UserTransformer())
            ->toArray();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ConfigUserRequest $request, Users $user, Alert $alert)
    {
        try{
            
            $user = new Users();
	        $user->nama =  $request->nama;
	        $user->username = $request->username;
	        $user->password = $request->password;
	        $user->id_status = $request->access;
	        $user->user_id = Auth::User()->id;
	        $user->deleted_at = ($request->active)?null:date("Y-m-d H:i:s");
	        $user->save();

	        $alert = Alert::create([
                'type' => "bg-green",
                'status_id' => $user->id_status,
                'user_id' =>$user->id,
                'nota' => "Bienvenido! ".$user->nama.", empieza configurando tu información y tu foto de perfil si lo crees necesario.",
                'created_id' => Auth::user()->id
            ]);

            if($user->id){
                return Response()
                    ->json([
                        'type' => 'success',
                        'title' => 'Agregado con éxito',
                        'text' => $request->nama.", ".$request->username
                    ], 201);
            }else{
                return Response()
                    ->json([
                        'type' => 'error',
                        'title' => 'No se pudo agregar',
                        'text' => $request->kode
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    //'title' => $e->getMessage(),
                    'title' => 'No se pudo agregar',
                    'text' => $e->getSql()
                ], 501);
        }
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Users $user)
    {
    	$this->validate($request, [
            'nama' => 'required|string|max:50',
            'username' => 'required|string|max:50',
            'password' => 'confirmed',
            'access' => 'required'
            ]
        );
        try{
            $user = $user->findOrFail($request->id);
            $user->nama =  $request->nama;
	        $user->username = $request->username;
	        if ($request->password!="") {
	        	$user->password = $request->password;
	        }
	        
	        $user->id_status = $request->access;
	        $user->user_id = Auth::User()->id;
	        $user->deleted_at = ($request->active)?null:date("Y-m-d H:i:s");
            if($user->save()){
                return Response()
                    ->json([
                        'type' => 'success',
                        'title' => 'Cambió exitosamente',
                        'text' => $request->nama
                    ], 202);
            }else{
                return Response()
                    ->json([
                        'type' => 'error',
                        'title' => 'No se pudo hacer el cambio',
                        'text' => $request->nama
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    'title' => 'No se pudo hacer el cambio',
                    'text' => $e->getMessage(),
                ],501);
        }
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatedetuser(Request $request, DetailUser $duser)
    {
        try{
            $duser = $duser->where("user_id",Auth::user()->id)->get()->first();
        	if (!$duser) {
        		$duser = new DetailUser;
        		$duser->user_id = Auth::user()->id;
        		$duser->photo = "/img/default.png";
        	}
            $duser->doc_identity =  $request->dni;
	        $duser->f_nac = $request->f_nac;
	        $duser->sex = $request->sex;

            if($duser->save()){
                return Response()
                    ->json([
                        'type' => 'success',
                        'title' => 'Cambió exitosamente',
                        'text' => Auth::user()->nama
                    ], 202);
            }else{
                return Response()
                    ->json([
                        'type' => 'error',
                        'title' => 'No se pudo hacer el cambio',
                        'text' =>  Auth::user()->nama
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    'title' => 'No se pudo hacer el cambio',
                    'text' => $e->getMessage(),
                ],501);
        }
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request, DetailUser $duser)
    {
    	if(Input::hasFile('file')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('file');
            $tmpFilePath = '/img/'.auth()->user()->id;
            //generamos un identificador
            $carbon = new Carbon();
            $date = $carbon->now()->timestamp;
            
            $tmpFileName =auth()->user()->id.'-'.$date.'.'.pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            
            $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
            //insertamos el registro en imagenes
            
        	$duser = $duser->where("user_id",Auth::user()->id)->get()->first();
        	if (!$duser) {
        		$duser = new DetailUser;
        		$duser->user_id = Auth::user()->id;
        	}
        	$duser->photo = "/img/".Auth::user()->id."/".$tmpFileName;
        	$duser->save();
            return Response()->json($duser);
        } else {
            return response()->json(Input::file('file'));
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function variables(Variable $variables)
    {
        //$keluar = $keluar->orderByTanggalDesc()->get();
        $variables = $variables::get();
        return fractal()
            ->collection($variables)
            ->transformWith(new VariableTransformer())
            ->toArray();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createvariable(VariableRequest $request, Variable $var)
    {
        try{
            $var = new Variable();
            $var->name =  $request->name;
            $var->value = $request->value;
            $var->state = ($request->state == 'on')?"AC":"IN";
            $var->user_id = Auth::User()->id;
            $var->save();

            if($var->id){
                return Response()
                    ->json([
                        'type' => 'success',
                        'title' => 'Agregado con éxito',
                        'text' => $request->name.", ".$request->value
                    ], 201);
            }else{
                return Response()
                    ->json([
                        'type' => 'error',
                        'title' => 'No se pudo agregar',
                        'text' => $request->name
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    //'title' => $e->getMessage(),
                    'title' => 'No se pudo agregar',
                    'text' => $e->getSql()
                ], 501);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatevariable(Request $request, Variable $var)
    {
        $this->validate($request, [
            'nama' => 'required|string|max:20',
            'value' => 'required|string|max:20'
            ]
        );
        try{
            $var = $var->findOrFail($request->id);
            $var->name =  $request->nama;
            $var->value = $request->value;
            $var->user_id = Auth::User()->id;
            $var->state = ($request->state)?"AC":"IN";
            if($var->save()){
                return Response()
                    ->json([
                        'type' => 'success',
                        'title' => 'Cambió exitosamente',
                        'text' => $request->nama.", ".$request->value
                    ], 202);
            }else{
                return Response()
                    ->json([
                        'type' => 'error',
                        'title' => 'No se pudo hacer el cambio',
                        'text' => $request->nama
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    'title' => 'No se pudo hacer el cambio',
                    'text' => $e->getMessage(),
                ],501);
        }
    }
}
