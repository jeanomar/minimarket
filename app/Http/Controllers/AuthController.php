<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\LoginRequest;
use App\Produk;
use App\Alert;
use App\Users;
use Illuminate\Support\Facades\URL;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function getLogin()
    {
        $s_c = '467a6fbe1be0a88ddb597b5fe5ee2df7';
        if(strtoupper(PHP_OS) == strtoupper("LINUX")){
            $ds=shell_exec('udevadm info --query=all --name=/dev/sda | grep ID_SERIAL_SHORT');
            $sx = explode("=", $ds);
            $s = $sx[1];
            $lic = md5('username'.'password'.trim($s).$s_c);
        }else{
            function GetVol($drive){(preg_match('#mero de serie del volumen es: (.*)\n#i', shell_exec('dir '.$drive.':'), $m))?($vol = ' ('.$m[1].')'):($vol = '');return $vol;}
            $lic = md5('2bb889a37bae9c0bdcf6ca121539a998x'.'bec170723ab9c6edef68f03efd40da96y'.trim(str_replace("(","",str_replace(")","",GetVol("c")))).$s_c);
        }
        if(file_exists($lic.'.key')) return view('auth.login');
        return view('welcome');
    }

    public function postLogin(LoginRequest $request, Users $user, Produk $products, Alert $alert)
    {
        $credentials = $request->only('username', 'password');
        if(!Auth::attempt($credentials, $request->has('remember')))
        {
            return Response()->json(['El nombre de usuario o la contraseña que ingresaste son incorrectos'], 401);
        }

        $user = $user->find(Auth::user()->id);

        if($user->deleted_at)
        {
            return Response()->json(['El usuario ya no esta habilitado para ingresar al sistema.'], 401);
        }

        if (Alert::where("type","bg-green")->where("created_id",$user->id)->count() == 0) {
            $ussers = Users::whereNull("deleted_at")->get();
            foreach ($ussers as $u) {
                if (($u->details)['f_nac'] == date("Y-m-d")) {
                    $alert = Alert::create([
                        'type' => "bg-green",
                        'status_id' => $user->id_status,
                        'user_id' =>$user->id,
                        'nota' => "Cumpleaños!!! ".$u->nama." cumple años hoy ".date("Y-m-d"),
                        'created_id' => Auth::user()->id
                    ]);
                }
            }
        }

        if ($user->id_status == 2) {
            $products = $products->all();
            $products = $products->where("stok",">",0);
            foreach ($products as $p) {
                $masuks = $p->produkmasuks->where("f_venc","<=",date("Y-m-d"));
                foreach ($masuks as $m) {
                    $users = Users::where("id_status",2)->whereNull("deleted_at")->get();
                    foreach ($users as $k) {
                        $alert = $alert->where("produkmasuk_id",$m->id)->where("created_id",$user->id)->get()->first();
                        if (!$alert) {
                            $alert = Alert::create([
                                'type' => "bg-red",
                                'status_id' => $k->id_status,
                                'user_id' =>$k->id,
                                'produkmasuk_id' =>$m->id,
                                'nota' => "Un lote del producto ".$p->nama." está vencido, compruebe si ya no tiene productos de este lote ".$m->f_venc,
                                'created_id' => Auth::user()->id
                            ]);
                        }
                    }
                }
                $masuks = $p->produkmasuks->where("f_venc","<=",date("Y-m-d", strtotime('+'.$p->venc_min.' day' , strtotime(date("Y-m-d")))))->where("f_venc",">",date("Y-m-d"));
                foreach ($masuks as $m) {
                    $users = Users::where("id_status",2)->get();
                    foreach ($users as $k) {
                        $alert = $alert->where("produkmasuk_id",$m->id)->where("created_id",$user->id)->get()->first();
                        if (!$alert) {
                            $alert = Alert::create([
                                'type' => "bg-yellow",
                                'status_id' => $k->id_status,
                                'user_id' =>$k->id,
                                'produkmasuk_id' =>$m->id,
                                'nota' => "Un lote del producto ".$p->nama." está por vencer, compruebe si ya no tiene productos de este lote ".$m->f_venc,
                                'created_id' => Auth::user()->id
                            ]);
                        }
                    }
                }
            }
        }
        
        return Response()->json([
            'intended' => URL::route('dashboard')
        ], 201);

        //redirect()->intended('', 201, )

            //http-equiv="refresh
        //return redirect()->to('dashboard');//redirect()->intended(URL::route('dashboard'));
//        $auth = false;
//        $credentials = $request->only('username', 'password');
////
////        if (Auth::attempt($credentials, $request->has('remember'))) {
////            $auth = true; // Success
////        }
////
////        if ($request->ajax()) {
//////            return response()->json([
//////                'auth' => $auth,
//////                'intended' => URL::previous()
//////            ]);
////
////            return redirect()->route('dashboard');
////        } else {
////            return redirect()->intended(URL::route('dashboard'));
////        }
////        return redirect(URL::route('login'));
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }

    public function c_activator(Request $request){
        //return $request;
        if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) ){
            $method=$_POST['method'];
            if($method == 'online'){

                $nama=$_POST['nama'];
                $password=$_POST['password'];

                if(strtoupper(PHP_OS) == strtoupper("LINUX")){
                    $ds=shell_exec('udevadm info --query=all --name=/dev/sda | grep ID_SERIAL_SHORT');
                    $serialx = explode("=", $ds);
                    $serial = $serialx[1];
                }else {
                    function GetVolumeLabel($drive){
                        if (preg_match('#mero de serie del volumen es: (.*)\n#i', shell_exec('dir '.$drive.':'), $m)){
                            $volname = ' ('.$m[1].')';
                        }else {
                            $volname = '';
                        }
                        return $volname;
                    }
                    $serial = str_replace("(","",str_replace(")","",GetVolumeLabel("c")));
                }

                $url=$_POST['url'];
                $ch = curl_init();  
                curl_setopt($ch,CURLOPT_URL,$url.'/index.php?nama='.$nama.'&password='.$password.'&serial='.trim($serial));
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                $output=curl_exec($ch);
                curl_close($ch);

                $array= array();
                if($output == 'error'){
                    $array['value'] = false;
                }else{
                    $content='empty';
                    $fp = fopen($output.'.key',"wb");
                    fwrite($fp,$content);
                    fclose($fp);
                    $array['value'] = true;
                }
                return json_encode($array);
            }
            

        } else {
            exit('No direct access allowed.');
        }
    }
}
