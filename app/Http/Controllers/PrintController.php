<?php

namespace App\Http\Controllers;
use App\Transaksi;
use Illuminate\Http\Request;

use Mike42\Escpos\Printer; 
use Mike42\Escpos\EscposImage; 
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

class PrintController extends Controller
{

    public function print(Request $request)
    {
        $transaksi = Transaksi::find($request->id);

        try {
            $connector = new WindowsPrintConnector(config('app.impresora'));
            $printer = new Printer($connector);
            /*
                Vamos a imprimir un logotipo
                opcional. Recuerda que esto
                no funcionará en todas las
                impresoras

                Pequeña nota: Es recomendable que la imagen no sea
                transparente (aunque sea png hay que quitar el canal alfa)
                y que tenga una resolución baja. En mi caso
                la imagen que uso es de 250 x 250
            */

            # Vamos a alinear al centro lo próximo que imprimamos
            $printer->setJustification(Printer::JUSTIFY_CENTER);

            /*
                Intentaremos cargar e imprimir
                el logo
            */
            try{
                $logo = EscposImage::load('img/avatar.png', false);
                $printer->bitImage($logo);
            }catch(Exception $e){/*No hacemos nada si hay error*/}

            /*
                Ahora vamos a imprimir un encabezado
            */

            $printer->text("\n".config('app.name')."\n");
            $printer->text("Direccion: ".config('app.direccion')."\n");
            $printer->text("Tel: ".config('app.tlf')."\n");
            #La fecha también
            date_default_timezone_set("America/Lima");
            $printer->text(date("Y-m-d H:i:s") . "\n");
            $printer->text("-----------------------------" . "\n");
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->text("CANT  DESCRIPCION    P.U   IMP.\n");
            $printer->text("-----------------------------"."\n");
            /*
                Ahora vamos a imprimir los
                productos
            */
                /*Alinear a la izquierda para la cantidad y el nombre*/
                foreach ($transaksi->transaksidetails as $detalle) {
                    $printer->setJustification(Printer::JUSTIFY_LEFT);
                    $printer->text($detalle->produk->nama."\n");
                    $printer->text( $detalle->qty." ".$detalle->satuan." ".$detalle->pu." ".($detalle->qty*$detalle->pu)."   \n");
                }
                
            /*
                Terminamos de imprimir
                los productos, ahora va el total
            */
            $printer->text("-----------------------------"."\n");
            $printer->setJustification(Printer::JUSTIFY_RIGHT);
            $printer->text("SUBTOTAL: S/. ".($transaksi->bayar-$transaksi->kembalian-$transaksi->igv)."\n");
            $printer->text("IGV: ".$transaksi->igv."\n");
            $printer->text("TOTAL: ".($transaksi->bayar-$transaksi->kembalian)."\n");


            /*
                Podemos poner también un pie de página
            */
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text("Muchas gracias por su compra\n");



            /*Alimentamos el papel 3 veces*/
            $printer->feed(3);

            /*
                Cortamos el papel. Si nuestra impresora
                no tiene soporte para ello, no generará
                ningún error
            */
            $printer->cut();

            /*
                Por medio de la impresora mandamos un pulso.
                Esto es útil cuando la tenemos conectada
                por ejemplo a un cajón
            */
            $printer->pulse();

            /*
                Para imprimir realmente, tenemos que "cerrar"
                la conexión con la impresora. Recuerda incluir esto al final de todos los archivos
            */
            $printer->close();

        } catch(Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
