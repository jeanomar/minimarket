<?php

namespace App\Http\Controllers;
use App\Produk;
use App\ProdukDetail;
use App\ProdukKeluar;
use App\Transformers\ProdukKeluarTransformer;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Spatie\Fractal\ArraySerializer;

class StockOutController extends Controller
{
    public function index()
    {
        return view('stockout');
    }

    public function create(Requests\StockRequest $request, ProdukKeluar $keluar, Produk $produk)
    {
        try{

            $produk = $produk->where('kode', $request->kode)->first();
            $kel= \DB::select('SELECT * FROM produk_masuk u where u.id_produk ='.$produk->id.' order by id desc');
            $sum=0;
            $harga_comp=0;
            $produkmasuk_id = 0;
            foreach ($kel as $k) {
                $sum += $k->stok;
                if($produk->stok <= $sum){
                    $harga_comp=$k->harga;
                    $produkmasuk_id = $k->id;
                }
            }
            $prod = Produk::find($produk->id);
            
            $keluar = $keluar->create([
                //'tanggal' => $request->tanggal,
                'tanggal' => date("Y-m-d H:i:s"),
                'harga' => $harga_comp,
                'produkmasuk_id' => ($produkmasuk_id > 0)?$produkmasuk_id:null,
                'stok' => $request->stok,
                'satuan' => $prod->satuan->nama,
                'id_produk' => $produk->id,
                'id_produk_detail' => $request->detailitem,
                //'id_user' => Auth::user()->id
            ]);
            if($keluar){
                return Response()
                    ->json([
                        'type' => 'success',
                        'title' => 'Agregado con éxito',
                        'text' => $request->kode.", ".$request->stok.', '.$request->detailitem
                    ], 201);
            }else{
                return Response()
                    ->json([
                        'type' => 'error',
                        'title' => 'No se pudo agregar',
                        'text' => $request->kode
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    //'title' => $e->getMessage(),
                    'title' => 'No se pudo agregar',
                    'text' => $e->getSql()
                ], 501);
        }
    }

    public function show(ProdukKeluar $keluar)
    {
        //$keluar = $keluar->orderByTanggalDesc()->get();
        $keluar = $keluar->get()->sortByDesc('id');

        return fractal()
            ->collection($keluar)
            ->transformWith(new ProdukKeluarTransformer())
            ->toArray();
    }

    public function caridetailitem(Request $request)
    {
        $q = $request->q;
        $manager = new Manager();
        $manager->setSerializer(new ArraySerializer());

        $detail = new ProdukDetail();

        $resource = new Collection($detail->whereIn('status', ['2', '3'])->get()->toArray(), function(array $dp){
            return [
                'id'        => $dp['id'],
                'nama'      => $dp['nama'],
                'deskripsi' => $dp['deskripsi'],
            ];
        });

        $json = $manager->createData($resource)->toArray();

        $filtered = $json;
        if(strlen($q)) {
            $filtered = array_filter($json, function ($val) use ($q) {
                if (stripos($val['nama'], $q) !== false) {
                    return true;
                } else {
                    return false;
                }
            });
        }

        echo json_encode(array_slice(array_values($filtered), 0, 20));
    }
}
