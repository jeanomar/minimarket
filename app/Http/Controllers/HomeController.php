<?php

namespace App\Http\Controllers;

use App\Http\Requests\AlertRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Alert;
use App\Users;
use App\ProdukMasuk;
use App\Transaksi;

class HomeController extends Controller
{
    public function index()
    {
        $users_to = Users::all();
        $transaction =  Transaksi::selectRaw('sum(bayar-kembalian) as price')
                        ->where('state_tran','AC')
                        ->selectRaw('date(tanggal) as tanggals')
                        ->groupBy('tanggals')
                        ->orderBY('tanggals')
                        ->get();
        $orders =  Transaksi::selectRaw('count(id) as cant')
                        ->where('state_tran','AC')
                        ->selectRaw('date(tanggal) as tanggals')
                        ->groupBy('tanggals')
                        ->orderBY('tanggals')
                        ->get();

        $compras =  ProdukMasuk::selectRaw('sum(harga * stok) as price')
                        ->selectRaw('date(tanggal) as tanggals')
                        ->groupBy('tanggals')
                        ->orderBY('tanggals')
                        ->get();

        $data = [
            'transaction' => $transaction,
            'orders' => $orders,
            'compras' => $compras
        ];
        return view('dashboard',compact("users_to"))->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function notif_state(Request $request, Alert $alert)
    {
        try{
            $alert = $alert->findOrFail($request->id);
            $alert ->state = $request->state;
        if($alert->save()){
                return Response()
                    ->json([
                        'type' => 'success',
                        'title' => 'Cambió exitosamente',
                        'text' => 'Notificación: '.$request->id.(($alert->state == "IN")?" Visto":" Sin leer"),
                        'types' => $alert->state
                    ], 202);
            }else{
                return Response()
                    ->json([
                        'type' => 'error',
                        'title' => 'No se pudo hacer el cambio',
                        'text' => $request->id
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    'title' => 'No se pudo hacer el cambio',
                    'text' => $e->getMessage(),
                ],501);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_notif(AlertRequest $request, Alert $alert)
    {
        try{
            $user = Users::findOrFail($request->user_to); 
            $alert = $alert->create([
                'type' => "bg-aqua",
                'status_id' => $user->id_status,
                'user_id' =>$request->user_to,
                'nota' => $request->nota,
                'created_id' => Auth::user()->id
            ]);
            if($alert){
                return Response()
                    ->json([
                        'type' => 'success',
                        'title' => 'La alerta se registro exitosamente',
                        'text' => $user->nama.", ".$request->nota,
                        'types' => $alert->state
                    ], 202);
            }else{
                return Response()
                    ->json([
                        'type' => 'error',
                        'title' => 'No se pudo hacer el cambio',
                        'text' => $request->nota
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    'title' => 'No se pudo hacer el cambio',
                    'text' => $e->getMessage(),
                ],501);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
