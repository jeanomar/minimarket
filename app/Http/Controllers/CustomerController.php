<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Transformers\PelangganTranformer;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use Spatie\Fractal\ArraySerializer;
use League\Fractal\Resource\Collection;
use App\Http\Requests;
use App\Http\Requests\CustomerRequest;
use App\Pelanggan;
use Psy\Exception\ErrorException;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends Controller
{
    public function index()
    {
        return view('customer');
    }

    //Tambah
    public function create(CustomerRequest $request, Pelanggan $pelanggan)
    {
        try{
            $result = $pelanggan->create([
                'id' => $request->id,
                'nama' => $request->nama,
                'kode' => $request->kode,
                'jenis_kelamin' => $request->jenis_kelamin,
                'telepon' => $request->telepon,
                'alamat' => $request->alamat
            ], 201);

            if($result){
                return Response()->json([
                    'type' => 'success',
                    'title' => 'Agregado con éxito',
                    'text' => $request->nama
                ], 201);
            }else{
                return Response()->json([
                    'type' => 'error',
                    'title' => 'No se pudo agregar',
                    'text' => $request->nama
                ], 406);
            }
        }catch (QueryException $e){
            return Response()->json([
                'type' => 'error',
                //'title' => $e->errorInfo,
                'title' => 'No se pudo agregar',
                'text' => $e->getMessage()
            ], 501);
        }
    }

    //Table
    public function show(Pelanggan $pelanggan)
    {
        try{
            $pelanggan = $pelanggan->all();
            return fractal()
                ->collection($pelanggan)
                ->transformWith(new PelangganTranformer)
                ->toArray();
        }catch (QueryException $e){
            return Response()->json([
                'type' => 'error',
                'title' => $e->errorInfo,
                'text' => $e->getMessage()
            ], 501);
        }
    }

    //Edit
    public function update(CustomerRequest $request, Pelanggan $pelanggan)
    {
        try{
            $update = $pelanggan->findOrFail($request->id);

            $update->nama = $request->nama;
             $update->kode = $request->kode;
            $update->jenis_kelamin = $request->jenis_kelamin;
            $update->alamat = $request->alamat;
            $update->telepon = $request->telepon;

            if($update->save()){
                return Response()->json([
                    'type' => 'success',
                    'title' => 'Cambió exitosamente',
                    'text' => $request->nama
                ], 202);
            }else{
                return Response()->json([
                    'type' => 'error',
                    'title' => 'No se pudo hacer el cambio',
                    'text' => $request->nama
                ], 406);
            }
        }catch (QueryException $e){
            return Response()->json([
                'type' => 'error',
                //'title' => $e->errorInfo,
                'title' => 'No se pudo hacer el cambio',
                'text' => $e->getMessage()
            ], 501);
        }
    }

    //Hapus
    public function destroy(Request $request, Pelanggan $pelanggan)
    {
        try{
            $pelanggan = $pelanggan->findOrFail($request->id);
            $name = $pelanggan->nama;
            if($pelanggan->delete()){
                return Response()->json([
                    'type' => 'success',
                    'title' => 'Eliminado exitosamente',
                    'text' => $name
                ], 202);
            }else{
                return Response()->json([
                    'type' => 'error',
                    'title' => 'No se pudo eliminar',
                    'text' => $name
                ], 406);
            }
        }catch (QueryException $e){
            return Response()->json([
                'type' => 'error',
                //'title' => $e->errorInfo,
                'title' => 'No se pudo eliminar',
                'text' => $e->getMessage()
            ], 501);
        }

    }

    public function caricustomer(Request $request)
    {
        $q = $request->q;
        $manager = new Manager();
        $manager->setSerializer(new ArraySerializer());

        $pelanggan = new Pelanggan();

        $resource = new Collection($pelanggan->all()->toArray(), function(array $book) {
            return [
                'id'        => $book['id'],
                'nama'      => $book['nama'],
                'kode'      => $book['kode']
            ];
        });

        $json = $manager->createData($resource)->toArray();

        $filtered = $json;
        if(strlen($q)) {
            $filtered = array_filter($json, function ($val) use ($q) {
                if (stripos($val['nama'], $q) !== false || stripos($val['kode'], $q) !== false) {
                    return true;
                } else {
                    return false;
                }
            });
        }

        echo json_encode(array_slice(array_values($filtered), 0, 20));
    }
}
