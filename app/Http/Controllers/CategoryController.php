<?php

namespace App\Http\Controllers;
use App\Transformers\ProdukKategoriTransformer;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CategoryRequest;
use App\ProdukKategori;
use Mockery\CountValidator\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('category');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CategoryRequest $request, ProdukKategori $kategori)
    {
        try{
            $kategori =$kategori->create([
                'nama' => $request->nama,
                'keterangan' => $request->keterangan,
                'user_id' => Auth::user()->id
            ]);
            if($kategori){
                return Response()
                    ->json([
                        'type' => 'success',
                        'title' => 'Agregado con éxito',
                        'text' => $request->nama.', '.$request->keterangan
                    ], 201);
            }else{
                return Response()
                    ->json([
                        'type' => 'error',
                        'title' => 'No se pudo agregar',
                        'text' => $request->nama.', '.$request->keterangan
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    'title' => $e->errorInfo,
                    'text' => $e->getMessage()
                ], 501 );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ProdukKategori $kategori)
    {
        $kategori = $kategori->all();

        return fractal()
            ->collection($kategori)
            ->transformWith(new ProdukKategoriTransformer)
            ->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProdukKategori $kategori)
    {   
        $this->validate($request, [
            'nama' => 'required|unique:produk_kategori|min:1|max:6',
            'keterangan' => 'required|max:20'
            ],
            [],
            [
            'nama' => 'Símbolo',
            'keterangan' => 'Nombre'
            ]
        );

        try{
            $category = $kategori->findOrFail($request->id);
            $category->nama = $request->nama;
            $category->keterangan = $request->keterangan;
            $category->user_id = Auth::user()->id;
            if($category->save()){
                return Response()->json([
                    'type'=>'success',
                    'title'=> 'Cambió exitosamente',
                    'text'=> $request->nama
                ], 202);
            }else{
                return Response()->json([
                    'type'=>'error',
                    'title'=>'No se pudo hacer el cambio',
                    'text'=> $request->nama
                ], 406);
            }
        }catch (QueryException $e){
            return Response()->json([
                'type'=>'error',
                'title'=>'No se pudo hacer el cambio',
                'text'=> $e->getMessage()
            ], 501);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ProdukKategori $kategori)
    {
        try{
            $category = $kategori->findOrFail($request->id);
            $name=$category->nama;
            if($category->delete()){
                return Response()->json([
                    'type' => 'success',
                    'title' => 'Eliminado exitosamente',
                    'text' => $name
                ], 202);
            }else{
                return Response()->json([
                    'type' => 'error',
                    'title' => 'No se pudo eliminar',
                    'text' => $name
                ], 406);
            }
        }catch (QueryException $e){
            return Response()->json([
                'type' => 'error',
                'title' => 'No se pudo eliminar',
                'text' => $e->getMessage()
            ], 501);
        }
    }
}
