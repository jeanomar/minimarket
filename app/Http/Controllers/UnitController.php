<?php

namespace App\Http\Controllers;
use App\ProdukSatuan;
use App\Transformers\ProdukSatuanTransformer;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UnitRequest;
use Illuminate\Http\Response;
use Mockery\CountValidator\Exception;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('unit');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(UnitRequest $request, ProdukSatuan $produkSatuan)
    {
        try{
            $produkSatuan->create([
                'nama' => $request->nama,
                'keterangan' => $request->keterangan,
                'user_id' => Auth::user()->id
            ]);
            if($produkSatuan){
                return Response()->json([
                    'type'=>'success',
                    'title'=>'Agregado con éxito',
                    'text'=> $request->nama
                ], 201);
            }else{
                return Response()->json([
                    'type'=>'error',
                    'type'=>'No se pudo agregar',
                    'text'=> $request->nama
                ], 406);
            }
        }catch (QueryException $e){
            return Response()->json([
                'type'=>'error',
                'text'=> $e->getMessage()
            ], 501);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ProdukSatuan $satuan)
    {
        $units = $satuan->all();

        return fractal()
            ->collection($units)
            ->transformWith(new ProdukSatuanTransformer())
            ->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProdukSatuan $satuan)
    {
        $this->validate($request, [
                'nama' => 'required:unique:produk_satuan|min:1|max:6',
                'keterangan' => 'required|max:20'
            ],
            [],
            [
                'nama' => 'Abreviatura',
                'keterangan' => 'Nombre'
            ]
        );
        try{
            $update = $satuan->findOrFail($request->id);
            $update->nama = $request->nama;
            $update->keterangan = $request->keterangan;
            $category->user_id = Auth::user()->id;

            if($update->save()){
                return Response()->json([
                    'type'=>'success',
                    'title'=>'Cambió exitosamente',
                    'text'=> $request->nama
                ], 202);
            }else{
                return Response()->json([
                    'type'=>'error',
                    'title'=>'No se pudo hacer el cambio',
                    'text'=> $request->nama
                ], 406);
            }
        }catch (QueryException $e){
            return Response()->json([
                'type'=>'error',
                'title'=>'No se pudo hacer el cambio',
                'text'=> $e->getMessage()
            ], 501);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ProdukSatuan $ps)
    {
        try{
            $produk = $ps->findOrFail($request->id);
            if($produk->delete()){
                return Response()->json([
                    'type'=>'success',
                    'title'=>'Eliminado exitosamente',
                    'text'=> $request->nama
                ], 202);
            }else{
                return Response()->json([
                    'type'=>'error',
                    'title'=> 'No se pudo eliminar',
                    'text'=> $request->nama
                ], 406);
            }
        }catch (QueryException $e) {
            return Response()->json([
                'type' => 'error',
                //'title' => $e->errorInfo,
                 'title'=> 'No se pudo eliminar',
                'text' => $e->getMessage()
            ], 501);
        }
    }
}
