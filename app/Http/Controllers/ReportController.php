<?php

namespace App\Http\Controllers;
use App\Pelanggan;
use App\Supplier;
use App\Produk;
use App\ProdukKeluar;
use App\ProdukDetail;
use App\ProdukMasuk;
use App\Transaksi;
use App\TransaksiDetail;
use App\ProdukKategori;
use App\Transformers\ProdukKeluarTransformer;
use App\Transformers\ProdukMasukTransformer;
use App\Transformers\TransaksiTransformer;
use App\Transformers\TransaksiDetTransformer;
use App\Users;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade as PDF;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sales()
    {
        return view('reportsales');
    }
    public function datareportsales(Request $request, Transaksi $transaksi)
    {

        if($request->tanggal || $request->cliente || $request->transac || $request->user){
            if ($request->tanggal) {
                $tanggal = explode('#', $request->tanggal);
                $transaksi = $transaksi->where('tanggal', '>=', $tanggal[0]." 00:00:00")->where('tanggal', '<=', $tanggal[1]." 23:59:59");
            }
            if ( $request->cliente) {
                $transaksi = $transaksi->whereIn('id_pelanggan',Pelanggan::select("id")->where('nama', 'like', "%".$request->cliente."%")->get());
            }
            if ( $request->user) {
                $transaksi = $transaksi->whereIn('id_user',Users::select("id")->where('nama', 'like', "%".$request->user."%")->get());
            }
            if ( $request->transac) {
                $transaksi = $transaksi->where('kode', 'like', "%".$request->transac."%");
            }
            $transaksi = $transaksi->get()->sortByDesc('id');
        }else{
            $transaksi = $transaksi->all()->sortByDesc('id');
        }

        return fractal()
            ->collection($transaksi)
            ->transformWith(new TransaksiTransformer())
            ->includeItems()
            ->toArray();
    }

    public function salespdf(Request $request, Transaksi $sales)
    {
        //$sales = new Users();
        $sales = $sales::find($request->id);

        //return view('downreportsales',compact('sales'));
        $pdf = PDF::loadView('downreportsales',compact('sales'));

        return $pdf->download($sales->kode.'.pdf');
    }

    public function hapus(Request $request)
    {
         
        $this->validate($request, [
                'nota' => 'required|min:1'
            ],
            [
                'required' => ':attribute está vacío'
            ],
            [
                'nota' => 'Motivo'
            ]
        );

        try{
            $transac = Transaksi::findOrFail($request->id);
            $transac->state_tran = "IN";
            $transac->nota = $request->nota;
            $transac->destroy_user_id = Auth::user()->id;
            $transac->destroy_tanggal = date("Y-m-d H:i:s");
            
            /*foreach ($transac->transaksidetails  as $detail) {
                $masuk =  new ProdukMasuk;
                $masuk->tanggal = date("Y-m-d H:i:s");
                $masuk->f_venc =$detail->f_venc;
                $masuk->stok = $detail->qty;
                $masuk->satuan = $detail->satuan;
                $masuk->harga = $detail->harga_comp;
                $masuk->id_produk = $detail->id_produk;
                $masuk->id_produk_detail = 6;
                $masuk->id_user = Auth::user()->id;
                $masuk->save();
            }*/

            $keluar =  \DB::update('update produk_keluar set state = "IN" where transaksi_id = :id', ['id' => $transac->id]);

            if( $transac->save()){
                return Response()
                    ->json([
                        'type'=> 'success',
                        'title'=> 'Anulado exitosamente',
                        'text'=>  $transac->kode
                    ], 201);
            }else{
                return Response()
                    ->json([
                        'type'=> 'error',
                        'title'=> 'No se pudo eliminar',
                        'text'=>  $transac->nama
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type'=> 'error',
                    'title'=> 'No se pudo eliminar',
                    'text'=>  $e->getMessage()
                ], 501);
        }
    }

    public function salesexcel()
    {
        $sales = new Users();
        $sales = $sales->all()->toArray();
        Excel::create('reposetsales', function($excel) use ($sales){
            $excel->setTitle('Laporan Penjualan');

            $excel->sheet('laporan', function($sheet) use ($sales){
//                $sheet->setPageMargin(array(
//                    0.25, 0.30, 0.25, 0.30
//                ));
                $sheet->setOrientation('landscape');
                $sheet->fromArray($sales, null, 'A1', true);

            });
        })->export('xlsx');
    }

    public function stockin()
    {
        return view('reportstockin');
    }

    public function datareportstockin(Request $request, ProdukMasuk $pm)
    {

        //$masuk = $pm->orderByTanggalDesc()->get();
        if($request->tanggal || $request->pelanggan || $request->nama || $request->nota){
            if ($request->tanggal) {
                $tanggal = explode('#', $request->tanggal);
                $pm = $pm->where('tanggal', '>=', $tanggal[0]." 00:00:00")->where('tanggal', '<=', $tanggal[1]." 23:59:59");
            }
            if ($request->pelanggan) { 
                $pm = $pm->whereIn('id_supplier',Supplier::select("id")->where('nama', 'like', "%".$request->pelanggan."%")->get());
            }
            if ($request->nama) {
                $resp = \DB::select('SELECT u.id FROM produk u where u.nama like "%'.$request->nama.'%"');
                $select = array();
                foreach ($resp as $k) {
                    $select[]=$k->id;
                }
                $pm = $pm->whereIn('id_produk',$select);
            }
            if ($request->nota) {
                $resp = \DB::select('SELECT u.id FROM produk u where u.kode like "%'.$request->nota.'%"');
                $select = array();
                foreach ($resp as $k) {
                    $select[]=$k->id;
                }
                $pm = $pm->whereIn('id_produk',$select);
            }
            $masuk = $pm->get()->sortByDesc('id');
        }else{
            $masuk = $pm->get()->sortByDesc('id');
        }
        
        return fractal()
            ->collection($masuk)
            ->transformWith(new ProdukMasukTransformer())
            ->toArray();
    }

    public function stockout()
    {
        return view('reportstockout');
    }

    public function datareportstockout(Request $request, ProdukKeluar $keluar)
    {   
        if($request->kode || $request->produk || $request->tanggal || $request->detail){
            if ($request->tanggal) {
                $tanggal = explode('#', $request->tanggal);
                $keluar = $keluar->where('tanggal', '>=', $tanggal[0]." 00:00:00")->where('tanggal', '<=', $tanggal[1]." 23:59:59");
            }
            if ($request->detail) {
                $keluar = $keluar->whereIn('id_produk_detail',ProdukDetail::select("id")->where('nama', 'like', "%".$request->detail."%")->get());
            }
            if ($request->produk) {
                $resp = \DB::select('SELECT u.id FROM produk u where u.nama like "%'.$request->produk.'%"');
                $select = array();
                foreach ($resp as $k) {
                    $select[]=$k->id;
                }
                $keluar = $keluar->whereIn('id_produk',$select);
            }
            if ($request->kode) {
                $resp = \DB::select('SELECT u.id FROM produk u where u.kode like "%'.$request->kode.'%"');
                $select = array();
                foreach ($resp as $k) {
                    $select[]=$k->id;
                }
                $keluar = $keluar->whereIn('id_produk',$select);
            }
            $keluar = $keluar->get()->sortByDesc('id');
        }else{
            $keluar = $keluar->get()->sortByDesc('id');
        }

        //$keluar = $keluar->get()->orderByTanggalDesc();
        
        return fractal()
            ->collection($keluar)
            ->transformWith(new ProdukKeluarTransformer())
            ->toArray();
    }

    public function money()
    {
        return view('report-money');
    }

    public function datareportmoney(Request $request, TransaksiDetail $tdet)
    {   
        if($request->kode || $request->produk || $request->tanggal || $request->kategori){
            if ($request->tanggal) {
                $tanggal = explode('#', $request->tanggal);
                $resp = \DB::select('SELECT p.id from transaksi p where p.tanggal >= "'. $tanggal[0].' 00:00:00" and  p.tanggal <= "'.$tanggal[1].' 23:59:59"');
                $select = array();
                foreach ($resp as $k) {
                    $select[]=$k->id;
                }
                $tdet = $tdet->whereIn('id_transaksi', $select);
            }
            if ($request->kategori) {

                $resp = \DB::select('SELECT p.id from produk p INNER JOIN produk_kategori k on k.id=p.id_kategori where k.keterangan LIKE "%'.$request->kategori.'%"');
                $select = array();
                foreach ($resp as $k) {
                    $select[]=$k->id;
                }
                $tdet = $tdet->whereIn('id_produk', $select);
            }
            if ($request->produk) {
                $resp = \DB::select('SELECT u.id FROM produk u where u.nama like "%'.$request->produk.'%"');
                $select = array();
                foreach ($resp as $k) {
                    $select[]=$k->id;
                }
                $tdet = $tdet->whereIn('id_produk',$select);
            }
            if ($request->kode) {
                $resp = \DB::select('SELECT u.id FROM transaksi u where u.kode like "%'.$request->kode.'%"');
                $select = array();
                foreach ($resp as $k) {
                    $select[]=$k->id;
                }
                $tdet = $tdet->whereIn('id_transaksi',$select);
            }
            $tdet = $tdet->get()->sortByDesc('id');
        }else{
            $tdet = $tdet->get()->sortByDesc('id');
        }
        
        return fractal()
            ->collection($tdet)
            ->transformWith(new TransaksiDetTransformer())
            ->toArray();
    }

    public function gananciaspdf(Request $request, Transaksi $sales)
    {
        if($request->kode || $request->produk || $request->tanggal || $request->kategori){
            if ($request->tanggal) {
                $tanggal = explode('@', $request->tanggal);
                $sales   =  $sales::where('tanggal', '>=', $tanggal[0]." 00:00:00")->where('tanggal', '<=', $tanggal[1]." 23:59:59");
            }
            if ($request->kategori) {

                $resp = \DB::select('SELECT t.id FROM transaksi t INNER JOIN detail_transaksi d ON d.id_transaksi=t.id INNER JOIN produk p on p.id=d.id_produk INNER JOIN produk_kategori k on k.id=p.id_kategori WHERE k.keterangan LIKE "%'.$request->kategori.'%"');
                $select = array();
                foreach ($resp as $k) {
                    $select[]=$k->id;
                }
                $sales = $sales->whereIn('id', $select);
            }
            if ($request->produk) {
                $resp = \DB::select('SELECT t.id FROM transaksi t INNER JOIN detail_transaksi d ON d.id_transaksi=t.id INNER JOIN produk p on p.id=d.id_produk where p.nama like "%'.$request->produk.'%"');
                $select = array();
                foreach ($resp as $k) {
                    $select[]=$k->id;
                }
                $sales = $sales->whereIn('id',$select);
            }
            if ($request->kode) {
                $sales = $sales->where('kode','like','%'.$request->kode.'%');
            }
            $sales = $sales->get()->sortByDesc('id');
        }else{
            $sales = $sales->get()->sortByDesc('id');
        }

        //return view('downreportganancias',compact('sales'));
        $pdf = PDF::loadView('downreportganancias',compact('sales'));

        return $pdf->download('listado.pdf');
    }

    public function gananciasexcel()
    {
        $sales = \DB::select('SELECT t.* FROM users t');
        
        $sales= json_decode( json_encode($sales), true);
        //return $sales;
        Excel::create('reposetsales', function($excel) use ($sales){
            $excel->setTitle('Laporan Penjualan');

            $excel->sheet('laporan', function($sheet) use ($sales){
//                $sheet->setPageMargin(array(
//                    0.25, 0.30, 0.25, 0.30
//                ));
                $sheet->setOrientation('landscape');
                $sheet->fromArray((array)$sales, null, 'A1', true);

            });
        })->export('xlsx');
    }

}
