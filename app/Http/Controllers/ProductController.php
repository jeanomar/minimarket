<?php

namespace App\Http\Controllers;
use Croppa;
use File;
use FileUpload;
use App\Produk;
use App\ProdukSatuan;
use App\ProdukKategori;
use App\Repository;
use App\Transformers\ProdukSatuanTransformer;
use App\Transformers\ProdukTransformer;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Requests\ProductRequest;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Spatie\Fractal\ArraySerializer;

class ProductController extends Controller
{
    public function index()
    {
        return view('product');
    }
    
    public function create(ProductRequest $request, Produk $produk)
    {
        try{
            $product = $produk->create([
                'kode' => $request->kode,
                'nama' => $request->nama,
                'harga' => $request->harga,
                'deskripsi' => $request->deskripsi,
                'id_kategori' => $request->kategori,
                'id_satuan' => $request->satuan,
                'venc_min' => $request->venc_min,
                'stok_min' => $request->stok_min
            ]);

            if($product){
                return Response()
                    ->json([
                        'type'=> 'success',
                        'title'=> 'Agregado con éxito',
                        'text'=>  $request->nama
                    ], 201);
            }else{
                return Response()
                    ->json([
                        'type'=> 'error',
                        'title'=> 'No se pudo agregar',
                        'text'=>  $request->nama
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type'=> 'error',
                    'title'=> 'No se pudo agregar',
                    'text'=>  $e->getMessage()
                ], 501);
        }
    }
    
    public function productpilih(Produk $produk)
    {
        $product = $produk->get()->sortByDesc('stok');

        return fractal()
            ->collection($product)
            ->transformWith(function($product){
                return [
                    'kode' => $product->kode,
                    'nama' => $product->nama,
                    'harga' => number_format($product->harga,2,'.','.'),
                    'satuan' => $product->satuan->nama,
                    'stok' => $product->stok,
                    'pilih' => $product->pilih,
                ];
            })
            ->toArray();
    }

    public function productpilihrep(Produk $produk)
    {
        $product = $produk->get()->sortByDesc('stok');

        return fractal()
            ->collection($product)
            ->transformWith(function($product){
                return [
                    'kode' => $product->kode,
                    'nama' => $product->nama,
                    'harga' => number_format($product->harga,2,'.','.'),
                    'satuan' => $product->satuan->nama,
                    'stok' => $product->stokrep,
                    'pilih' => $product->pilih,
                ];
            })
            ->toArray();
    }

    public function productalm(Request $request, Repository $repos)
    {
        $resp = \DB::select('SELECT p.id from produk p where p.kode = "'.$request->kode.'"');
        $select = array();
        foreach ($resp as $k) {
            $select[]=$k->id;
        }
        $repos = $repos->where("type","INPUT")->where("id_produk", $select)->get()->sortByDesc('stok');
        return fractal()
            ->collection($repos)
            ->transformWith(function($repos){
                return [
                    'kode' => $repos->produk->kode,
                    'f_venc' => ($repos->f_venc)?$repos->f_venc:"Imperecible",
                    'stok' => ($repos->stok - Repository::where("id_repository_in",$repos->id)->where("type","OUTPUT")->sum('stok')).' '.$repos->satuan,
                    'action' => '<div class="btn-group pull-right">
                                    <input type="radio" title="Seleccionar" name="reposin" value="'.$repos->id.'">
                                </div>'
                ];
            })
            ->toArray();
    }

    
    public function show(Produk $produk)
    {
        $product = $produk->get()->sortByDesc('id');

        return fractal()
            ->collection($product)
            ->transformWith(new ProdukTransformer())
            ->toArray();
    }

    public function cari(Request $request, Produk $produk)
    {
        $produk = $produk->where('kode', $request->kode);
        $satuan = \DB::select('SELECT s.nama, u.nama as produk FROM produk u inner join produk_satuan s on s.id = u.id_satuan where u.kode = '.$request->kode);

        if($produk->count() == 1){
            return Response()
                ->json([
                    'type' => 'success',
                    'title' => '',
                    'text' => $request->kode.' existe en la base de datos',
                    "val" => $satuan[0]->nama,
                    "produk" => $satuan[0]->produk
                ], 200);
        }else if($produk->count() == null){
            return Response()
                ->json([
                    'type' => 'error',
                    'title' => '',
                    'text' => $request->kode.' no existe en la base de datos',
                ], 200);
        }
    }

    public function update(Request $request, Produk $product)
    {
        $this->validate($request, [
            'kode' => 'required',
            'nama' => 'required',
            'harga' => 'required:numeric|min:1',
            'deskripsi' => 'nullable',
            'venc_min' => 'required:numeric|min:1',
            'stok_min' => 'required:numeric|min:1'
            ],
            [],
            [
            'kode' => 'Código',
            'nama' => 'Nombre',
            'harga' => 'Precio',
            'deskripsi' => 'Descripción',
            'kategori' => 'Categoria',
            'satuan' => 'Unidad',
            'venc_min' => 'Notificación vencimiento',
            'stok_min' => 'Notificación stock'
            ]
        );
        try{
            $product = $product->findOrFail($request->id);

            $product->kode = $request->kode;
            $product->nama = $request->nama;
            $product->harga = $request->harga;
            $product->stok_min = $request->stok_min;
            $product->venc_min = $request->venc_min;
            $product->deskripsi = $request->deskripsi;
            $product->id_kategori = ($request->kategori) ?  $request->kategori : $product->id_kategori;
            $product->id_satuan = ($request->satuan) ? $request->satuan : $product->id_satuan;

            if($product->save()){
                return Response()
                    ->json([
                        'type'=> 'success',
                        'title'=> 'Cambió exitosamente',
                        'text'=>  $request->nama
                    ], 201);
            }else{
                return Response()
                    ->json([
                        'type'=> 'error',
                        'title'=> 'No se pudo hacer el cambio',
                        'text'=>  $request->nama
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type'=> 'error',
                    'title'=> 'No se pudo hacer el cambio',
                    'text'=>  $e->getMessage()
                ], 501);
        }
    }

    public function destroy(Request $request)
    {
        try{
            $product = Produk::where('kode', $request->kode)->first();

            if($product->delete()){
                return Response()
                    ->json([
                        'type'=> 'success',
                        'title'=> 'Eliminado exitosamente',
                        'text'=>  $request->nama
                    ], 201);
            }else{
                return Response()
                    ->json([
                        'type'=> 'error',
                        'title'=> 'No se pudo eliminar',
                        'text'=>  $request->nama
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type'=> 'error',
                    'title'=> 'No se pudo eliminar',
                    'text'=>  $e->getMessage()
                ], 501);
        }
    }

    public function getsatuan(Request $request){
        $q = $request->q;
        $kategori = ProdukSatuan::find($q);
        return $kategori->nama;
    }

    public function carisatuan(Request $request)
    {
        $q = $request->q;
        $manager = new Manager();
        $manager->setSerializer(new ArraySerializer());

        $satuan = new ProdukSatuan();

        $resource = new Collection($satuan->all()->toArray(), function(array $book) {
            return [
                'id'        => $book['id'],
                'nama'      => $book['nama'],
                'keterangan'=> $book['keterangan'],
            ];
        });

        $json = $manager->createData($resource)->toArray();

        $filtered = $json;
        if(strlen($q)) {
            $filtered = array_filter($json, function ($val) use ($q) {
                if (stripos($val['nama'], $q) !== false) {
                    return true;
                } else {
                    return false;
                }
            });
        }

        echo json_encode(array_slice(array_values($filtered), 0, 20));
    }

    public function getkategori(Request $request){
        $q = $request->q;
        $kategori = ProdukKategori::find($q);
        return $kategori->nama;
    }

    public function carikategori(Request $request)
    {
        $q = $request->q;
        $manager = new Manager();
        $manager->setSerializer(new ArraySerializer());

        $kategori = new ProdukKategori();

        $resource = new Collection($kategori->all()->toArray(), function(array $book) {
            return [
                'id'      => $book['id'],
                'nama'      => $book['nama'],
                'keterangan'   => $book['keterangan'],
            ];
        });

        $json = $manager->createData($resource)->toArray();

        $filtered = $json;
        if(strlen($q)) {
            $filtered = array_filter($json, function ($val) use ($q) {
                if (stripos($val['nama'], $q) !== false) {
                    return true;
                } else {
                    return false;
                }
            });
        }

        echo json_encode(array_slice(array_values($filtered), 0, 20));
    }

    public $folder = '/uploads/products';

    public function imgupload(Request $request)
    {
        // create upload path if it does not exist
        $path = public_path($this->folder."/");
        if(!File::exists($path)) {
            File::makeDirectory($path);
        };
        // Simple validation (max file size 2MB and only two allowed mime types)
        $validator = new FileUpload\Validator\Simple('2M', ['image/png', 'image/jpg', 'image/jpeg']);
        // Simple path resolver, where uploads will be put
        $pathresolver = new FileUpload\PathResolver\Simple($path);
        // The machine's filesystem
        $filesystem = new FileUpload\FileSystem\Simple();
        // FileUploader itself
        $fileupload = new FileUpload\FileUpload($_FILES['img'], $_SERVER);
        $slugGenerator = new FileUpload\FileNameGenerator\Slug();
        // Adding it all together. Note that you can use multiple validators or none at all
        $fileupload->setPathResolver($pathresolver);
        $fileupload->setFileSystem($filesystem);
        $fileupload->addValidator($validator);
        $fileupload->setFileNameGenerator($slugGenerator);
        
        // Doing the deed
        list($files, $headers) = $fileupload->processAll();
        // Outputting it, for example like this
        foreach($headers as $header => $value) {
            header($header . ': ' . $value);
        }
        foreach($files as $file){
            //Remember to check if the upload was completed
            if ($file->completed) {
                // set some data
                $filename = $file->getFilename();
                $url = $this->folder."/". $filename;
                
                // save data
                $produk = Produk::find($request->get('id'));
                $produk->url = $url;
                $produk->save();
                
                // output uploaded file response
                return $url;
            }
        }
        
        return response()->json(['files' => $files]);
    }
}
