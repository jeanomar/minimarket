<?php

namespace App\Http\Controllers;

use App\Supplier;
use App\Transformers\SupplierTransformer;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\SupplierRequest;
use Illuminate\Http\Response;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('supplier');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(SupplierRequest $request, Supplier $supplier)
    {
        try{
            $result = $supplier->create([
                'nama' => $request->nama,
                'alamat' => $request->alamat,
                'telepon' => $request->telepon,
                'deskripsi' => $request->deskripsi
            ]);
            if($result){
                return Response()
                    ->json([
                        'type' => 'success',
                        'title' => 'Agregado con éxito',
                        'text' => $request->nama
                    ], 201);
            }else{
                return Response()
                    ->json([
                        'type' => 'error',
                        'title' => 'No se pudo agregar',
                        'text' => $request->nama
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    'title' => 'No se pudo agregar',
                    'text' => $e->getMessage()
                ], 501);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        try{
            $supplier = $supplier->all();
            return fractal()
                ->collection($supplier)
                ->transformWith(new SupplierTransformer())
                ->toArray();
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    'title' => $e->errorInfo,
                    'text' => $e->getMessage()
                ], 501);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SupplierRequest $request, Supplier $supplier)
    {
        try{
            $update = $supplier->findOrFail($request->id);
            $update->nama = $request->nama;
            $update->alamat = $request->alamat;
            $update->telepon = $request->telepon;
            $update->deskripsi = $request->deskripsi;
            if($update->save()){
                return Response()
                    ->json([
                        'type' => 'success',
                        'title' => 'Cambió exitosamente',
                        'text' => $request->nama
                    ], 202);
            }else{
                return Response()
                    ->json([
                        'type' => 'error',
                        'title' => 'No se pudo hacer el cambio',
                        'text' => $request->nama
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    'title' => 'No se pudo hacer el cambio',
                    'text' => $e->getMessage(),
                ],501);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Supplier $supplier)
    {
        try{
            $supplier = $supplier->findOrFail($request->id);
            if($supplier->delete()){
                return Response()
                    ->json([
                        'type' => 'success',
                        'title' => 'Eliminado exitosamente',
                        'text' => $request->id
                    ], 201);
            }else{
                return Response()
                    ->json([
                        'type' => 'error',
                        'title' => 'No se pudo eliminar',
                        'text' => $request->id
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    'title' => 'No se pudo eliminar',
                    'text' => $e->getMessage()
                ], 501);
        }
    }
}
