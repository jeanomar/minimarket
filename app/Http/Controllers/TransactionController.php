<?php

namespace App\Http\Controllers;

use App\Alert;
use App\Users;
use App\Box;
use App\Produk;
use App\ProdukKeluar;
use App\ProdukMasuk;
use App\Transaksi;
use App\Variable;
use App\TransaksiDetail;
use App\Transformers\TransaksiDetTransformer;
use Carbon\Carbon;
use Faker\Provider\tr_TR\DateTime;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Spatie\Fractal\ArraySerializer;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Barryvdh\DomPDF\Facade as PDF;

class TransactionController extends Controller
{

    public function index()
    {
        if(Auth::user()->id == 3){
            $box = Box::where("user_id",Auth::user()->id)->where("created_at",">=", date("Y-m-d")." 00:00:00")->where("created_at","<=", date("Y-m-d")." 23:59:59")->get();
            if ($box->count("id")==0) {
                $state = "open";
                return view('openclosebox', compact("state"));
            }elseif($box[0]->estate == "open"){
                return view('transaction');
            }else{
                $state = "close";
                return view('openclosebox', compact("state"));
            }
            
        }else{
            return redirect("/");
        }
    }

    public function openbox()
    {
        $box = new Box();
        $box->user_id = Auth::user()->id;
        $box->save();
        return redirect('transaction');
    }

    public function closebox()
    {
        $box = Box::where("user_id",Auth::user()->id)->where("created_at",">=", date("Y-m-d")." 00:00:00")->where("created_at","<=", date("Y-m-d")." 23:59:59")->get()->first();

        $transac = Transaksi::where("id_user",Auth::user()->id)->where("tanggal",">=", date("Y-m-d")." 00:00:00")->where("tanggal","<=", date("Y-m-d")." 23:59:59")->update(['state_box' => "CE", 'box_tanggal' => date("Y-m-d H:i:s")]);

        $box->estate = "close";
        $box->save();
        return redirect('transaction');
    }

    public function printbox(Transaksi $sales)
    {
        $sales = $sales->where("id_user",Auth::user()->id)->where("tanggal",">=", date("Y-m-d")." 00:00:00")->where("tanggal","<=", date("Y-m-d")." 23:59:59")->get()->sortByDesc('id');
        //return view('downreportbox',compact('sales'));
        if (sizeof($sales)==0) {
            return "No ha realizado ventas el dia de hoy ".date("d-m-Y");
        }
        $pdf = PDF::loadView('downreportbox',compact('sales'));

        return $pdf->download('Caja_'.Auth::user()->username."_".date("d-m-Y").'.pdf');

    }

    public function getKodeTransaksi()
    {
        $transaksi = new Transaksi();
        $date = Carbon::now();
        $id = $transaksi->max('id');

        return Response()
            ->json([
                'kode' => sprintf('TR'.'%0' . 6 .'s-%s', ($id) ? intval($id) + 1 : 1, $date->format('Y/m/d')),
                'tgl' => $date->format('Y-m-d H:i:s'),
            ], 200);
    }

    public function caribyid(Request $request)
    {
        $this->validate($request, [
                'kode' => 'required|exists:produk'
            ],
            [],
            [
                'kode' => 'Código',
            ]
        );

        $result =  Produk::where('kode', $request->kode)->first();

        $kel= \DB::select('SELECT * FROM produk_masuk u where u.id_produk ='.$result->id.' order by id desc');
        $sum=0;
        $discount=0;
        $produk_masuk_id=0;

        foreach ($kel as $k) {
            $sum += $k->stok;
            if($result->stok <= $sum){
                $discount=$k->discount;
                $produk_masuk_id=$k->id;
            }
        }
        $result->discount=$discount;
        $result->produk_masuk_id=$produk_masuk_id;

        return fractal()
            ->item($result)
            ->transformWith(function($result){
                return [
                    'kode' => $result->kode,
                    'nama' => $result->nama,
                    'harga' => $result->harga,
                    'discount' => $result->discount,
                    'produk_masuk_id' => $result->produk_masuk_id,
                    'url' => $result->url,
                    'satuan' => $result->satuan->nama
                ];
            })->toArray();
    }

    public function bayar(Request $request, Produk $produk, Alert $alert)
    {
        $this->validate($request, [
            'bayar' => 'required',
            'kembalian' => 'required',
            'kode_transaksi' => 'required',
        ]);

        $var = Variable::where('name','igv')->where('state','AC')->get();

        if (sizeof($var)==0) {
            return Response()
                ->json([
                    'type' => 'error',
                    'title' => "Atención",
                    'text' => "El igv está desactivado, comuníquese con su Administrador para solucionarlo..."
                ], 501);
        }

        $cart = Cart::Content();

        $transaksi = new Transaksi();
        $transaksi->kode = $request->kode_transaksi;
        $transaksi->tanggal = date("Y-m-d H:i:s");
        $transaksi->id_user = Auth::user()->id;
        $transaksi->bayar = $request->bayar;
        $transaksi->kembalian = $request->kembalian;
        $transaksi->igv = ($request->bayar-$request->kembalian)* floatval($var[0]->value);
        $transaksi->id_pelanggan = ($request->customer) ? $request->customer : null;
        $transaksi->save();

        foreach($cart as $c)
        {
            $id_produk = $produk->where('kode', $c->id)->first();
            $kel= ProdukMasuk::find($c->options->pmid);

            $f_venc=$kel->f_venc;
            $harga_comp=$kel->harga;
            $produkmasuk_id = $kel->id;
            
            $transaksi->produks()->attach($id_produk, ['qty'=>$c->qty, 'satuan'=>$id_produk->satuan->nama, 'pu'=>$c->price,'harga_comp'=>($harga_comp>0) ? $harga_comp:null, 'f_venc'=>($f_venc!="")?$f_venc:null]);
            //$keluar->tanggal = ;
            $keluar = new ProdukKeluar;
            $keluar->stok = $c->qty;
            $keluar->harga = $c->price;
            if ($produkmasuk_id > 0) {
                $keluar->produkmasuk_id = $produkmasuk_id;
            }
            $keluar->satuan = $id_produk->satuan->nama;
            $keluar->id_produk = $produk->where('kode', $c->id)->pluck('id')->first();
            $keluar->id_produk_detail = 2;//Penjulan id(2) in Database
            $keluar->transaksi_id = $transaksi->id;
            $keluar->transaksi_kode = $transaksi->kode;
            $keluar->save();
            if($id_produk->stok <= 0){
                $users = Users::where("id_status",2)->get();
                foreach ($users as $k) {
                    $alert = $alert->create([
                        'type' => "bg-red",
                        'status_id' => $k->id_status,
                        'user_id' =>$k->id,
                        'nota' => "El producto ".$id_produk->nama." se terminó, quedan ". $id_produk->stok." ".$id_produk->satuan->nama,
                        'created_id' => Auth::user()->id
                    ]);
                }
            }elseif ($id_produk->stok_min >= $id_produk->stok) {
                $users = Users::where("id_status",2)->get();
                foreach ($users as $k) {
                    $alert = $alert->create([
                        'type' => "bg-yellow",
                        'status_id' => $k->id_status,
                        'user_id' =>$k->id,
                        'nota' => "El producto ".$id_produk->nama." se está terminando, quedan ". $id_produk->stok." ".$id_produk->satuan->nama,
                        'created_id' => Auth::user()->id
                    ]);
                }
            }
        }

        Cart::destroy();

        return Response()
            ->json([
                'type' => 'success',
                'title' => 'Transacción exitosa',
                'text' => 'Código de Transacción : '.$request->kode_transaksi,
                'resumen' => $transaksi,
                'detalle' => fractal()
                ->collection($transaksi->transaksidetails)
                ->transformWith(new TransaksiDetTransformer())
                ->toArray(),
                'comprador' => ($transaksi->pelanggan)?$transaksi->pelanggan->nama:""
            ], 201);


    }

    public function addcart(Request $request)
    {
        $produk = Produk::where('kode', $request->kode)->first();
        if ($produk->stok < $request->qty) {
            return Response()
                ->json([
                    'text' => 'La cantidad supera al stock en '.($request->qty-$produk->stok)
                ], 422);
        }
        $this->validate($request, [
            'kode' => 'required|exists:produk'
            ],
            [],
            [
                'kode' => 'Código'
            ]
        );
        try{
            $data = Cart::add($request->kode, $request->nama, $request->qty, $request->harga, ['pmid' => $request->pmid]);
            return Response()
                ->json([
                    'type' => 'success',
                    'title' => 'Cargado con éxito al carrito',
                    'text' => 'Código: '.$request->kode.', Producto: '.$request->nama.', Cantidad: '.$request->qty.', Precio: '.round($request->harga,2)
                ], 201);
        }
        catch (HttpException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    'title' => $e->getHeaders(),
                    'text' => $e->getMessage()
                ], 501);
        }
    }

    public function datacart()
    {
        $cart = Cart::Content();
        return fractal()
            ->collection($cart)
            ->transformWith(function($cart){
                return [
                    'kode' => $cart->id,
                    'nama' => $cart->name,
                    'qty' => $cart->qty,
                    'harga' => $this->curRuliah($cart->price),
                    'subharga' => $this->curRuliah($cart->price * $cart->qty),
                    'action' => '<button title="Quitar" class="btn btn-danger btn-xs pull-right" onclick="removeitem('."'".$cart->rowId."'".', '."'".$cart->name."'".')"><i class="fa fa-trash"></i></button>'
                ];
            })
            ->addMeta(['tagihan'=>Cart::subtotal()])
            ->toArray();
    }

    protected function curRuliah($value){
        return "S/.".number_format($value,2,'.','.');
    }

    public function deletecart()
    {
        $data = Cart::destroy();
        return Response()
            ->json([
                'type' => 'success',
                'title' => 'El carrito se restablecio correctamente',
                'text' => '',
            ], 201);
    }

    public function removeitem(Request $request)
    {
        $data = Cart::remove($request->rowId);;

        return Response()
            ->json([
                'type' => 'success',
                'title' => 'Eliminado con éxito',
                'text' => $request->nama,
            ], 201);
    }

}
