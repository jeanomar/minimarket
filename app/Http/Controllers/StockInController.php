<?php

namespace App\Http\Controllers;
use App\Produk;
use App\ProdukDetail;
use App\ProdukMasuk;
use App\Supplier;
use App\Transformers\ProdukMasukTransformer;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

use App\Http\Requests\StockRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Spatie\Fractal\ArraySerializer;

class StockInController extends Controller
{
    public function index()
    {
        return view('stockin');
    }

    public function create(StockRequest $request, ProdukMasuk $masuk, Produk $produk)
    {
        $this->validate($request, [
                'harga' => 'required|numeric',
                //'f_venc' => 'required'
            ],
            [],
            [
                'harga' => 'P. U. Compra S/.',
                //'f_venc' => 'Fecha de vencimiento'
            ]
        );
        try{
            $id_produk = $produk->where('kode', $request->kode)->pluck('id')->first();
            $prod = Produk::find($id_produk);

            $result = $masuk->create([
                //'tanggal' => $request->tanggal,
                'tanggal' => date("Y-m-d H:i:s"),
                'f_venc' => ($request->f_venc) ? $request->f_venc : null,
                'stok' => $request->stok,
                'satuan' => $prod->satuan->nama,
                'harga' => $request->harga,
                'id_produk' => $id_produk,
                'id_produk_detail' => $request->detailitem,
                'id_user' => Auth::user()->id,
                'id_supplier' => ($request->supplier) ? $request->supplier : null
            ]);
            if($result){
                return Response()
                    ->json([
                        'type'=> 'success',
                        'title' => 'Agregado con éxito',
                        'text' => $request->tanggal." ".$request->stok." ".$request->kode
                    ], 201);
            }else{
                return Response()
                    ->json([
                        'type'=> 'error',
                        'title' => 'No se pudo agregar',
                        'text' => $request->tanggal." ".$request->stok." ".$request->kode
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type' => 'error',
                    'title' => 'No se pudo agregar',
                    'text' => $e->getMessage()
                ], 501);
        }

    }

    public function show(ProdukMasuk $pm)
    {
        //$pm = $pm->orderByTanggalDesc()->get();
        $pm = $pm->get()->sortByDesc('id');
        return fractal()
            ->collection($pm)
            ->transformWith(new ProdukMasukTransformer())
            ->toArray();
    }

    public function caridetailitem(Request $request)
    {
        $q = $request->q;
        $manager = new Manager();
        $manager->setSerializer(new ArraySerializer());

        $detail = new ProdukDetail();

        $resource = new Collection($detail->whereIn('status', ['1', '3'])->get()->toArray(), function(array $dp){
            return [
                'id'        => $dp['id'],
                'nama'      => $dp['nama'],
                'deskripsi' => $dp['deskripsi'],
            ];
        });

        $json = $manager->createData($resource)->toArray();

        $filtered = $json;
        if(strlen($q)) {
            $filtered = array_filter($json, function ($val) use ($q) {
                if (stripos($val['nama'], $q) !== false) {
                    return true;
                } else {
                    return false;
                }
            });
        }

        echo json_encode(array_slice(array_values($filtered), 0, 20));
    }

    public function carisupplier(Request $request)
    {
        $q = $request->q;
        $manager = new Manager();
        $manager->setSerializer(new ArraySerializer());

        $supplier = new Supplier();


        $resource = new Collection($supplier->all()->toArray(), function(array $supplier){
            return [
                'id'        => $supplier['id'],
                'nama'      => $supplier['nama'],
                'deskripsi' => $supplier['deskripsi'],
            ];
        });

        $json = $manager->createData($resource)->toArray();

        $filtered = $json;
        if(strlen($q)) {
            $filtered = array_filter($json, function ($val) use ($q) {
                if (stripos($val['nama'], $q) !== false) {
                    return true;
                } else {
                    return false;
                }
            });
        }

        echo json_encode(array_slice(array_values($filtered), 0, 20));
    }

    public function discount()
    {
        return view('discount');
    }

    public function discountdata(ProdukMasuk $pm)
    {
        //$pm = $pm->orderByTanggalDesc()->get();
        $pm = $pm->where('discount','>',0)->get()->sortByDesc('id');
        return fractal()
            ->collection($pm)
            ->transformWith(function($pm){
                return [
                    'tanggal' => $pm->tanggal,
                    'f_venc' => (!$pm->f_venc)?'Imperecible':$pm->f_venc,
                    'stok' => $pm->stok*1,
                    'satuan' => $pm->satuan,
                    'harga' => 'S/. '.$pm->harga,
                    'discount' => 'S/. '.$pm->discount,
                    'kode' => $pm->produk->kode,
                    'nama' => $pm->produk->nama,
                    'detail' => $pm->produkdetail->nama,
                    'supplier' => (!$pm->id_supplier) ? '' : $pm->supplier->nama,
                    'quit' => $pm->quit
                ];
            })
            ->toArray();
        
    }

    public function stokpilih(ProdukMasuk $pm)
    {
        $pm = $pm->where('discount',0)->get()->where('stokf','>',0)->sortByDesc('id');
        
        return fractal()
            ->collection($pm)
            ->transformWith(function($pm){
                return [
                    'tanggal' => $pm->tanggal,
                    'f_venc' => (!$pm->f_venc)?'Imperecible':$pm->f_venc,
                    'stok' => $pm->stokf*1,
                    'satuan' => $pm->satuan,
                    'harga' => 'S/. '.$pm->harga,
                    'discount' => 'S/. '.$pm->discount,
                    'kode' => $pm->produk->kode,
                    'nama' => $pm->produk->nama,
                    'supplier' => (!$pm->id_supplier) ? '' : $pm->supplier->nama,
                    'pilih' => $pm->pilih
                ];
            })
            ->toArray();
    }

    public function creatediscount(Request $request, ProdukMasuk $pm)
    {
        try {
            $pm = $pm->find($request->id);
            $pm->discount = $request->discount;
            $result = $pm->save();
            if($result){
                return Response()
                    ->json([
                        'type'=> 'success',
                        'title' => 'Agregado con éxito',
                        'text' => $request->id." ".$request->discount
                    ], 201);
            }else{
                return Response()
                    ->json([
                        'type'=> 'error',
                        'title' => 'No se pudo agregar',
                        'text' => $request->tanggal." ".$request->stok." ".$request->kode
                    ], 406);
            }
        } catch (Exception $e) {
            return Response()
                ->json([
                    'type' => 'error',
                    'title' => 'No se pudo agregar',
                    'text' => $e->getMessage()
                ], 501);
        }
        
    }

    public function deletediscount(Request $request, ProdukMasuk $pm)
    {
        try{
            $pm = $pm->find($request->id);
            $pm->discount = 0;
            $result = $pm->save();

            if($result){
                return Response()
                    ->json([
                        'type'=> 'success',
                        'title'=> 'Eliminado exitosamente',
                        'text'=>  $request->id
                    ], 201);
            }else{
                return Response()
                    ->json([
                        'type'=> 'error',
                        'title'=> 'No se pudo eliminar',
                        'text'=>  $request->id
                    ], 406);
            }
        }catch (QueryException $e){
            return Response()
                ->json([
                    'type'=> 'error',
                    'title'=> 'No se pudo eliminar',
                    'text'=>  $e->getMessage()
                ], 501);
        }
    }
}
