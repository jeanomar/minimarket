<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    protected $fillable = ['user_id', 'created_at', 'updated_at', 'state', 'status_id', 'created_id', 'type', 'nota', 'produkmasuk_id'];
    protected $hidden = ['id'];
    protected $appends = ['action'];

    public function user()
    {
        return $this->belongsTo(Users::class, 'user_id');
    }

    public function creator()
    {
        return $this->belongsTo(Users::class, 'created_id');
    }

    public function status()
    {
        return $this->belongsTo(Users::class, 'status_id');
    }

    public function getActionAttribute()
    {
        return $this->attributes['action'] = '<div class="btn-group pull-right"><button type="button" title="Desactivar" class="btn btn-sm btn-primary" data-id="'.$this->id.'" data-nota="'.$this->nota.'id="btn-edit"><i class="fa fa-edit"></i></button></div>';
    }

}
