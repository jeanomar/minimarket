<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DateTime;
class Transaksi extends Model
{
    protected $table = 'transaksi';
    public $timestamps = false;
    protected $fillable = ['kode', 'tanggal', 'id_pelanggan', 'id_user', 'igv'];
    protected $primary = 'kode';
    protected $appends = ['action'];
    //protected $appends = ['nota', 'tanggaltransaksi'];

    public function user()
    {
        return $this->belongsTo(Users::class, 'id_user');
    }

    public function transaksidetails()
    {
        return $this->hasMany(TransaksiDetail::class, 'id_transaksi');
    }

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class, 'id_pelanggan');
    }

    public function produks()
    {
        return $this->belongsToMany(Produk::class, 'detail_transaksi', 'id_transaksi', 'id_produk');
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $dt = new DateTime;
            $model->tanggal = $dt->format('Y-m-d H:i:s');
            return true;
        });
//		static::saving(function($model){
//
//		});
    }

    public function getActionAttribute()
    {
        return $this->attributes['action'] = '<div class="btn-group pull-right">'.(($this->state_tran!='AC')?('<a type="button" title="Transacción anulada" class="btn btn-sm btn-danger" id="btn-info"><i class="fa fa-info"></i></a>'):('<button type="button" title="Anular" class="btn btn-sm btn-info" data-id="'.$this->id.'" data-nama="'.$this->kode.'" data-total="'.($this->bayar - $this->kembalian).'" id="btn-delete"><i class="fa fa-trash"></i></button>')).'<a href="/report/sales/salespdf?id='.$this->id.'" class="btn btn-sm buttons-pdf pull-right btn-warning" title="Impimir venta" data-id="'.$this->id.'" data-nama="'.$this->kode.'" id="btn-print"><i class="fa fa-file-pdf-o"></i></a></div>';
    }
}
