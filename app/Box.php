<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    protected $table = 'box';
    protected $fillable = ['user_id', 'created_at', 'updated_at', 'estate'];
    protected $hidden = ['id'];

}
