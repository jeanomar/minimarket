<?php
/**
 * Created by PhpStorm.
 * User: Omar Ludeña Chávez
 * Date: 15/08/2018
 * Time: 13:57
 */

namespace App\Transformers;


use App\Produk;
use League\Fractal\TransformerAbstract;

class ProdukTransformer extends TransformerAbstract
{
    public function transform(Produk $product)
    {
        return [
            'kode' => $product->kode,
            'nama' => $product->nama,
            'harga' => number_format($product->harga,2,'.','.'),
            'stok' => $product->stok,
            'category' => $product->kategori->nama,
            'unit' => $product->satuan->nama,
            'stok_min' => $product->stok_min,
            'venc_min' => $product->venc_min,
            'action' => $product->action,
        ];
    }
}