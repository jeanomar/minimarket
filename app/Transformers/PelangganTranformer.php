<?php
/**
 * Created by PhpStorm.
 * User: Omar Ludeña Chávez
 * Date: 15/08/2018
 * Time: 19:01
 */

namespace App\Transformers;


use App\Pelanggan;
use League\Fractal\TransformerAbstract;

class PelangganTranformer extends TransformerAbstract
{
    public function transform(Pelanggan $pelanggan)
    {
        return [
            'nama' => $pelanggan->nama,
            'kode' => $pelanggan->kode,
            'jenis_kelamin' => $pelanggan->jenis,
            'telepon' => $pelanggan->telepon,
            'alamat' => $pelanggan->alamat,
            'action' => $pelanggan->action,
        ];
    }
}