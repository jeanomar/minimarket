<?php
/**
 * Created by PhpStorm.
 * User: Omar Ludeña Chávez
 * Date: 15/08/2018
 * Time: 9:11
 */

namespace App\Transformers;


use App\Produk;
use App\Transaksi;
use App\TransaksiDetail;
use League\Fractal\TransformerAbstract;

class TransaksiTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'items'
    ];
    public function transform(Transaksi $transaksi)
    {
        return [
            'kode' => $transaksi->kode,
            'tanggal' => $transaksi->tanggal,
            'state_tran' => $transaksi->state_tran,
           // 'item' => $transaksi->tanggal,
            'sub_harga' => "S/. ".number_format(($transaksi->bayar - $transaksi->kembalian),2,'.','.'),
            'pelanggan' => ($transaksi->id_pelanggan) ? $transaksi->pelanggan->nama : '',
            'kasir' => $transaksi->user->nama,
            'action' => $transaksi->action
        ];
    }

    public function includeItems(Transaksi $transaksi)
    {
        $transaksidetails = $transaksi->transaksidetails;

        return $this->collection($transaksidetails, function($transaksidetails){
            return [
                'nama_item' => $transaksidetails->produk->nama,
                'harga_item' => "S/. ".$transaksidetails->produk->harga,
                'qty_item' => ($transaksidetails->qty*1)." ".$transaksidetails->satuan
            ];
        });

    }
}