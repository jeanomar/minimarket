<?php
/**
 * Created by PhpStorm.
 * User: Omar Ludeña Chávez
 * Date: 15/08/2018
 * Time: 9:11
 */

namespace App\Transformers;

use App\TransaksiDetail;
use League\Fractal\TransformerAbstract;

class TransaksiDetTransformer extends TransformerAbstract
{
    public function transform(TransaksiDetail $detail)
    {
        return [
            'kode_tran' => $detail->transaksi->kode,
            'tanggal' => $detail->transaksi->tanggal,
            "kategori" => $detail->produk->kategori->keterangan,
            'kode' => $detail->produk->kode,
            'nama' => $detail->produk->nama,
            'state_tran' => ($detail->transaksi->state_tran == "AC")?"Activo":"Anulado",
            'qty' => $detail->qty." ".$detail->satuan,
            'puv' => 'S/. '.number_format(($detail->pu),2,'.','.'),
            'totalv' => 'S/.'.number_format(($detail->pu*$detail->qty),2,'.','.'),
            'puc' => 'S/. '.number_format(($detail->harga_comp),2,'.','.'),
            'totalc' => 'S/.'.number_format(($detail->harga_comp*$detail->qty),2,'.','.')
        ];
    }

}