<?php
/**
 * Created by PhpStorm.
 * User: Omar Ludeña Chávez
 * Date: 15/08/2018
 * Time: 14:55
 */

namespace App\Transformers;


use App\ProdukSatuan;
use League\Fractal\TransformerAbstract;

class ProdukSatuanTransformer extends TransformerAbstract
{
    public function transform(ProdukSatuan $ps)
    {
        return [
            'nama' => $ps->nama,
            'keterangan' => $ps->keterangan,
            'action' => $ps->action,
        ];
    }
}