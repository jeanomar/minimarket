<?php
/**
 * Created by PhpStorm.
 * User: Omar Ludeña Chávez
 * Date: 15/08/2018
 * Time: 23:04
 */

namespace App\Transformers;


use App\Repository;
use League\Fractal\TransformerAbstract;

class RepositoryoutTransformer extends TransformerAbstract
{
    public function transform(Repository $keluar)
    {
        return [
            'tanggal' => $keluar->tanggal,
            'stok' => $keluar->stok*1,
            'satuan' => $keluar->satuan,
            'kode' => $keluar->produk->kode,
            'nama' => $keluar->produk->nama,
            'detail' => $keluar->produkdetail->nama,
            'user' => $keluar->user->nama
        ];
    }
}