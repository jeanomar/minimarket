<?php
/**
 * Created by PhpStorm.
 * User: Omar Ludeña Chávez
 * Date: 15/08/2018
 * Time: 21:54
 */

namespace App\Transformers;


use League\Fractal\TransformerAbstract;
use App\ProdukMasuk;

class ProdukMasukTransformer extends TransformerAbstract
{
    public function transform(ProdukMasuk $masuk)
    {
        return [
            'tanggal' => $masuk->tanggal,
            'f_venc' => (!$masuk->f_venc)?'Imperecible':$masuk->f_venc,
            'stok' => $masuk->stok*1,
            'satuan' => $masuk->satuan,
            'harga' => 'S/. '.$masuk->harga,
            'discount' => 'S/. '.$masuk->discount,
            'kode' => $masuk->produk->kode,
            'nama' => $masuk->produk->nama,
            'detail' => $masuk->produkdetail->nama,
            'supplier' => (!$masuk->id_supplier) ? '' : $masuk->supplier->nama
        ];
    }
}