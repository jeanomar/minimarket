<?php
/**
 * Created by PhpStorm.
 * User: Omar Ludeña Chávez
 * Date: 15/08/2018
 * Time: 23:36
 */

namespace App\Transformers;


use App\Users;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(Users $user)
    {
        return [
            'nama' => $user->nama,
            'username' => $user->username,
            'access' => $user->status->status,
            'activate' => ($user->deleted_at)?'Inactivo':'Activo',
            'action' => $user->action
        ];
    }
}