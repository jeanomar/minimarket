<?php
/**
 * Created by PhpStorm.
 * User: Omar Ludeña Chávez
 * Date: 15/08/2018
 * Time: 23:36
 */

namespace App\Transformers;


use App\Variable;
use League\Fractal\TransformerAbstract;

class VariableTransformer extends TransformerAbstract
{
    public function transform(Variable $var)
    {
        return [
            'nama' => $var->name,
            'val' => $var->value,
            'user' => $var->user->username,
            'activate' => ($var->state == 'IN')?'Inactivo':'Activo',
            'action' => $var->action
        ];
    }
}