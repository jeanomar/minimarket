<?php


Route::group(['middleware'=>'web'], function(){
    Route::get('login', [
        'uses'=>'AuthController@getLogin',
        'as'=>'login'
    ]);

    Route::post('login', [
        'uses'=>'AuthController@postLogin',
        'as'=>'post-login'
    ]);

    Route::post('c_activator', [
        'uses'=>'AuthController@c_activator',
        'as'=>'c_activator'
    ]);

    Route::get('logout', [
        'uses'=>'AuthController@logout',
        'as'=>'logout'
    ]);
});

Route::group(['middleware'=>'auth'], function(){
    //Auth::Route();
    Route::get('/', [
        'uses'=>'HomeController@index',
        'as'=>'dashboard'
    ]);

    Route::post('notif_state', [
        'uses'=>'HomeController@notif_state',
        'as'=>'notif_state'
    ]);

    Route::post('create_notif', [
        'uses'=>'HomeController@create_notif',
        'as'=>'create_notif'
    ]);
});

Route::group(['middleware'=>'auth', 'prefix'=>'supplier'], function(){
    Route::get('/',[
        'uses'=>'SupplierController@index',
        'as'=>'supplier'
    ]);
    Route::get('data-supplier', [
        'uses' => 'SupplierController@show',
        'as' => 'data-supplier'
    ]);
    Route::post('tambah',[
        'uses'=>'SupplierController@create',
        'as'=>'tambah-supplier'
    ]);
    Route::post('edit',[
        'uses'=>'SupplierController@update',
        'as'=>'edit-supplier'
    ]);
    Route::post('hapus',[
        'uses'=>'SupplierController@destroy',
        'as'=>'hapus-supplier'
    ]);
});

Route::group(['middleware'=>'auth', 'prefix'=>'customer'], function(){
    Route::get('/',[
        'uses'=>'CustomerController@index',
        'as'=>'customer'
    ]);
    Route::get('data-customer', [
        'uses' => 'CustomerController@show',
        'as' => 'data-customer'
    ]);
    Route::post('tambah',[
        'uses'=>'CustomerController@create',
        'as'=>'tambah-customer'
    ]);
    Route::post('edit',[
        'uses'=>'CustomerController@update',
        'as'=>'edit-customer'
    ]);
    Route::post('hapus',[
        'uses'=>'CustomerController@destroy',
        'as'=>'hapus-customer'
    ]);
    Route::post('select-customer',[
        'uses'=>'CustomerController@caricustomer',
        'as'=>'select-customer'
    ]);
});

Route::group(['middleware'=>'auth', 'prefix'=>'product'], function(){
    Route::post('select-satuan', [
        'uses' => 'ProductController@carisatuan',
        'as' => 'cari-satuan'
    ]);
    Route::get('select-satuan', [
        'uses' => 'ProductController@getsatuan',
        'as' => 'cari-satuann'
    ]);
    Route::post('select-kategori', [
        'uses' => 'ProductController@carikategori',
        'as' => 'cari-kategori'
    ]);
    Route::get('select-kategori', [
        'uses' => 'ProductController@getkategori',
        'as' => 'cari-kategorii'
    ]);
    Route::post('cari-item', [
        'uses' => 'ProductController@cari',
        'as' => 'cari'
    ]);
    Route::get('/', [
        'uses' => 'ProductController@index',
        'as' => 'product'
    ]);
    Route::get('data-product', [
        'uses' => 'ProductController@show',
        'as' => 'data-product'
    ]);
    Route::get('product-pilih', [
        'uses' => 'ProductController@productpilih',
        'as' => 'product-pilih'
    ]);
    Route::get('product-pilihrep', [
        'uses' => 'ProductController@productpilihrep',
        'as' => 'product-pilihrep'
    ]);
    Route::get('product-alm', [
        'uses' => 'ProductController@productalm',
        'as' => 'product-alm'
    ]);
    Route::post('tambah',[
        'uses'=>'ProductController@create',
        'as'=>'tambah-product'
    ]);
    Route::post('edit',[
        'uses'=>'ProductController@update',
        'as'=>'edit-product'
    ]);
    Route::post('hapus',[
        'uses'=>'ProductController@destroy',
        'as'=>'hapus-product'
    ]);

    Route::post('imgupload',[
        'uses'=>'ProductController@imgupload',
        'as'=>'imgupload-product'
    ]);

    Route::group(['prefix'=>'category'], function(){
        Route::get('/',[
            'uses'=>'CategoryController@index',
            'as'=>'category'
        ]);
        Route::get('data-category', [
            'uses' => 'CategoryController@show',
            'as' => 'data-category'
        ]);
        Route::post('tambah',[
            'uses'=>'CategoryController@create',
            'as'=>'tambah-category'
        ]);
        Route::post('edit',[
            'uses'=>'CategoryController@update',
            'as'=>'edit-category'
        ]);
        Route::post('hapus',[
            'uses'=>'CategoryController@destroy',
            'as'=>'hapus-category'
        ]);
    });

    Route::group(['prefix'=>'unit'], function(){
        Route::get('/',[
            'uses'=>'UnitController@index',
            'as'=>'unit'
        ]);
        Route::get('data-unit', [
            'uses'=>'UnitController@show',
            'as'=>'data-unit'
        ]);
        Route::post('tambah',[
            'uses'=>'UnitController@create',
            'as'=>'tambah-unit'
        ]);
        Route::post('edit',[
            'uses'=>'UnitController@update',
            'as'=>'edit-unit'
        ]);
        Route::post('hapus',[
            'uses'=>'UnitController@destroy',
            'as'=>'hapus-unit'
        ]);
    });
});

Route::group(['middleware'=>'auth', 'prefix'=>'stock'], function(){
    Route::group(['prefix'=>'in'], function(){
        Route::get('/',[
            'uses'=>'StockInController@index',
            'as'=>'stock-in'
        ]);
        Route::get('data',[
            'uses'=>'StockInController@show',
            'as'=>'data-stock-in'
        ]);
        Route::post('tambah',[
            'uses'=>'StockInController@create',
            'as'=>'tambah-stock-in'
        ]);
        Route::post('select-detailitem',[
            'uses'=>'StockInController@caridetailitem',
            'as'=>'select-detailitem'
        ]);
        Route::post('select-supplier',[
            'uses'=>'StockInController@carisupplier',
            'as'=>'select-supplier'
        ]);
    });

    Route::group(['prefix'=>'out'], function(){
        Route::get('/',[
            'uses'=>'StockOutController@index',
            'as'=>'stock-out'
        ]);
        Route::get('data',[
            'uses'=>'StockOutController@show',
            'as'=>'data-stock-out'
        ]);
        Route::post('tambah',[
            'uses'=>'StockOutController@create',
            'as'=>'tambah'
        ]);
        Route::post('select-detailitem',[
            'uses'=>'StockOutController@caridetailitem',
            'as'=>'select-detailitem-out'
        ]);
    });

    Route::get('',[
        'uses'=>'SupplierController@index'
    ]);
});

Route::group(['middleware'=>'auth', 'prefix'=>'repository'], function(){
    Route::group(['prefix'=>'in'], function(){
        Route::get('/',[
            'uses'=>'RepositoryController@input',
            'as'=>'input'
        ]);
        Route::get('data',[
            'uses'=>'RepositoryController@showin',
            'as'=>'data-stock-in'
        ]);
        Route::post('tambah',[
            'uses'=>'RepositoryController@createin',
            'as'=>'tambah-stock-in'
        ]);
        Route::post('select-detailitem',[
            'uses'=>'StockInController@caridetailitem',
            'as'=>'select-detailitem'
        ]);
        Route::post('select-supplier',[
            'uses'=>'StockInController@carisupplier',
            'as'=>'select-supplier'
        ]);
    });

    Route::group(['prefix'=>'out'], function(){
        Route::get('/',[
            'uses'=>'RepositoryController@output',
            'as'=>'output'
        ]);
        Route::get('data',[
            'uses'=>'RepositoryController@showout',
            'as'=>'data-stock-out'
        ]);
        Route::post('tambah',[
            'uses'=>'RepositoryController@createout',
            'as'=>'tambah'
        ]);
        Route::post('select-detailitem',[
            'uses'=>'StockOutController@caridetailitem',
            'as'=>'select-detailitem-out'
        ]);
    });

    Route::get('',[
        'uses'=>'SupplierController@index'
    ]);
});

Route::group(['middleware'=>'auth', 'prefix'=>'transaction'], function(){
    Route::get('/',[
        'uses'=>'TransactionController@index',
        'as'=>'transaction'
    ]);
    Route::post('openbox',[
        'uses'=>'TransactionController@openbox',
        'as'=>'openbox'
    ]);
    Route::post('closebox',[
        'uses'=>'TransactionController@closebox',
        'as'=>'closebox'
    ]);
     Route::post('printbox',[
        'uses'=>'TransactionController@printbox',
        'as'=>'printbox'
    ]);
    Route::get('nota-tgl', [
        'uses' => 'TransactionController@getKodeTransaksi',
        'as' => 'nota-tgl'
    ]);
    Route::post('cari-id', [
        'uses' => 'TransactionController@caribyid',
        'as' => 'cari-id'
    ]);
    Route::post('add-cart', [
        'uses' => 'TransactionController@addcart',
        'as' => 'add-cart'
    ]);
    Route::get('data-cart', [
        'uses' => 'TransactionController@datacart',
        'as' => 'add-cart'
    ]);
    Route::post('delete-cart', [
        'uses' => 'TransactionController@deletecart',
        'as' => 'delete-cart'
    ]);
    Route::post('remove-item', [
        'uses' => 'TransactionController@removeitem',
        'as' => 'remove-item'
    ]);
    Route::get('totalbayar', function(){
        return Response()->json(Cart::subtotal());
    });
    Route::post('bayar', [
        'uses' => 'TransactionController@bayar',
        'as' => 'bayar'
    ]);
    Route::post('printt', [
        'uses' => 'PrintController@print',
        'as' => 'printt'
    ]);
});

Route::group(['middleware'=>'auth', 'prefix'=>'report'], function(){
    Route::group(['prefix' => 'sales'], function(){
        Route::get('/',[
            'uses'=>'ReportController@sales',
            'as'=>'sales-report'
        ]);
        Route::post('data-report',[
            'uses'=>'ReportController@datareportsales',
            'as'=>'data-report'
        ]);

        Route::get('salespdf', [
            'uses'=> 'ReportController@salespdf',
            'as'=>'salespdf'
        ]);

        Route::get('salesexcel', [
            'uses'=> 'ReportController@salesexcel',
            'as'=>'salesexcel'
        ]);

        Route::get('downsalespdf', [
            'uses'=> 'ReportController@downsalespdf',
            'as'=>'downsalespdf'
        ]);

        Route::post('hapus',[
            'uses'=>'ReportController@hapus',
            'as'=>'hapus'
        ]);

    });

    Route::group(['prefix' => 'stockin'], function(){
        Route::get('/',[
            'uses'=>'ReportController@stockin',
            'as'=>'in-report'
        ]);
        Route::get('/data-report',[
            'uses'=>'ReportController@datareportstockin',
            'as'=>'data-report'
        ]);

    });
    Route::group(['prefix' => 'stockout'], function(){
        Route::get('/',[
            'uses'=>'ReportController@stockout',
            'as'=>'out-report'
        ]);
        Route::get('/data-report',[
            'uses'=>'ReportController@datareportstockout',
            'as'=>'data-report'
        ]);

    });
    Route::group(['prefix' => 'money-report'], function(){
        Route::get('/',[
            'uses'=>'ReportController@money',
            'as'=>'money-report'
        ]);
        Route::get('/data-report',[
            'uses'=>'ReportController@datareportmoney',
            'as'=>'data-report'
        ]);

        Route::get('gananciaspdf', [
            'uses'=> 'ReportController@gananciaspdf',
            'as'=>'gananciaspdf'
        ]);

        Route::get('gananciasexcel', [
            'uses'=> 'ReportController@gananciasexcel',
            'as'=>'gananciasexcel'
        ]);

        Route::get('downganaciaspdf', [
            'uses'=> 'ReportController@downganaciaspdf',
            'as'=>'downganaciaspdf'
        ]);


    });


});

Route::group(['middleware'=>'auth', 'prefix'=>'setting'], function(){
    Route::get('/',[
        'uses'=>'ConfigUserController@index',
        'as'=>'setting'
    ]);
    Route::get('data-user', [
        'uses' => 'ConfigUserController@show',
        'as' => 'data-user'
    ]);
    Route::post('tambah-user',[
        'uses'=>'ConfigUserController@create',
        'as'=>'tambah-user'
    ]);
    Route::post('edit-user',[
        'uses'=>'ConfigUserController@update',
        'as'=>'edit-user'
    ]);
    Route::post('edit-detuser',[
        'uses'=>'ConfigUserController@updatedetuser',
        'as'=>'edit-detuser'
    ]);
    Route::post('upload-photo',[
        'uses'=>'ConfigUserController@upload',
        'as'=>'upload-photo'
    ]);
    Route::get('variables', [
        'uses' => 'ConfigUserController@variables',
        'as' => 'variables'
    ]);
    Route::post('tambah-variable',[
        'uses'=>'ConfigUserController@createvariable',
        'as'=>'tambah-variable'
    ]);
    Route::post('edit-variable',[
        'uses'=>'ConfigUserController@updatevariable',
        'as'=>'edit-variable'
    ]);
});

Route::group(['middleware'=>'auth', 'prefix'=>'discount'], function(){
    Route::get('/',[
        'uses'=>'StockInController@discount',
        'as'=>'discount'
    ]);
    Route::get('stok-pilih', [
        'uses' => 'StockInController@stokpilih',
        'as' => 'stok-pilih'
    ]);
    Route::get('data',[
        'uses'=>'StockInController@discountdata',
        'as'=>'data-discount'
    ]);
    Route::post('tambah',[
        'uses'=>'StockInController@creatediscount',
        'as'=>'tambah-discount'
    ]);
    Route::post('delete',[
        'uses'=>'StockInController@deletediscount',
        'as'=>'delete-discount'
    ]);
    Route::post('select-detailitem',[
        'uses'=>'StockInController@caridetailitem',
        'as'=>'select-detailitem'
    ]);
    Route::post('select-supplier',[
        'uses'=>'StockInController@carisupplier',
        'as'=>'select-supplier'
    ]);
});