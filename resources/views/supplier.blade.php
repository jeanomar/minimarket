@extends('layouts.template')

@push('css')
{{ Html::style('plugins/datatables/dataTables.bootstrap.css') }}
@endpush

@push('js')
{{ Html::script('plugins/datatables/jquery.dataTables.min.js') }}
{{ Html::script('plugins/datatables/dataTables.bootstrap.min.js') }}
{{ Html::script('plugins/input-mask/jquery.inputmask.js') }}
{{ Html::script('js/pages/supplier.js') }}
@endpush

@section('title', 'Proveedores')

@section('content-header')
    <h1>
        Listado de Proveedores
        <small> </small>
    </h1>
@endsection

@section('content')
    <section class="content">
        {{ csrf_field() }}

        <div class="box box-widget ">
            <div class="box-header">
                
            </div>
            <div class="box-body">
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr>
                            <th>Nombres</th>
                            <th>Dirección</th>
                            <th>Teléfono</th>
                            <th>Descripsión</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection