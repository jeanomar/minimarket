<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    {{Html::style('css/bootstrap.min.css')}}
            <!-- Font Awesome -->
    
            <!-- Ionicons -->
    
     <link href="{{ asset('css/ionicons/ionicons.min.css') }}" rel="stylesheet" />
     <link href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" />
            <!-- Theme style -->
    {{Html::style('css/AdminLTE.min.css')}}
    {{Html::style('css/skins/skin-blue.min.css')}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{Html::style('plugins/pnotify/pnotify.css')}}
    {{Html::style('plugins/pnotify/pnotify.brighttheme.css')}}
    {{Html::style('plugins/pnotify/pnotify.buttons.css')}}
    {{Html::style('plugins/pnotify/pnotify.history.css')}}
    
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">


    @stack('css')
            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        /* Not PNotify specific, just make this page a little more presentable. */
        #switcher-container {
            position: fixed;
            top: 60px;
            right: 5px;
            z-index: 100;
        }
        #switcher-jqueryui, #switcher-jqueryui * {
            box-sizing: content-box;
        }
        @media (max-width: 980px) {
            #switcher-container {
                position: absolute;
                top: 55px;
            }
        }
        .ui-widget {
            font-size: 75% !important;
        }
        .btn-toolbar {
            line-height: 28px;
        }
        .btn-toolbar h4 {
            margin: 1em 0 .3em;
        }
        .btn-toolbar .btn-group {
            vertical-align: middle;
        }
        .panel .btn {
            margin-top: 5px;
        }

        /* Translucent notice CSS */
        .ui-pnotify.translucent.ui-pnotify-fade-in {
            opacity: .8;
        }

        /* Custom styled notice CSS */
        .ui-pnotify.custom .ui-pnotify-container {
            background-color: #404040 !important;
            background-image: none !important;
            border: none !important;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border-radius: 10px;
        }
        .ui-pnotify.custom .ui-pnotify-title, .ui-pnotify.custom .ui-pnotify-text {
            font-family: Arial, Helvetica, sans-serif !important;
            text-shadow: 2px 2px 3px black !important;
            font-size: 10pt !important;
            color: #FFF !important;
            padding-left: 50px !important;
            line-height: 1 !important;
            text-rendering: geometricPrecision !important;
        }
        .ui-pnotify.custom .ui-pnotify-title {
            font-weight: bold;
        }
        .ui-pnotify.custom .ui-pnotify-icon {
            float: left;
        }
        .ui-pnotify.custom .fa {
            margin: 3px;
            width: 33px;
            height: 33px;
            font-size: 33px;
            color: #FF0;
        }

        /* Alternate stack initial positioning. This one is done through code,
            to show how it is done. Look down at the stack_bottomright variable
            in the JavaScript below. */
        .ui-pnotify.stack-bottomright {
            /* These are just CSS default values to reset the PNotify CSS. */
            right: auto;
            top: auto;
            left: auto;
            bottom: auto;
        }
        .ui-pnotify.stack-custom {
            /* Custom values have to be in pixels, because the code parses them. */
            top: 200px;
            left: 200px;
            right: auto;
        }
        .ui-pnotify.stack-custom2 {
            top: auto;
            left: auto;
            bottom: 200px;
            right: 200px;
        }
        /* This one is totally different. It stacks at the top and looks
            like a Microsoft-esque browser notice bar. */
        .ui-pnotify.stack-bar-top {
            right: 0;
            top: 0;
        }
        .ui-pnotify.stack-bar-bottom {
            margin-left: 200;
            right: auto;
            bottom: 0;
            top: auto;
            left: auto;
        }

        /*******************************
        * ACCORDION WITH TOGGLE ICONS
        * Does not work properly if "in" is added after "collapse".
        *******************************/
          .panel-group .panel {
            border-radius: 0;
            box-shadow: none;
            border-color: #EEEEEE;
          }

          .panel-default > .panel-heading {
            padding: 0;
            border-radius: 0;
            
          }

          .panel-title {
            font-size: 14px;
          }

          .panel-title > a {
            display: block;
            padding: 15px;
            text-decoration: none;
          }

          .more-less {
            float: right;
            color: #212121;
          }

          .panel-default > .panel-heading + .panel-collapse > .panel-body {
            border-top-color: #EEEEEE;
          }

          /**********************/
    .btn.min { background:white; }
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
    /**********************/

.btn.min:after {
  content: attr(data-inactive-icon);
  font-family: 'FontAwesome';
}
.btn:after {
  content: attr(data-active-icon);
  font-family: 'FontAwesome';
}


        .popup-box {
           background-color: #ffffff;
            border: 1px solid #b0b0b0;
            bottom: 0;
            display: none;
            position: fixed;
            right: 15px;
            width: 600px;
            font-family: 'Open Sans', sans-serif;
        }
        .round.hollow {
            margin: 40px 0 0;
        }
        .round.hollow a {
            border: 2px solid #ff6701;
            border-radius: 35px;
            color: red;
            color: #ff6701;
            font-size: 23px;
            padding: 10px 21px;
            text-decoration: none;
            font-family: 'Open Sans', sans-serif;
        }
        .round.hollow a:hover {
            border: 2px solid #000;
            border-radius: 35px;
            color: red;
            color: #000;
            font-size: 23px;
            padding: 10px 21px;
            text-decoration: none;
        }
        .popup-box-on {
            display: block !important;
        }
        .popup-box .popup-head {
            background-color: #fff;
            clear: both;
            color: #7b7b7b;
            display: inline-table;
            font-size: 21px;
            padding: 7px 10px;
            width: 100%;
             font-family: Oswald;
        }
        .bg_none i {
            border: 1px solid #ff6701;
            border-radius: 25px;
            color: #ff6701;
            font-size: 17px;
            height: 33px;
            line-height: 30px;
            width: 33px;
        }
        .bg_none:hover i {
            border: 1px solid #000;
            border-radius: 25px;
            color: #000;
            font-size: 17px;
            height: 33px;
            line-height: 30px;
            width: 33px;
        }
        .bg_none {
            background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
            border: medium none;
        }
        .popup-box .popup-head .popup-head-right {
            margin: 11px 7px 0;
        }
        .popup-box .popup-messages {
        }
        .popup-head-left img {
            border: 1px solid #7b7b7b;
            border-radius: 50%;
            width: 44px;
        }
        .popup-messages-footer > textarea {
            border-bottom: 1px solid #b2b2b2 !important;
            height: 34px !important;
            margin: 7px;
            padding: 5px !important;
             border: medium none;
            width: 95% !important;
        }
        .popup-messages-footer {
            background: #fff none repeat scroll 0 0;
            bottom: 0;
            position: absolute;
            width: 100%;
        }
        .popup-messages-footer .btn-footer {
            overflow: hidden;
            padding: 2px 5px 10px 6px;
            width: 100%;
        }
        .simple_round {
            background: #d1d1d1 none repeat scroll 0 0;
            border-radius: 50%;
            color: #4b4b4b !important;
            height: 21px;
            padding: 0 0 0 1px;
            width: 21px;
        }
        .popup-box .popup-messages {
            background: #0d3930 none repeat scroll 0 0;
            height: 275px;
            overflow: auto;
        }
        .direct-chat-messages {
            overflow: auto;
            padding: 10px;
            transform: translate(0px, 0px);
            
        }
        .popup-messages .chat-box-single-line {
            border-bottom: 1px solid #a4c6b5;
            height: 12px;
            margin: 7px 0 20px;
            position: relative;
            text-align: center;
        }
        .popup-messages abbr.timestamp {
            background: #0d3930 none repeat scroll 0 0;
            color: #fff;
            padding: 0 11px;
        }

        .popup-head-right .btn-group {
            display: inline-flex;
            margin: 0 8px 0 0;
            vertical-align: top !important;
        }
        .chat-header-button {
            background: transparent none repeat scroll 0 0;
            border: 1px solid #636364;
            border-radius: 50%;
            font-size: 14px;
            height: 30px;
            width: 30px;
        }
        .popup-head-right .btn-group .dropdown-menu {
            border: medium none;
            min-width: 122px;
            padding: 0;
        }
        .popup-head-right .btn-group .dropdown-menu li a {
            font-size: 12px;
            padding: 3px 10px;
            color: #303030;
        }

        .popup-messages abbr.timestamp {
            background: #0d3930  none repeat scroll 0 0;
            color: #fff;
            padding: 0 11px;
        }
        .popup-messages .chat-box-single-line {
            border-bottom: 1px solid #a4c6b5;
            height: 12px;
            margin: 7px 0 20px;
            position: relative;
            text-align: center;
        }
        .popup-messages .direct-chat-messages {
            height: auto;
        }
        .popup-messages .direct-chat-text {
            background: #dfece7 none repeat scroll 0 0;
            border: 1px solid #dfece7;
            border-radius: 2px;
            color: #1f2121;
        }

        .popup-messages .direct-chat-timestamp {
            color: #fff;
            opacity: 0.6;
        }

        .popup-messages .direct-chat-name {
            font-size: 15px;
            font-weight: 600;
            margin: 0 0 0 49px !important;
            color: #fff;
            opacity: 0.9;
        }
        .popup-messages .direct-chat-info {
            display: block;
            font-size: 12px;
            margin-bottom: 0;
        }
        .popup-messages  .big-round {
            margin: -9px 0 0 !important;
        }
        .popup-messages  .direct-chat-img {
            border: 1px solid #fff;
            background: #0d3930  none repeat scroll 0 0;
            border-radius: 50%;
            float: left;
            height: 40px;
            margin: -21px 0 0;
            width: 40px;
        }
        .direct-chat-reply-name {
            color: #fff;
            font-size: 15px;
            margin: 0 0 0 10px;
            opacity: 0.9;
        }

        .direct-chat-img-reply-small
        {
            border: 1px solid #fff;
            border-radius: 50%;
            float: left;
            height: 20px;
            margin: 0 8px;
            width: 20px;
            background:#0d3930;
        }

        .popup-messages .direct-chat-msg {
            margin-bottom: 10px;
            position: relative;
        }

        .popup-messages .doted-border::after {
            background: transparent none repeat scroll 0 0 !important;
            border-right: 2px dotted #fff !important;
            bottom: 0;
            content: "";
            left: 17px;
            margin: 0;
            position: absolute;
            top: 0;
            width: 2px;
             display: inline;
              z-index: -2;
        }

        .popup-messages .direct-chat-msg::after {
            background: #fff none repeat scroll 0 0;
            border-right: medium none;
            bottom: 0;
            content: "";
            left: 17px;
            margin: 0;
            position: absolute;
            top: 0;
            width: 2px;
             display: inline;
              z-index: -2;
        }
        .direct-chat-text::after, .direct-chat-text::before {
           
            border-color: transparent #dfece7 transparent transparent;
            
        }
        .direct-chat-text::after, .direct-chat-text::before {
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            border-color: transparent #d2d6de transparent transparent;
            border-image: none;
            border-style: solid;
            border-width: medium;
            content: " ";
            height: 0;
            pointer-events: none;
            position: absolute;
            right: 100%;
            top: 15px;
            width: 0;
        }
        .direct-chat-text::after {
            border-width: 5px;
            margin-top: -5px;
        }
        .popup-messages .direct-chat-text {
            background: #dfece7 none repeat scroll 0 0;
            border: 1px solid #dfece7;
            border-radius: 2px;
            color: #1f2121;
        }
        .direct-chat-text {
            background: #d2d6de none repeat scroll 0 0;
            border: 1px solid #d2d6de;
            border-radius: 5px;
            color: #444;
            margin: 5px 0 0 50px;
            padding: 5px 10px;
            position: relative;
        }
body{
            font-family: 'Open Sans', sans-serif;
            }

        /* checkbox*/
        .boxes {
          margin: auto;
          padding: 50px;
          background: #484848;
        }

        /*Checkboxes styles*/
        input[type="checkbox"] { display: none; }

        input[type="checkbox"] + label {
          display: block;
          position: relative;
          padding-left: 35px;
          margin-bottom: 20px;
          font: 14px/20px 'Open Sans', Arial, sans-serif;
          color: #ddd;
          cursor: pointer;
          -webkit-user-select: none;
          -moz-user-select: none;
          -ms-user-select: none;
        }

        input[type="checkbox"] + label:last-child { margin-bottom: 0; }

        input[type="checkbox"] + label:before {
          content: '';
          display: block;
          width: 20px;
          height: 20px;
          border: 1px solid #6cc0e5;
          position: absolute;
          left: 0;
          top: 0;
          opacity: .6;
          -webkit-transition: all .12s, border-color .08s;
          transition: all .12s, border-color .08s;
        }

        input[type="checkbox"]:checked + label:before {
          width: 10px;
          top: -5px;
          left: 5px;
          border-radius: 0;
          opacity: 1;
          border-top-color: transparent;
          border-left-color: transparent;
          -webkit-transform: rotate(45deg);
          transform: rotate(45deg);
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">{{config('app.app')}}</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">{{config('app.name')}}</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <!-- Notifications Menu -->
                    <li class="dropdown notifications-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">{{(Auth::user()->alerts)?(Auth::user()->alerts->where('state','AC')->count()):0}}</span>
                        </a>
                        @if(Auth::user()->alerts)
                            @if(Auth::user()->alerts->where('state','AC')->count() > 0)
                            <ul class="dropdown-menu">
                                <li class="header">Tu tienes {{Auth::user()->alerts->where('state','AC')->count()}} notificaciones nuevas</li>
                                <li>
                                    <!-- Inner Menu: contains the notifications -->
                                    <ul class="menu">
                                        @if(Auth::user()->alerts->where('state','AC')->where('type','bg-aqua')->count() > 0)
                                        <li><!-- start notification -->
                                            <a><i class="fa fa-flag text-aqua"></i> {{Auth::user()->alerts->where('state','AC')->where('type','bg-aqua')->count()}} notificaciones de administrador</a>
                                        </li>
                                        @endif
                                        @if(Auth::user()->alerts->where('state','AC')->where('type','bg-green')->count() > 0)
                                        <li>
                                            <a><i class="fa fa-flag text-green"></i> {{Auth::user()->alerts->where('state','AC')->where('type','bg-green')->count()}} notificaciones informativas</a>
                                        </li>
                                        @endif
                                        @if(Auth::user()->alerts->where('state','AC')->where('type','bg-yellow')->count() > 0)
                                        <li>
                                            <a><i class="fa fa-flag text-yellow"></i> {{Auth::user()->alerts->where('state','AC')->where('type','bg-yellow')->count()}} notificaciones preventivas</a>
                                        </li>
                                        @endif
                                        @if(Auth::user()->alerts->where('state','AC')->where('type','bg-red')->count() > 0)
                                        <li>
                                            <a><i class="fa fa-flag text-red"></i> {{Auth::user()->alerts->where('state','AC')->where('type','bg-red')->count()}} notificaciones correctivas</a>
                                        </li>
                                        @endif
                                        <!-- end notification -->
                                    </ul>
                                </li>
                                <li class="footer"><a style="cursor: pointer;" id="openall">Ver Todo</a></li>
                            </ul>
                            @endif
                        @endif
                    </li>
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{(!Auth::user()->details)?((Auth::user()->id_status==1)?asset('img/avatar.png'):((Auth::user()->id_status==2)?asset('img/avatar5.png'):asset('img/avatar4.png'))):Auth::user()->details->photo}}" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->nama }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{(!Auth::user()->details)?((Auth::user()->id_status==1)?asset('img/avatar.png'):((Auth::user()->id_status==2)?asset('img/avatar5.png'):asset('img/avatar4.png'))):Auth::user()->details->photo}}" class="img-circle" alt="User Image">

                                <p>
                                    {{ Auth::user()->nama }}
                                    <small>{{Auth::user()->status->status}}</small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer" style="background: #092638;">
                                <div class="pull-left">
                                    <a href="{{url('/')}}" class="btn bg-aqua btn-flat">Inicio</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{url('/logout')}}" class="btn bg-yellow btn-flat">Cerrar Sesión</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{(!Auth::user()->details)?((Auth::user()->id_status==1)?asset('img/avatar.png'):((Auth::user()->id_status==2)?asset('img/avatar5.png'):asset('img/avatar4.png'))):Auth::user()->details->photo}}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->nama }}</p>
                    <!-- Status -->
                    <strong>{{Auth::user()->status->status}}</strong>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Menu</li>
                <!-- Optionally, you can add icons to the links -->
                @inject('menu', 'App\Helpers\Navigation\Contract\NavigationContract')
                @foreach($menu->getMenu() as $keyMenu => $valueMenu)
                    @if(empty($valueMenu['submenus']))
                        <li><a href="{{'HTTP://'.$_SERVER['HTTP_HOST'].'/'.$valueMenu['url']}}"><i class="fa {{$valueMenu['icon']}}"></i> <span>{{$valueMenu['title']}}</span></a></li>
                    @else
                        <li class="treeview category-tabs">
                            <a href="#">
                                <i class="fa {{$valueMenu['icon']}}"></i> <span>{{$valueMenu['title']}}</span>
								<span class="pull-right-container">
								  <icon class="fa fa-angle-left pull-right"></icon>
								</span>
                            </a>
                            <ul class="treeview-menu">
                                @foreach($valueMenu['submenus'] as $keySubmenu => $valSubmenu)
                                    <li><a href="{{route($valSubmenu['url'])}}" style="padding-left: 30px;"><i class="fa {{$valSubmenu['icon']}}"></i> {{$valSubmenu['title']}}</a></li>

                                @endforeach
                            </ul>
                        </li>
                    @endif
                @endforeach
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @yield('content-header')
        </section>

        <!-- Main content -->
        @yield('content')

        <!-- /.content -->
        <!--Notificaciones-->
        <div class="popup-box chat-popup" id="qnimate" style="z-index: 1000;">
            <div class="popup-head">
                <div class="popup-head-left pull-left"><i class="fa fa-bell-o"></i> Notificaciones</div>
                <div class="popup-head-right pull-right" style="margin-top: 0px;">                  
                    <button data-widget="remove" id="removeClass" class="chat-header-button pull-right" type="button" title="Cerrar">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="popup-messages">
            
                <div class="direct-chat-messages">   
                @if(Auth::user()->alerts)
                    @foreach(Auth::user()->alerts->sortByDesc('tanggal')->sortBy('state') as $alert)
                        <div class="chat-box-single-line">
                            <abbr class="timestamp">{{date_format($alert->created_at,'d-m-Y')}}</abbr>
                        </div>

                        <!-- Message. Default to the left -->
                        <div class="direct-chat-msg doted-border">
                          <div class="direct-chat-info clearfix">
                            <span class="direct-chat-name pull-left">

                                {{($alert->type == 'bg-aqua')?"Administrador":(($alert->type == 'bg-green')?"Información":(($alert->type == 'bg-yellow')?"Atención":"Alerta"))}}!
                            </span>
                            
                            <input type="checkbox" value="{{$alert->id}}" {{($alert->state == "IN")?'checked':''}} name="state" id="state{{$alert->id}}">
                            <label class="pull-right" for="state{{$alert->id}}">Visto</label>
                          </div>
                          <!-- /.direct-chat-info -->
                            <i class="fa-2x fa fa-bell {{($alert->type == 'bg-aqua')?"text-aqua":(($alert->type == 'bg-green')?"text-green":(($alert->type == 'bg-yellow')?"text-yellow":"text-red"))}} direct-chat-img" style="padding-left: 5px;padding-top: 4px;"></i>
                          <!-- /.direct-chat-img -->
                          <div class="direct-chat-text">
                            <span name="alert{{$alert->id}}" class="{{($alert->state == "IN")?'text-muted':''}}">

                                {{$alert->nota}}.
                                
                            </span>
                          </div>
                          <div class="direct-chat-info clearfix">
                            <span class="direct-chat-timestamp pull-right">{{date_format($alert->created_at,'h:y:s')}}</span>
                          </div>
                            <div class="direct-chat-info clearfix">
                            <span class="direct-chat-img-reply-small pull-left">
                                
                            </span>
                            <span class="direct-chat-reply-name">

                            </span>
                            </div>
                          <!-- /.direct-chat-text -->
                        </div>
                        <!-- /.direct-chat-msg -->
                    @endforeach
                @endif
                
                </div>
            
            </div>
            <div class="popup-messages-footer">
                <div class="btn-footer">
                
                </div>
            </div>
        </div>
        <!-- /.fin notificacionesr -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer" style="padding-bottom: 5px;padding-top: 5px;">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            Versión - {{config('app.version')}}
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; {{config('app.anio')}} - {{config('app.developer')}}.</strong> Todos los derechos reservados.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark" tabindex="-1">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane active" id="control-sidebar-home-tab">
                <div class="form-group">
                    <div class=" col-sm-8"><label class="control-sidebar-heading">Mis datos</label></div>
                    
                    <div class="col-sm-1">
                        <button name="btn_toggle" type="button" class="btn btn-flat bg-aqua" id="btn_u_det" ng-class="{min: min}" ng-click="toggle()" data-active-icon='&#xf044;' data-inactive-icon='&#xf0c7;'> </button>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-12">DNI / RUC / CE</label>
                    <div class="col-sm-12">
                        <input name="dni" class="form-control" type="text" maxlength="11" onkeyup="this.value = this.value.replace(/[^0-9]/g,'')" value="{{(Auth::user()->details)?Auth::user()->details->doc_identity:''}}" disabled="">
                    </div>    
                </div>
                <div class="col-sm-12"><br></div>
                <div class="form-group" style="padding-top: 15">
                    <label class="control-label col-sm-12">F. Nacimiento</label>
                    <div class="col-sm-12">
                        <input name="f_nac" class="form-control" type="date" value="{{(Auth::user()->details)?Auth::user()->details->f_nac:''}}" disabled="">
                    </div>    
                </div>
                <div class="col-sm-12"><br></div>
                <div class="form-group" style="padding-top: 15">
                    <label class="control-label col-sm-12">Sexo</label>
                    <div class="col-sm-12">
                        <select name="sex" class="form-control" disabled="">
                            <option value="masculino" {{(Auth::user()->details)?((Auth::user()->details->sex == 'masculino')?'selected':''):''}}>Masculino</option>
                            <option value="femenino" {{(Auth::user()->details)?((Auth::user()->details->sex == 'femenino')?'selected':''):''}}>Femenino</option>
                        </select>
                    </div>    
                </div>
                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="tokensi">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <img src="{{(!Auth::user()->details)?((Auth::user()->id_status==1)?asset('img/avatar.png'):((Auth::user()->id_status==2)?asset('img/avatar5.png'):asset('img/avatar4.png'))):Auth::user()->details->photo}}" width="175" height="190" id="previewimg">
                        </div>
                        
                    </div>

                    <div class="input-group">
                        <label class="input-group-btn">
                            <span class="btn btn-primary">
                                Examinar&hellip; <input type="file" style="display: none;"  id="inputimg" name="file" accept="image/png, image/jpeg, image/gif">
                            </span>
                        </label>
                        <input type="text" class="form-control" readonly>
                    </div>
                    <div class="col-sm-12"><br></div>
                    <div class="form-group" style="padding-top: 15px;">
                        <button type="button" class="col-md-12 btn btn-default" id="uploadbtn">Cambiar foto</button>
                    </div>
                </form>
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
{{Html::script('plugins/jQuery/jquery-2.2.3.min.js')}}
        <!-- Bootstrap 3.3.6 -->
{{Html::script('js/bootstrap.min.js')}}
        <!-- AdminLTE App -->
{{Html::script('js/app.min.js')}}
{{Html::script('plugins/pnotify/pnotify.js')}}
{{Html::script('plugins/pnotify/pnotify.animate.js')}}
{{Html::script('plugins/pnotify/pnotify.buttons.js')}}
{{Html::script('plugins/pnotify/pnotify.callbacks.js')}}
{{Html::script('plugins/pnotify/pnotify.confirm.js')}}
{{Html::script('plugins/pnotify/pnotify.desktop.js')}}
{{Html::script('plugins/pnotify/pnotify.history.js')}}
{{Html::script('plugins/pnotify/pnotify.nonblock.js')}}
{{Html::script('js/rfid.js')}}

<script src="{{ asset('plugins/vuejs/vue.js') }}"></script>

<script>
$("#openall").click(function () {
    $('#qnimate').addClass('popup-box-on');
});

$("#addClass").click(function () {
    $('#qnimate').addClass('popup-box-on');
});

$("#removeClass").click(function () {
    $('#qnimate').removeClass('popup-box-on');
});

$(function() {
    $("input[name='state']").click( function(e) {
        $.ajax({
            type     : 'POST',
            url      : "/notif_state",
            dataType : 'json',
            cache    : false,
            data: {
                '_token'         : $("input[name='_token']").val(),
                'id'             : e.target.value,
                'state'          : ($('input:checkbox[id="state'+e.target.value+'"]:checked').val() > 0)?"IN":"AC"
            },
            success: function(data){
                if (data.types == "IN") {
                    $("span[name='alert"+e.target.value+"']").attr({class: 'text-muted'});
                }else{
                    $("span[name='alert"+e.target.value+"']").attr({class: ''});
                }
            }
        });
    });
    
    var tab =0;
    $("button[name='btn_toggle']").click( function() {
        $("button").toggleClass('min');
        tab++;
        if (tab%2 == 0) {
           $("input[name=dni]").prop('disabled', true);
           $("input[name=f_nac]").prop('disabled', true);
           $("select[name=sex]").prop('disabled', true);
           $.ajax({
                type     : 'POST',
                url      : "/setting/edit-detuser",
                dataType : 'json',
                cache    : false,
                data: {
                    '_token'    : $("#tokensi").val(),
                    'dni'       : $('input[name="dni"]').val(),
                    'f_nac'     : $('input[name="f_nac"]').val(),
                    'sex'       : $('select[name="sex"]').val()
                },
                complete: function () {
                    //table.ajax.reload();
                }
            });

        }else{
           $("input[name=dni]").prop('disabled', false);
           $("input[name=f_nac]").prop('disabled', false);
           $("select[name=sex]").prop('disabled', false);
        }
    });
    var file;
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#previewimg').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


    $("#inputimg").change(function (e) {
        readURL(this);
        var files = e.target.files || e.dataTransfer.files;

        if (!files.length)
            return;
        //this.createImage(files[0]);
        file=files[0];
    });

    $("#uploadbtn").on("click", function(e){
        var fil = file;
        var formData = new FormData();
        formData.append('file', fil);
        formData.append('nameimg', file.name);
        var token = $("#tokensi").val();
        $.ajax({
            url: "setting/upload-photo",
            headers: {'X-CSRF-TOKEN': token},
            type: 'POST',
            datatype: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success:function(response){
                $(".img-circle").attr("src", response.photo);
                $(".user-image").attr("src", response.photo);
            },
            error:function(msj){
                
            }
        });
    });

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {

    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);


  });

  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }
          
      });


  });
  
});



    $('.category-tabs a').click(function(){
        $('.category-tabs a').find('icon').removeClass('fa-angle-down fa-angle-left').addClass('fa-angle-left');
        $(this).find('icon').toggleClass('fa-angle-left fa-angle-down');
    });

    var csrf =  $('meta[name="csrf-token"]').attr('content');
    var modal = '' +
            '<div id="modal" class="modal modal" role="dialog" tabindex="-1" aria-labelledby="" aria-hidden="true">'+
            '<div class="modal-dialog">'+
            '<div class="modal-content">'+
            '<div class="modal-header">'+
            '<button type="button" class="close" data-dismiss="modal">&times;</button>'+
            '<h4 class="modal-title"></h4>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>';

    var stack_bottomright = {"dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25};
    var stack_bar_top = {"dir1": "down", "dir2": "right", "push": "top", "spacing1": 0, "spacing2": 0};
    var stack_bar_bottom = {"dir1": "up", "dir2": "right", "spacing1": 0, "spacing2": 0};
    $(function(){
        PNotify.prototype.options.styling = "bootstrap3";

        var url = window.location;
        $('ul.sidebar-menu a').filter(function() {
            return this.href == url; //true
        }).parent('li').addClass('active');
        $('li.treeview a').filter(function() {
            return this.href == url; //true
        }).parents('li').find('icon').toggleClass('fa-angle-left fa-angle-down');
        $('ul.treeview-menu a').filter(function(){
            return this.href == url;
        }).parents('li.treeview').addClass('active');

    });

    function show_notif(notif) {
        var opts = {
            type: notif.type,
            title: notif.title,
            text: notif.text,
            addclass: "stack-bar-bottom",
            cornerclass: "",
            width: "100%",
            stack: stack_bar_bottom,
            delay: 3000
        };
        new PNotify(opts);
    }


    $(document).ajaxStart(onStart)
            .ajaxStop(onStop)
            .ajaxSend(onSend)
            .ajaxComplete(onComplete)
            .ajaxSuccess(onSuccess)
            .ajaxError(onError);

    function onStart(event) {
        console.log("START");
    }
    function onStop(event) {
        console.log("STOP");
    }
    function onSend(event, xhr, settings) {
        $(".help-block" ).remove();
        $(".form-group").removeClass('has-error');
        if(typeof settings.context !== 'undefined'){
          switch (settings.context.context) {
            case "form" :
              $('.loading').show();
            break;
            case "rfid" :
              $('body').append(modal);
              if($('form#rfid').length == 0) {
                $('.modal-content').append(rfidform);
              }
              $('.modal-title').text(settings.context.title);
              $('#modal').modal({keyboard: false, backdrop: 'static'});
            break;
          }
        }
    }
    function onComplete(event, xhr, settings) {
        switch (xhr.status){
            //create
            case 201:{
                $('#modal').modal('hide');
                break;
            }
            //update & delete
            case 202:{
                $('#modal').modal('hide');
                break;
            }
        }
    }
    function onSuccess(event, xhr, settings) {
        switch (xhr.status){
            case 200:
                break;
            case 201:{
                //show_notif(xhr.responseJSON);
            }
            case 202:{
                show_notif(xhr.responseJSON);
            }
        }
    }
    function onError(event, xhr, settings, err) {
        switch (xhr.status){
            //message error delete
            case 501:{
                show_notif(xhr.responseJSON);
                break;
            }
            //mesage form validation
            case 422:{
                if(typeof xhr.responseJSON !== "undefined"){
                    $.each(xhr.responseJSON, function(key, val){
                        var opts = {
                            type: "error",
                            title: "",
                            text: val
                        };
                        show_notif(opts);
                        if(key){
                            $("input[name='"+key+"']").parents('.form-group').addClass('has-error');
                            $("select[name='"+key+"']").parents('.form-group').addClass('has-error');
                            var errorJenis = '<span class="help-block"><strong>'+val+'</strong></span>';
                            $(errorJenis).insertAfter($("input[name='"+key+"']"));
                            $(errorJenis).insertAfter($("select[name='"+key+"']"));
                        }
                    })
                }
                break;
            }
        }
    }
</script>

@stack('js')

</body>
</html>
