<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Informe de venta</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- jQuery 2.2.3 -->

            <!-- Bootstrap 3.3.6 -->

    <style>
        
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="col-md-12">
        <section class="content">
            {{csrf_field()}}
            <div class="box box-widget ">
                <div class="box-header with-border">
                    <h3 class="box-title text-center">Informe de venta</h3>
                    <span style="color: red;">{{($sales->state_tran = 'IN')?"ELIMINADO":""}}</span>
                </div>
                <div class="box-body">
                    <div><label>Transacción: </label> {{$sales->kode}}</div>
                    <div><label>Comprador: </label> {{($sales->pelanggan)?$sales->pelanggan->nama:"No registrado"}}</div>
                    <div><label>Vendedor: </label> {{$sales->user->nama}}</div>
                    <div><label>Fecha - hora: </label> {{$sales->tanggal}}</div>
                    <table class="table table-bordered" id="table">
                        <thead>
                            <tr>
                                <th class="text-center">Producto</th>
                                <th class="text-center">Cantidad</th>
                                <th class="text-center">P. U. S/.</th>
                                <th class="text-center">Total S/.</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $sum = 0;
                            ?>
                            @foreach($sales->transaksidetails as $detail)
                            <?php
                                $sum += ($detail->produk->harga * $detail->qty);
                            ?>
                            <tr>
                                <th>{{$detail->produk->nama}}</th>
                                <th class="text-right">{{$detail->qty*1}}</th>
                                <th class="text-right">{{sprintf("%01.2f" , round($detail->produk->harga,2))}}</th>
                                <th class="text-right">{{sprintf("%01.2f" , round(($detail->produk->harga * $detail->qty),2))}}</th>
                            </tr>
                            @endforeach
                            <tr>
                                <th colspan="2" rowspan="3"></th>
                                <th class="text-right">
                                   Suma S/.
                                </th>
                                <th class="text-right">
                                    {{sprintf("%01.2f" , round($sum,2))}}
                                </th>
                            </tr>
                            <tr>
                                <th class="text-right">
                                    Redondeo +-
                                </th>
                                <th class="text-right">
                                    {{sprintf("%01.2f" , round(($sum - ($sales->bayar - $sales->kembalian)),2))}}
                                </th>
                            </tr>
                            <tr>

                                <th class="text-right">
                                    VENTA TOTAL S/.
                                </th>
                                <th class="text-right">
                                    {{sprintf("%01.2f" , round(($sales->bayar - $sales->kembalian),2))}}
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="tambah-modal-query"></div>
        </section>
    </div>

</body>
</html>
