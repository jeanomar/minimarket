<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Minimarket</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        {{Html::style('css/bootstrap.min.css')}}
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Minimarket
                </div>

                <div class="links">
                     <div class=" login-box-body">
                     <div class="row">
                     <div class="col-xs-12"><h2>Please activate your application before use</h2>
                     </div>
                     {{csrf_field()}}
                     <div class="tab-pane fade active in" role="tabpanel" id="home" aria-labelledby="home-tab">
                     <h3>* Please be sure you re connected to the Internet</h3>
                     <div>
                     <div class="form-group">
                     <label for="exampleInputEmail1">Username</label>
                     <input type="text" class="form-control" autofocus id="txtuser" placeholder="Username">
                     </div>
                     <div class="form-group">
                     <label for="exampleInputPassword1">Password</label>
                     <input type="password" class="form-control" id="txtpass" placeholder="Password">
                     </div>
                     <div class="form-group">
                     <label for="exampleInputFile">Web Activator</label>
                     <input type="text" value="" placeholder="URL WEB SERVER" class="form-control" id="txtweb">
                     </div>
                     <button type="submit" id="btnonline" class="btn btn-default">Submit</button>
                     </div>
                     </div>
                     
                     <div class="col-xs-12" id="result"></div>
                     
                     </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <!-- jQuery 2.2.3 -->
    {{Html::script('plugins/jQuery/jquery-2.2.3.min.js')}}
    <!-- Bootstrap 3.3.6 -->
    {{Html::script('js/bootstrap.min.js')}}

     <script type="text/javascript">
        $(document).on("click","#btnonline",function(){
            var nama = $("#txtuser").val();
            var pass = $("#txtpass").val();
            var url = $("#txtweb").val();
            var token = $("input[name='_token']").val();
            var value = {
                 nama:nama,
                 password:pass,
                 url:url,
                 method : "online",
                 '_token' : token
            };

            $.ajax({
                url : "c_activator",
                type: "POST",
                data : value,
                success: function(data, textStatus, jqXHR){
                    var data = jQuery.parseJSON(data);
                    if(data.value == true){
                        $("#result").html("<h2>Application active!!, Thank you</h2><p>You can login from <a href='/'>this link</a>");
                        $("#xtabbs").html("");
                    }else{
                        $("#result").html("Sorry, failed to activate your application");
                    }
         
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $("#result").html("error");
                }
            });
        })
     </script>
</html>
