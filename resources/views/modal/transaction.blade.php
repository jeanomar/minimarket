<div id="modalCariItem" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Buscar producto</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered" id="info-item">
                    <thead>
                    <tr>
                        <td>Código</td>
                        <td>Producto</td>
                        <td>Precio S/.</td>
                        <td>Stock</td>
                        <td>Acciones</td>
                    </tr>
                    </thead>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-danger" id="modal-stok-kosong" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Lo sentimos ...!</h4>
            </div>
            <div class="modal-body">
                <p>Stock Vacío</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal modal-danger" id="modal-kode-kosong" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Lo sentimos ...!</h4>
            </div>
            <div class="modal-body">
                <p>El Código no existe en la base de datos</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal modal-danger" id="modal-kode-closebox" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cerrar Caja</h4>
            </div>
            <div class="modal-body">
                <p>Está seguro de cerrar caja, no podra realizar más compras por el dia de hoy?</p>
            </div>
            <div class="modal-footer">
                {!! Form::open(['method'=>'POST', 'route'=>'closebox']) !!}
                <button class="btn btn-primary pull-left">Aceptar</button>
                {!!Form::close() !!}
                 <button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modalPrint" class="modal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Imprimir venta</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Comprador:</label>
                    <div class="col-sm-9">
                        <label class="form-control" id="comprador"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Transacción:</label>
                    <div class="col-sm-9">
                        <label class="form-control" id="transac"></label>
                        <input type="hidden" id="id_transaction">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Fecha:</label>
                    <div class="col-sm-9">
                        <label class="form-control" id="fechav"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Pagó con S/.:</label>
                    <div class="col-sm-9">
                        <label class="form-control" id="vpagoc"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Vuelto S/.:</label>
                    <div class="col-sm-9">
                        <label class="form-control" id="vcueltoc"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Total S/.:</label>
                    <div class="col-sm-9">
                        <label class="form-control" id="vtotal"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Detalle de venta</label>
                </div>
                <table  id="info-venta" class="table table-bordered">
                    <thead>
                    <tr>
                        <td>Código</td>
                        <td>Producto</td>
                        <td>Precio S/.</td>
                        <td>Cantidad</td>
                    </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="modal-footer">
                <button onclick="print($('#id_transaction').val())" class="btn btn-primary pull-left" data-dismiss="modal">Imprimir</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div id="modalnewclient" class="modal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Nuevo cliente</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="formclient">
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>Nombres</label>
                        <div class='col-sm-9'>
                            <input class='form-control' placeholder='Nombres' type='text' name='namac'>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>DNI/RUC/CE</label>
                        <div class='col-sm-9'>
                            <input class='form-control' placeholder='DNI/RUC/CE' type='text' name='kodec'>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>Sexo</label>
                        <div class='col-sm-9'>
                            <select name='jenis_kelamin' class='form-control'>
                                <option value='' disabled selected>Seleccione una opción</option>
                                <option value='M'>Masculino</option>
                                <option value='F'>Femenino</option>
                            </select>
                        </div>
                    </div>
                    <div class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#collapse1">Informacón adicional<i class="fa fa-angle-down pull-right"></i></a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse" style="padding-top: 15px;">
                                <div class='form-group'>
                                    <label class='col-sm-3 control-label'>Teléfono</label>
                                    <div class='col-sm-9'>
                                        <input class='form-control' placeholder='Teléfono' name='telepon'>
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='col-sm-3 control-label'>Dirección</label>
                                    <div class='col-sm-9'>
                                        <textarea class='form-control' name='alamat'>
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
            <div class="modal-footer">
                <button type='button' class='btn btn-flat bg-aqua' id='btnSimpan'>Aceptar</button>
                <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>
            </div>
        </div>
    </div>
</div>
