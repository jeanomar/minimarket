<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Informe de ganancias</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <style>
            table td, table th{

        border:1px solid black;

    }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="col-md-12">
        <section class="content">
            {{csrf_field()}}
            <div class="box box-widget ">
                <div class="box-header with-border">
                    <h3 class="box-title text-center">Informe de ganancias</h3>
                    
                </div>
                <div class="box-body" style="font-size: 10px">
                    <div class="form-control">
                        <label for="form-control">Desde:</label> {{$sales[0]->tanggal}}
                        <label for="form-control">Hasta:</label> {{$sales[$sales->count() - 1]->tanggal}}
                    </div>
                    <br>
                    <div><h5>Transacciones activas</h5></div>
                    <table class="table table-bordered" id="table">
                        <thead>
                            <tr>
                                <th class="text-center">Transacción</th>
                                <th class="text-center">Categoría</th>
                                <th class="text-center">Producto</th>
                                <th class="text-center">Cantidad</th>
                                <th class="text-center">P. U. Venta S/.</th>
                                <th class="text-center">Subtotal Venta S/.</th>
                                <th class="text-center">P. U. Compra S/.</th>
                                <th class="text-center">Subtotal Compra S/.</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $atotalcompra=0;
                            $atotalventa=0;
                            $acant=0;
                            $totaTran=0;
                        ?>
                        @foreach($sales as $tran)
                            @if( $tran->state_tran == "AC")
                                <?php
                                    $acant += 1;
                                    $totaTran += ($tran->bayar - $tran->kembalian);
                                ?>
                                @foreach($tran->transaksidetails as $detail)
                                    <?php
                                        $atotalventa += ($detail->pu * $detail->qty);
                                        $atotalcompra += ($detail->harga_comp * $detail->qty);
                                    ?>
                                    <tr>
                                        <th>{{$tran->kode}}</th>
                                        <th>{{$detail->produk->kategori->keterangan}}</th>
                                        <th>{{$detail->produk->nama}}</th>
                                        <th class="text-right">{{$detail->qty}}</th>
                                        <th class="text-right">{{$detail->pu}}</th>
                                        <th class="text-right">{{$detail->pu * $detail->qty}}</th>
                                        <th class="text-right">{{$detail->harga_comp}}</th>
                                        <th class="text-right">{{$detail->harga_comp * $detail->qty}}</th>
                                    </tr>
                                @endforeach
                            @endif
                        @endforeach
                            <tr>
                                <th colspan="4">Cant. Transacciones activas: {{$acant}}</th>
                                <th class="text-right">SUMA VENTAS S/.</th>
                                <th class="text-right">{{ sprintf("%01.2f" , round($atotalventa,2)) }}</th>
                                <th class="text-right">SUMA COMPRAS S/.</th>
                                <th class="text-right">{{ sprintf("%01.2f" , round($atotalcompra,2)) }}</th>
                            </tr>
                        </tbody>
                    </table>
                    
                    <div><h5>Transacciones eliminadas</h5></div>
                    <table class="table table-bordered" id="table">
                        <thead>
                            <tr>
                                <th class="text-center">Transacción</th>
                                <th class="text-center">Categoría</th>
                                <th class="text-center">Producto</th>
                                <th class="text-center">Cantidad</th>
                                <th class="text-center">P. U. Venta S/.</th>
                                <th class="text-center">Subtotal Venta S/.</th>
                                <th class="text-center">P. U. Compra S/.</th>
                                <th class="text-center">Subtotal Compra S/.</th>
                            </tr>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $itotalcompra=0;
                            $itotalventa=0;
                            $icant=0;
                        ?>
                        @foreach($sales as $tran)
                            @if( $tran->state_tran == "IN")
                                <?php
                                    $icant+=1;
                                ?>
                                @foreach($tran->transaksidetails as $detail)
                                    <?php
                                        $itotalventa += ($detail->pu * $detail->qty);
                                        $itotalcompra += ($detail->harga_comp * $detail->qty);
                                    ?>
                                    <tr>
                                        <th>{{$tran->kode}}</th>
                                        <th>{{$detail->produk->kategori->keterangan}}</th>
                                        <th>{{$detail->produk->nama}}</th>
                                        <th class="text-right">{{$detail->qty}}</th>
                                        <th class="text-right">{{$detail->pu}}</th>
                                        <th class="text-right">{{$detail->pu * $detail->qty}}</th>
                                        <th class="text-right">{{$detail->harga_comp}}</th>
                                        <th class="text-right">{{$detail->harga_comp * $detail->qty}}</th>
                                    </tr>
                                 @endforeach
                            @endif
                        @endforeach
                            <tr>
                                <th colspan="4">Cant. Transacciones eliminadas: {{$icant}}</th>
                                <th class="text-right">SUMA VENTAS S/.</th>
                                <th class="text-right">{{ sprintf("%01.2f" , round($itotalventa,2)) }}</th>
                                <th class="text-right">SUMA COMPRAS S/.</th>
                                <th class="text-right">{{ sprintf("%01.2f" , round($itotalcompra,2)) }}</th>
                            </tr>
                        </tbody>
                    </table>

                    <div><h5>Resumen</h5></div>
                    <table class="table table-bordered" id="table">
                        <thead>
                            <tr>
                                <th class="text-center">Subtotal Venta S/.</th>
                                <th class="text-center">Redondeos S/.</th>
                                <th class="text-center">Recaudado </th>
                                <th class="text-center">total Compra S/.</th>
                                <th class="text-center">GANANCIA S/.</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th class="text-right">{{ sprintf("%01.2f" , round(($atotalventa),2)) }}</th>
                                <th class="text-right">{{ sprintf("%01.2f" , round(($totaTran - $atotalventa),2)) }}</th>
                                <th class="text-right">{{ sprintf("%01.2f" , round(($totaTran),2)) }}</th>
                                <th class="text-right">{{ sprintf("%01.2f" , round(($atotalcompra),2)) }}</th>
                                <th class="text-right">{{ sprintf("%01.2f" , round(($totaTran - $atotalcompra),2)) }}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="tambah-modal-query"></div>
        </section>
    </div>

</body>
</html>
