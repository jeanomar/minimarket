@extends('layouts.template')

@push('css')
{{ Html::style('plugins/daterangepicker/daterangepicker.css') }}
{{ Html::style('plugins/datepicker/datepicker3.css') }}
{{ Html::style('plugins/timepicker/bootstrap-timepicker.min.css') }}
{{ Html::style('plugins/buttons/buttons.dataTables.min.css') }}
@endpush

@push('js')
{{ Html::script('plugins/moment/moment.min.js') }}
{{ Html::script('plugins/daterangepicker/daterangepicker.js') }}
{{ Html::script('plugins/datepicker/bootstrap-datepicker.js') }}
{{ Html::script('plugins/timepicker/bootstrap-timepicker.min.js') }}
{{ Html::script('plugins/datatables/jquery.dataTables.min.js') }}
{{ Html::script('plugins/datatables/dataTables.bootstrap.min.js') }}
{{ Html::script('plugins/buttons/dataTables.buttons.min.js') }}

{{ Html::script('plugins/buttons/buttons.bootstrap.min.js ') }}
{{ Html::script('plugins/buttons/jszip.min.js') }}
{{ Html::script('plugins/buttons/pdfmake.min.js') }}
{{ Html::script('plugins/buttons/vfs_fonts.js') }}
{{ Html::script('plugins/buttons/buttons.html5.min.js') }}
{{ Html::script('plugins/buttons/buttons.colVis.min.js ') }}

{{ Html::script('plugins/buttons/buttons.print.min.js') }}
{{ Html::script('js/pages/reportmoney.js') }}
@endpush

@section('title', 'Informe de artículos salientes')

@section('content-header')

@endsection

@section('content')
    <section class="content">
        {{ csrf_field() }}
        <div class="box box-default box-solid collapsed-box" style="border-radius: 0; margin-bottom: 0">
            <div class="box-header with-border">
                <h3 class="box-title">Informe de ganancias</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: none;">

                <div class="col-sm-3">
                    <div class="input-group">
                        <div class="input-group-addon" title="Código">
                            <i class="fa  fa-clone"></i>
                        </div>
                        <input class="form-control pull-right" type="text" name="kode">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="input-group">
                        <div class="input-group-addon" title="Producto">
                            <i class="fa fa-briefcase"></i>
                        </div>
                        <input class="form-control pull-right" type="text" name="produk">
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="input-group">
                        <div class="input-group-addon" title="Categoría">
                            <i class="fa fa-list-alt"></i>
                        </div>
                        <input class="form-control pull-right" type="text" name="kategori">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="input-group">
                        <div class="input-group-addon" title="Entre fechas">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input class="form-control pull-right" type="text" name="tanggal">
                    </div>
                </div>

                <div class="col-sm-1">
                    <div class="form-group">
                        <button class="btn btn-block btn-info" id="btn-search"><i class="fa fa-search"></i> </button>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>

        <div class="box box-widget" style="border-radius: 0">
            <div class="box-body no-padding" style="display: block;">
                <table class="table table-striped table-bordered" id="table">
                    <thead>
                    <tr>
                        <td><i class="fa fa-clone"></i> Transacción</td>
                        <td><i class="fa fa-calendar"></i> Fecha</td>
                        <td><i class="fa fa-list-alt"></i>Categoría</td>
                        <td>Código</td>
                        <td><i class="fa fa-briefcase"></i> Producto</td>
                        <td><i class="fa fa-asterisk"></i> Detalle</td>
                        <td><i class="fa fa-linode"></i> Cantidad</td>
                        <td><i class="fa fa-money"></i> P. U. Venta</td>
                        <td><i class="fa fa-money"></i> Total Venta</td>
                        <td><i class="fa fa-money"></i> P. U. Compra</td>
                        <td><i class="fa fa-money"></i> Total Compra</td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection