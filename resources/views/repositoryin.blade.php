@extends('layouts.template')

@push('css')
{{ Html::style('plugins/datatables/dataTables.bootstrap.css') }}
{{ Html::style('css/bootstrap-select.min.css') }}
{{ Html::style('plugins/select-ajax/css/ajax-bootstrap-select.css') }}
@endpush

@push('js')
{{ Html::script('plugins/datatables/jquery.dataTables.min.js') }}
{{ Html::script('plugins/datatables/dataTables.bootstrap.min.js') }}
<script src="{{ asset('plugins\jquery-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('plugins\bootstrap-select/bootstrap-select.min.js') }}"></script>
{{ Html::script('plugins/select-ajax/js/ajax-bootstrap-select.min.js') }}
{{ Html::script('js/pages/repositoryin.js') }}
@endpush

@section('title', 'Entrada para el almacén')

@section('content-header')
    <h1>
        Entrada para el almacén
        <small> </small>
    </h1>
@endsection

@section('content')
    <section class="content">
        {{csrf_field()}}
        <div class="box box-widget ">
            <div class="box-header">
                <button title="Nuevo registro" class="btn btn-flat bg-aqua" id="btn-tambah">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
            <div class="box-body">
                <table class="table table-bordered" id="table">
                    <thead>
                    <tr>
                        <th>Fecha Registro</th>
                        <th>Código</th>
                        <th>Producto</th>
                        <th>Detalle</th>
                        <th>P. U. compra</th>
                        <th>Cantidad</th>
                        <th>Unidad</th>
                        <th>Proveedor</th>
                        <th>Fecha Vencimiento</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div id="tambah-modal-query"></div>
        <div id="info-modal-query">
            @include('modal.stockin')
        </div>
    </section>
@endsection