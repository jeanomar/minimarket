@extends('layouts.template')

@push('css')
{{ Html::style('plugins/datatables/dataTables.bootstrap.css') }}
@endpush

@push('js')
{{ Html::script('plugins/datatables/jquery.dataTables.min.js') }}
{{ Html::script('plugins/datatables/dataTables.bootstrap.min.js') }}
{{ Html::script('js/pages/unit.js') }}
@endpush

@section('title', 'Unidades de medida')

@section('content-header')
    <h1>
        Listado de Unidades de Medida
        <small> </small>
    </h1>
@endsection

@section('content')
    <section class="content">
        {{csrf_field()}}
        <div class="box box-widget ">
            <div class="box-header">
                @if(Auth::user()->id_status!=3)
                <button class="btn btn-flat bg-aqua" id="btn-tambah" title="Nuevo registro"><i class="fa fa-plus"></i></button>
                @endif
            </div>
            <div class="box-body">
                <table class="table table-bordered" id="table">
                    <thead>
                    <tr>
                        <th>Abreviatura</th>
                        <th>Nombre</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection