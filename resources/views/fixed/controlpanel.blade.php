<div class="row">
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>
                    <sup style="font-size: 20px">S/.</sup>
                    @if(Auth::user()->id_status == 3)
                    {{ sprintf("%01.2f" , round((Auth::user()->transaksis->where("state_tran",'AC')->sum("bayar"))-(Auth::user()->transaksis->where("state_tran",'AC')->sum("kembalian")),2) )}}
                    @else
                    {{sprintf("%01.2f" , round(((\DB::select('SELECT (sum(u.stok * u.harga) - (SELECT sum(k.stok * k.harga) FROM produk_keluar k )) as sum FROM produk_masuk u'))[0]->sum),2) )}}
                    @endif
                </h3>

                <p>
                    @if(Auth::user()->id_status == 3)
                    Total de ventas
                    @else
                    Capital invertido que falta vender
                    @endif
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-stats-bars"></i>
            </div>
            <!--a href="#" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a-->
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>
                    @if(Auth::user()->id_status == 3)
                    {{Auth::user()->transaksis->where("state_tran",'AC')->where("tanggal",">=", date("Y-m-d")." 00:00:00")->where("tanggal","<=", date("Y-m-d")." 23:59:59")->count()}}
                    @else
                    {{(\DB::select('SELECT count(u.id) as cant FROM transaksi u where u.state_tran = "AC" and tanggal >= "'.date("Y-m-d").' 00:00:00" and tanggal <= "'.date("Y-m-d").' 23:59:59"'))[0]->cant}}
                    @endif
                </h3>

                <p>Nro. Órdenes del día</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <!--a href="#" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a-->
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>
                    <sup style="font-size: 20px">S/.</sup>
                    @if(Auth::user()->id_status == 3)
                    {{ sprintf("%01.2f" , round((Auth::user()->transaksis->where("state_tran",'AC')->where("tanggal",">=", date("Y-m-d")." 00:00:00")->where("tanggal","<=", date("Y-m-d")." 23:59:59")->sum("bayar"))-(Auth::user()->transaksis->where("state_tran",'AC')->where("tanggal",">=", date("Y-m-d")." 00:00:00")->where("tanggal","<=", date("Y-m-d")." 23:59:59")->sum("kembalian")),2) )}}
                    @else
                    {{sprintf("%01.2f" , round(((\DB::select('SELECT sum(u.bayar - u.kembalian) as sum FROM transaksi u WHERE  u.state_tran = "AC" and tanggal >= "'.date("Y-m-d").' 00:00:00" and tanggal <= "'.date("Y-m-d").' 23:59:59"'))[0]->sum),2) )}}
                    @endif
                </h3>

                <p>Ventas del día</p>
            </div>
            <div class="icon">
                <i class="ion ion-person-add"></i>
            </div>
            <!--a href="#" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a-->
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>
                    @if(Auth::user()->id_status == 3)
                    {{(\DB::select('SELECT count(u.id) as sum FROM transaksi u where state_tran="IN" and tanggal >= "'.date("Y-m-d").' 00:00:00" and tanggal <= "'.date("Y-m-d").' 23:59:59" and id_user='.Auth::user()->id))[0]->sum}}
                    
                    @else
                    {{(\DB::select('SELECT count(u.id) as sum FROM transaksi u where state_tran="IN" and tanggal >= "'.date("Y-m-d").' 00:00:00" and tanggal <= "'.date("Y-m-d").' 23:59:59"'))[0]->sum}}
                    @endif
                </h3>

                <p>Órdenes anuladas del día</p>
            </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
            </div>
            <!--a href="#" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a-->
        </div>
    </div>
    <!-- ./col -->
</div>