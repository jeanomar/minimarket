@extends('layouts.template')

@push('css')
{{ Html::style('plugins/datatables/dataTables.bootstrap.css') }}
{{ Html::style('css/bootstrap-select.min.css') }}
{{ Html::style('plugins/select-ajax/css/ajax-bootstrap-select.css') }}
<link href="{{ asset('css/css/imgupload.css') }}" rel="stylesheet" />
@endpush

@push('js')
{{ Html::script('plugins/datatables/jquery.dataTables.min.js') }}
{{ Html::script('plugins/datatables/dataTables.bootstrap.min.js') }}
<script src="{{ asset('plugins\bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins\jquery-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
{{ Html::script('plugins/select-ajax/js/ajax-bootstrap-select.es-ES.min.js') }}
{{ Html::script('js/pages/product.js') }}
<script>
    /************************************************/
    if ($('.file-upload-image').attr('src') != "") {
          $('.image-upload-wrap').hide();

          $('.file-upload-content').show();

    }

    function readURL(input) {
        if (input.files[0].size/1024 > 2000) {
            removeUpload();
            alert("Solo imagenes menores a 2 Mb.");
            $('.image-title').html('Error');
        }
        else if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function(e) {
              $('.image-upload-wrap').hide();

              $('.file-upload-image').attr('src', e.target.result);
              $('.file-upload-content').show();

              $('.image-title').html(input.files[0].name);
            };
            
            fileimg = input.files[0];

            reader.readAsDataURL(input.files[0]);

        } else {
            removeUpload();
        }
    }

    function removeUpload() {
        $('.file-upload-input').replaceWith($('.file-upload-input').clone());
        $('.file-upload-content').hide();
        $('.image-upload-wrap').show();
    }
    $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });
        $('.image-upload-wrap').bind('dragleave', function () {
        $('.image-upload-wrap').removeClass('image-dropping');
    });
</script>

@endpush

@section('title', 'Productos')

@section('content-header')
    <h1>
        Listado de Productos
        <small> </small>
    </h1>
@endsection

@section('content')
    <section class="content">
        {{ csrf_field() }}

        <div class="box box-widget" id="products">
            <div class="box-header">
                @if(Auth::user()->id_status!=3)
                <button class="btn btn-flat bg-aqua" id="btn-tambah" title="Nuevo registro"><i class="fa fa-plus"></i></button>
                @endif
            </div>

            <div class="box-body">
                <table class="table table-bordered" id="table">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nombre</th>
                        <th>P. U. Venta S/.</th>
                        <th>Stock</th>
                        <th>Unidad</th>
                        <th>Categoría</th>
                        <th>Stock Min.</th>
                        <th>Días antes Venc.</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
    
@endsection