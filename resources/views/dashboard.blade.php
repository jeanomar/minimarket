@extends('layouts.template')

@push('css')
	{{-- Html::style('css/css.js') --}}

	{{ Html::style('plugins/jqplot/jquery.jqplot.css') }}
@endpush

@push('js')
	{{-- Html::script('js/pages/contoh.js') --}}
	<script>
		$(function() {

	    	$("button[name='btn_new_alert']").click( function(e) {
	    		$.ajax({
	                type     : 'POST',
	                url      : "/create_notif",
	                dataType : 'json',
	                cache    : false,
	                data: {
	                    '_token'         : $("input[name='_token']").val(),
	                    'user_to'        : $('select[name="user_to"]').val(),
	                    'nota'			 : $("textarea[name='nota']").val(),
	                },
	                success: function(data){
	                	$('#modal-kode-notif').modal('toggle');
	                	$('select[name="user_to"]').val("");
	                    $("textarea[name='nota']").val("");
	                }
	            });

	    	});
		})
        
	</script>
	{{ Html::script('plugins/jqplot/excanvas.js') }}

	{{ Html::script('plugins/jqplot/jquery.jqplot.js') }}
	{{ Html::script('plugins/jqplot/jqplot.highlighter.js') }}
	{{ Html::script('plugins/jqplot/jqplot.cursor.js') }}
	{{ Html::script('plugins/jqplot/jqplot.dateAxisRenderer.js') }}
	{{ Html::script('plugins/jqplot/jqplot.pieRenderer.js') }}
	{{ Html::script('plugins/jqplot/jqplot.logAxisRenderer.js') }}
	{{ Html::script('plugins/jqplot/jqplot.barRenderer.js') }}
	<script>
		$(document).ready(function(){
			var trans = {!! json_encode($transaction)!!};
			var ordenes = {!! json_encode($orders)!!};
			var compras = {!! json_encode($compras)!!};

            var line1=[];
            var line2=[];
            var line3=[];

            for(i=0; i<=trans.length-1; i++){
            	line1.push([trans[i].tanggals, parseFloat(trans[i].price)]);
            }
            for(i=0; i<=ordenes.length-1; i++){
            	line2.push([ordenes[i].tanggals, parseFloat(ordenes[i].cant)]);
            }
            for(i=0; i<=compras.length-1; i++){
            	line3.push([compras[i].tanggals, parseFloat(compras[i].price)]);
            }


		  	var plot1 = $.jqplot('chart1', [line1, line2, line3], {
		      	axes:{
		        	xaxis:{
		          		renderer:$.jqplot.DateAxisRenderer,
		          		tickOptions:{
		            		formatString:'%b&nbsp;%#d'
		          		} //,
		          		//tickInterval: "1 day",
		        	},
		        	yaxis:{
		          		tickOptions:{
		            		formatString:'S/.%.2f'
		        	    }
		        	},
		        	y2axis:{
		          		tickOptions:{
		            		formatString:'%.0f'
		        	    }
		        	},
		        	y3axis:{
		          		tickOptions:{
		            		formatString:'S/.%.2f'
		        	    }
		        	}
		      	},
		      	highlighter: {
		        	show: true,
		        	sizeAdjust: 7.5
		      	},
		      	cursor: {
		        	show: false
		      	},
		      	legend:{ 
				    renderer: $.jqplot.EnhancedLegendRenderer, 
				    show: true, 
				    rendererOptions: { 
				     numberRows: 1 
			    	}

				},
				series:[
  					{ label: 'Ventas' },
  					{ label: 'Órdenes', yaxis:'y2axis' },
  					{ label: 'Compras', yaxis:'y3axis' }
				]
		  	});
		});
	</script>
@endpush
<?php
$title = config("app.name")
?>
@section('title', $title)

@section('content-header')
	<h1>
		{{config("app.name")}}
		<small>Tablero</small>
	</h1>
@endsection

@section('content')
	<section class="content">
		{{ csrf_field() }}
		@include("fixed.controlpanel")

		<div class="box box-default color-palette-box">
	        <div class="box-header with-border">
	          <h3 class="box-title"><i class="fa fa-tag"></i> Gráfica </h3>
	        </div>
	        <div class="box-body">

	        	<!-- ./col -->
			    <div class="col-lg-12 col-xs-12" style="height: 360px;" >
			        <!-- small box -->
			        <div id="chart1" style="height:350px;width:100%; "></div> 
			    </div>
	        </div>
	        <!-- /.box-body -->
		</div>
		
		<div class="row" style="margin-left: 1px;margin-right: 1px;">
        	<label class="pull-right">
        		<a id="addClass" class="btn btn-primary"><span class="fa fa-bell-o"></span> Ver notificaciones </a>
        	</label>
        	@if(Auth::user()->id_status != 3)
	        	<div class="pull-right" style="margin-right: 5px;">
	        		<span title="Nueva notificación" class="btn btn-info" id="contoh" data-toggle="modal" data-target="#modal-kode-notif"><i class="fa fa-plus"></i></span>
	        	</div>
        	@endif
        </div>
        
		@if(Auth::user()->id_status != 3)
		<div class="modal modal-default form-horizontal" id="modal-kode-notif" data-backdrop="static" data-keyboard="false">
		    <div class="modal-dialog modal-md">
		        <div class="modal-content">
		            <div class="modal-header">
		                <span type="button" class="close" data-dismiss="modal" aria-label="Close">
		                    <span aria-hidden="true">&times;</span></span>
		                <h4 class="modal-title">Nueva Notificación</h4>
		            </div>
		            <div class="modal-body">
		            	<div class='form-group'>
						    <label class='col-sm-3 control-label'>Para:</label>
						    <div class='col-sm-9'>
						    	<select name="user_to" id="user_to" class="form-control">
						    		<option value="" disabled selected>Seleccione un usuario</option>
						    		@foreach($users_to as $us)
						    		<option value="{{$us->id}}">{{$us->nama}}</option>
						    		@endforeach
						    	</select>
						    </div>
					    </div>
					    <div class='form-group'>
						    <label class='col-sm-3 control-label'>Mensaje</label>
						    <div class='col-sm-9'>
						    	<textarea class='form-control' id="nota" name='nota'></textarea>
						    </div>
					    </div>
		            </div>
		            <div class="modal-footer">
		            	<span class="btn btn-default pull-right" data-dismiss="modal" style="margin-left: 10px;">Cerrar</span>
		                <button name="btn_new_alert" class="btn btn-primary pull-right">Aceptar</button>
		            </div>
		        </div>
		        <!-- /.modal-content -->
		    </div>
		    <!-- /.modal-dialog -->
		</div>
		@endif
    </section>
@endsection