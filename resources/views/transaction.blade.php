@extends('layouts.template')

@push('css')
{{ Html::style('plugins/jQueryUI/jquery-ui.css') }}
{{ Html::style('plugins/datatables/dataTables.bootstrap.css') }}
{{ Html::style('css/bootstrap-select.min.css') }}
<style>
   // Some variables
$color-grey-darker: #444;
$color-grey-light: #ccc;
$color-grey-lighter: #e2e2e2;

.radio-button {
    display: none;
}

.radio-button-click-target {
    cursor: pointer;
    display: inline-block;
    font-size: 16px;
    line-height: 1.5;
    padding: 0px 60px 0px 10px;
    position: relative;

    &:hover,
    &:focus {
        .radio-button-circle:before {
            border-color: $color-grey-darker;
        }
    }

    &:active .radio-button-circle:before {
        transform: scale(1.1);
    }
}

.radio-button-circle {
    border-radius: 50%;
    cursor: pointer;
    display: block;
    height: 8px;
    left: 4px;
    position: absolute;
    top: 20px;
    transition: background-color .1s ease-out;
    vertical-align: middle;
    width: 8px;

    &:before {
        border: 3px solid $color-grey-light;
        border-radius: 50%;
        content: '';
        display: inline-block;
        height: 20px;
        position: absolute;
        top: -6px;
        transition: border-color .1s ease-out,
                    transform .1s ease-out;
        left: -6px;
        width: 20px;
    }
}

.radio-button:checked + .radio-button-click-target .radio-button-circle {
    background-color: $color-grey-darker;
}

.radio-button:disabled + .radio-button-click-target {
    cursor: not-allowed;

    .radio-button-circle:before {
        border-color: $color-grey-lighter;
    }

    &:active .radio-button-circle:before {
        transform: none;
    }
}

}

#swatches {
    margin: 20px;
}

#hoverThumb {
    height: 100px;
    width: 100px;
    position:absolute;
    min-height: 1px;
    
    -webkit-border-radius: 10px;
       -moz-border-radius: 10px;
            border-radius: 10px;
    
    -webkit-box-shadow: 0px 0px 15px rgba(0,0,0,0.5);
       -moz-box-shadow: 0px 0px 15px rgba(0,0,0,0.5);
        -ms-box-shadow: 0px 0px 15px rgba(0,0,0,0.5);
         -o-box-shadow: 0px 0px 15px rgba(0,0,0,0.5);
            box-shadow: 0px 0px 15px rgba(0,0,0,0.5);
            z-index: 100000px;
    
    
}
</style>

@endpush

@push('js')
{{ Html::script('plugins/jQueryUI/jquery-ui.js') }}
{{ Html::script('plugins/datatables/jquery.dataTables.min.js') }}
{{ Html::script('plugins/datatables/dataTables.bootstrap.min.js') }}
<script src="{{ asset('plugins\jquery-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('plugins\bootstrap-select/bootstrap-select.min.js') }}"></script>
{{ Html::script('plugins/select-ajax/js/ajax-bootstrap-select.es-ES.min.js') }}
{{Html::script('js/pages/transaction.js')}}
@endpush

@section('title', 'Transacciones')

@section('content-header')
@endsection

@section('content')
    <section class="content" style="padding-top: 0px;padding-bottom: 0px;">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-sm-6">
                <div class="box  box-widget">
                    <div class="box-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nro.Transacción</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="kode_transaksi" type="text" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Fecha</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="tanggal_transaksi" type="text" readonly>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="box box-widget" id="info">
                    <div class="box-body bg-yellow">
                        <center>
                            <h1 class="box-title" style="margin: 30px auto" id="tagihan">S/. 0.00</h1>
                        </center>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="box box-widget" style="margin-bottom: 10px;">
                    <div class="box-body">
                        <div class="col-sm-3">
                            <label>Código</label>
                            <div class="input-group">
                                <input class="form-control" type="text" tabindex="1" name="kode">
								<span class="input-group-btn">
									<button title="Buscar producto" type="button" class="btn btn-info btn-flat" onclick="_caribarang()"><i class="fa fa-search"></i></button>
								</span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label>Producto</label>
                            <div class="input-group">
                                <input type="text" name="nama_item" class="form-control" readonly>
                                <span class="input-group-btn">
                                    <div id="swatches" class="btn btn-default" style="z-index: initial;">
                                        <img id="my_image" src="img/default.png" height="19" width="19">
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group" style="margin-bottom: 0px;">
                                <label>Precio unitario</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        S/.
                                    </div>
                                    <input class="form-control" name="harga_item" type="text" readonly style="z-index: initial;">
                                </div>
                                <label id="discount" class="pull-right label-primary" style="font-size: 11px;"></label>
                                <input type="hidden" id="discountv">
                                <input type="hidden" id="pmid">
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label style="margin-bottom: 0px;">Cantidad (<label name="unit">UND</label>)</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <button title="Disminuir" class="btn btn-default" type="button" onclick="minusqty()"><i class="fa fa-minus"></i> </button>
                                    </span>
                                    <input class="form-control" value="1" name="qty_item" tabindex="2">
                                    <span class="input-group-btn">
                                        <button title="Aumentar" class="btn btn-default" type="button" onclick="plusqty()"><i class="fa fa-plus"></i> </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Precio Total</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        S/.
                                    </div>
                                    <input class="form-control" type="text" name="total_harga_item" readonly>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="pull-left">
                                <button title="Agregar al detalle" type="button" class="btn btn-success" onclick="addcart()" id="btnaddcart"  disabled tabindex="3">
                                    _<i class="fa fa-cart-plus"></i>_
                                </button>
                                <button title="Limpiar búsqueda" type="button" class="btn btn-danger" onclick="resetform()">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                            <div class="pull-right">
                                <button title="Resetear venta" type="button" class="btn btn-success" onclick="resetcart()"><i class="fa fa-refresh"></i></button>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>
                    -->
                </div>
            </div>

            <div class="col-sm-12 section-table">
                <div class="box box-widget">
                    <div class="box-body no-padding">
                        <div  class="col-sm-12">
                            <table class="table table-striped table-bordered" id="table-cart">
                                <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                    <th>Precio unitario</th>
                                    <th>Total</th>
                                    <th>Opción</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!--
                        <div class="overlay">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        -->
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 pull-left">
                <div class="box box-widget">
                    <div class="box-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="no_transaksi" class="col-sm-5 control-label" style="padding-right: 0px;">
                                    <h4>
                                        <span class="pull-left"><i class="fa fa-id-badge"></i> Cliente (opcional) </span>
                                        <i class="fa fa-plus-square pull-right" style="cursor: pointer;" id="btnNewClient"></i>
                                    </h4>
                                </label>
                                <div class="col-sm-7" style="padding-top: 10px;">
                                    <select class='selectpicker form-control' data-live-search='true' name='cliente' tabindex="4">
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>  
                </div>

                <div class="box box-widget">
                    <div class="box-body">
                        <div class="form-horizontal">
                            <button class="btn btn-default col-sm-12 col-xs-12" data-toggle="modal" data-target="#modal-kode-closebox">
                                <i class="fa-2x fa fa-power-off"></i> <h4><strong>CERRAR CAJA</strong></h4>
                            </button>
                        </div>
                    </div>  
                </div>

                <div class="box box-widget">
                    <div class="box-body">
                        <div class="form-horizontal">
                            <div class="col-md-12">

                              <input type="radio" name="common-radio-name" id="radio-1" class="radio-button" value="0" checked />
                              <label for="radio-1" class="radio-button-click-target">
                                <span class="radio-button-circle"></span>Imprimir ticket
                              </label>

                              <input type="radio" name="common-radio-name" id="radio-2" class="radio-button" value="1" />
                              <label for="radio-2" class="radio-button-click-target">
                                <span class="radio-button-circle"></span>Elegir
                              </label>

                              <input type="radio" name="common-radio-name" id="radio-3" class="radio-button" value="2" />
                              <label for="radio-3" class="radio-button-click-target">
                                <span class="radio-button-circle"></span>No imprimir
                              </label>
                              
                            </div>
                        </div>
                    </div>  
                </div>
            </div>

            <div class="col-sm-6 col-xs-12 pull-right">
                <div class="box box-widget">
                    <div class="box-body">
                        <div class="col-sm-12">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="no_transaksi" class="col-sm-3 control-label">Monto a pagar</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <div class="input-group-addon">S/.</div>
                                            <input class="form-control" type="text" readonly name="tagihan">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <div class="input-group-addon">+/-</div>
                                            <input class="form-control" type="text" readonly name="redondeo">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <div class="input-group-addon hidden-md hidden-lg hidden-sm">S/.</div>
                                            <input class="form-control" type="text" readonly name="final">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="no_transaksi" class="col-sm-3 control-label">Pago con</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-addon">S/.</div>
                                            <input class="form-control" type="text" tabindex="5" name="bayar">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="no_transaksi" class="col-sm-3 control-label">Vuelto</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-addon">S/.</div>
                                            <input class="form-control" type="text" readonly name="kembalian">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="box-footer with-border">
                        <div class="col-sm-12">
                            <button title="Pago en efectivo" class="btn btn-info btn-flat col-sm-12 col-xs-12" id="btnbayar" onclick="bayarTunai()" tabindex="5">
                                <i class="fa-5x fa fa-money"></i>
                            </button>
                            <!--button title="Pago con tarjeta" class="btn btn-success pull-right" id="btnpayment" onclick="bayarRfid()">
                                <i class="fa-3x fa fa-credit-card"></i>
                            </button-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('modal.transaction')
@endsection