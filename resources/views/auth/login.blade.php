@extends('layouts.auth')

@push('css')
{{-- Html::style('css/css.js') --}}
@endpush

@push('js')
<script src="{{ asset('js/vue/sesion.js') }}"></script>
@endpush

@section('title', 'Login '.config('app.name'))

@section('content-header')
    <h1>
        {{config("app.name")}}
    </h1>
@endsection

@section('content')
    <div class="login-box">
        <div class="login-logo" style="background: #FFFFFF;margin-bottom: 0px;">
            <div class="col-md-12"><img src="{{asset(config('app.logo'))}}" alt=""></div>
            <a href=""><b>Login {{config('app.name')}}</b></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body" id="sesion">

                {{csrf_field()}}
                <div class="form-group has-feedback {{ $errors->has('username') ? ' has-error' : '' }}">
                    <input type="text" class="form-control" placeholder="Usuario" name="username" {{old('username')}}>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input v-on:keyup="enter($event)" type="password" class="form-control" placeholder="Contraseña" name="password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember"> Recordar Contraseña
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <button @click="login()" class="btn btn-primary btn-block btn-flat" id="sign">Iniciar Sesión</button>
                    </div>
                </div>

        </div>
    </div>
@endsection