@extends('layouts.template')

@push('css')
{{ Html::style('plugins/jQueryUI/jquery-ui.css') }}
{{ Html::style('plugins/datatables/dataTables.bootstrap.css') }}

@endpush

@push('js')
{{ Html::script('plugins/jQueryUI/jquery-ui.js') }}

@endpush

@section('title', 'Transacciones')

@section('content-header')
@endsection

@section('content')
    <section class="content">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-sm-6">
                <div class="box  box-widget">
                    <div class="box-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nombres:</label>
                                <div class="col-sm-9">
                                    <label class="form-control">{{Auth::user()->nama}}</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Fecha:</label>
                                <div class="col-sm-9">
                                    <label class="form-control">{{date("Y-m-d")}}</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Estado:</label>
                                <div class="col-sm-9">
                                    
                                    @if($state == "open")
                                        <label class="form-control label-success">Abrir caja para iniciar ventas</label>
                                    @else
                                        <label class="form-control label-danger">Caja cerrada</label>
                                    @endif

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 pull-right">
                <div class="box box-widget">
                    <div class="box-body">
                        <div class="col-sm-12">
                            @if($state == "open" )
                            {!! Form::open(['method'=>'POST', 'route'=>'openbox']) !!}
                                <button class="btn btn-info btn-flat col-md-12">
                                    <i class="fa-3x fa fa-shopping-cart"></i> <h3>Abrir Caja</h3>
                                </button>
                            {!!Form::close() !!}
                            @else
                            {!! Form::open(['method'=>'POST', 'route'=>'printbox']) !!}
                                <button class="btn btn-default col-md-12">
                                    <i class="fa-3x fa fa-file-text"></i> <h3>Imprimir ventas del dia</h3>
                                </button>
                            {!!Form::close() !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection