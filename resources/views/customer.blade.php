@extends('layouts.template')

@push('css')
{{ Html::style('plugins/datatables/dataTables.bootstrap.css') }}
@endpush

@push('js')
{{ Html::script('plugins/datatables/jquery.dataTables.min.js') }}
{{ Html::script('plugins/datatables/dataTables.bootstrap.min.js') }}
{{ Html::script('plugins/input-mask/jquery.inputmask.js') }}
{{ Html::script('js/pages/customer.js') }}
@endpush

@section('title', 'Clientes')

@section('content-header')
    <h1>
        Listado de Clientes
        <small> </small>
    </h1>
@endsection

@section('content')
    <section class="content">
        {{ csrf_field() }}
        <div class="box box-widget ">
            <div class="box-header">
                
            </div>
            <div class="box-body">
                <table class="table table-bordered" id="table">
                    <thead>
                    <tr>
                        <th>Nombres</th>
                        <th>DNI/RUC/CE</th>
                        <th>Sexo</th>
                        <th>Teléfono</th>
                        <th>Dirección</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection