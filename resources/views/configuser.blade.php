@extends('layouts.template')

@push('css')
{{ Html::style('plugins/datatables/dataTables.bootstrap.css') }}
{{ Html::style('css/bootstrap-select.min.css') }}
{{ Html::style('plugins/select-ajax/css/ajax-bootstrap-select.css') }}
@endpush

@push('js')
{{ Html::script('plugins/datatables/jquery.dataTables.min.js') }}
{{ Html::script('plugins/datatables/dataTables.bootstrap.min.js') }}
<script src="{{ asset('plugins\jquery-inputmask/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('plugins\bootstrap-select/bootstrap-select.min.js') }}"></script>
{{ Html::script('plugins/select-ajax/js/ajax-bootstrap-select.min.js') }}
{{ Html::script('js/pages/configuser.js') }}
@endpush

@section('title', 'Configuraciones')

@section('content-header')
    <h1>
        Configuraciones
        <small> </small>
    </h1>
@endsection

@section('content')
    <section class="content">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-6">
                <div class="box box-widget" style="background: #f5ece8;">
                    <div class="box-header">
                        <h3 class="text-center">Usuarios del sistema</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="table">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Usuario</th>
                                <th>Acceso</th>
                                <th>Estado</th>
                                <th>Acción</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        
            <div class="col-md-6">
                <div class="box box-widget" style="background: #d8dede;">
                    <div class="box-header">
                        <h3 class="text-center">Variables del Sistema</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="table2">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Valor</th>
                                <th>Creador</th>
                                <th>Estado</th>
                                <th>Acción</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="tambah-modal-query"></div>
        <div id="info-modal-query">
           
        </div>
    </section>
@endsection