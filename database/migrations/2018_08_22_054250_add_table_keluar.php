<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableKeluar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('produk_keluar', function (Blueprint $table) {
            $table->integer('transaksi_id')->nullable();
            $table->string('transaksi_kode')->nullable();
            $table->enum('state', ['AC','IN'])->default('AC');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produk_keluar', function (Blueprint $table) {
            //
        });
    }
}
