<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRepository extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repository', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('tanggal');
            $table->decimal('stok',10,3);
            $table->string('satuan',6)->nullable();
            $table->decimal("harga",10,2)->nullable();
            $table->unsignedInteger('id_produk');
            $table->unsignedInteger('id_produk_detail');
            $table->date("f_venc")->nullable();
            $table->unsignedInteger('id_supplier')->nullable();
            $table->unsignedInteger('id_user');
            $table->enum('type', ['OUTPUT','INPUT'])->default('INPUT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('repository', function (Blueprint $table) {
            //
        });
    }
}
