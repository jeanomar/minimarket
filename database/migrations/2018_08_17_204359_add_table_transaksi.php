<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaksi', function (Blueprint $table) {
            $table->enum('state_box', ['AB','CE'])->default('AB');
            $table->datetime('box_tanggal')->nullable();
            $table->integer('destroy_user_id')->unsigned()->nullable();
            $table->foreign('destroy_user_id')->references('id')->on('users');
            $table->enum('state_tran', ['AC','IN'])->default('AC');
            $table->datetime('destroy_tanggal')->nullable();
            $table->string('nota')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi', function (Blueprint $table) {
            //
        });
    }
}
