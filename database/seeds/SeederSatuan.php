<?php

use Illuminate\Database\Seeder;

class SeederSatuan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $satuan1 = new \App\ProdukSatuan();
        $satuan1->nama = 'L';
        $satuan1->keterangan = 'Litros';
        $satuan1->save();

        $satuan1 = new \App\ProdukSatuan();
        $satuan1->nama = 'KG';
        $satuan1->keterangan = "Kilogramo";
        $satuan1->save();

        $satuan1 = new \App\ProdukSatuan();
        $satuan1->nama = 'M';
        $satuan1->keterangan = "Metros";
        $satuan1->save();

        $satuan1 = new \App\ProdukSatuan();
        $satuan1->nama = 'UND';
        $satuan1->keterangan = "Unidades";
        $satuan1->save();

        $satuan1 = new \App\ProdukSatuan();
        $satuan1->nama = 'GL';
        $satuan1->keterangan = "Galones";
        $satuan1->save();


    }
}
