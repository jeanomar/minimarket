<?php

use Illuminate\Database\Seeder;

class VariablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $var = new \App\Variable();
        $var->name = 'igv';
        $var->value = '0.18';
        $var->user_id = 1;
        $var->save();
    }
}
