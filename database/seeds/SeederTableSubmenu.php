<?php

use Illuminate\Database\Seeder;
use App\Submenu;

class SeederTableSubmenu extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $menu = new \App\Menu;
        $dashboard = $menu->where('title', 'Tablero')->pluck('id')->first();
        $transaksi = $menu->where('title', 'Transacciones')->pluck('id')->first();
        $produk = $menu->where('title', 'Mantenimiento')->pluck('id')->first();
        $stok = $menu->where('title', 'Stock')->pluck('id')->first();
        $repos = $menu->where('title', 'Almacén')->pluck('id')->first();
        $pelanggan = $menu->where('title', 'Cliente')->pluck('id')->first();
        $supplier = $menu->where('title', 'Proveedor')->pluck('id')->first();
        $laporan = $menu->where('title', 'Reportes')->pluck('id')->first();
        $setting = $menu->where('title', 'Configuración')->pluck('id')->first();

        $admin = \App\Status::where('status', 'ADMIN')->first();
        $manager = \App\Status::where('status', 'GERENTE')->first();
        $kasir = \App\Status::where('status', 'CAJERO')->first();

        $submenu1 = new Submenu();
        $submenu1->title = 'Productos';
        $submenu1->icon = 'fa-briefcase';
        $submenu1->url = 'product';
        $submenu1->id_menu = $produk;
        $submenu1->save();
        $submenu1->status()->attach($admin);
        $submenu1->status()->attach($manager);
        $submenu1->status()->attach($kasir);

        $submenu2 = new Submenu();
        $submenu2->title = 'Categorias';
        $submenu2->icon = 'fa-list-alt';
        $submenu2->url = 'category';
        $submenu2->id_menu = $produk;
        $submenu2->save();
        $submenu2->status()->attach($admin);
        $submenu2->status()->attach($manager);
        $submenu2->status()->attach($kasir);

        $submenu3 = new Submenu();
        $submenu3->title = 'Unidades de medida';
        $submenu3->icon = 'fa-list';
        $submenu3->url = 'unit';
        $submenu3->id_menu = $produk;
        $submenu3->save();
        $submenu3->status()->attach($admin);
        $submenu3->status()->attach($manager);
        $submenu3->status()->attach($kasir);

        $submenu4 = new Submenu();
        $submenu4->title = 'Artículos entrantes';
        $submenu4->icon = 'fa-sign-in';
        $submenu4->url = 'stock-in';
        $submenu4->id_menu = $stok;
        $submenu4->save();
        $submenu4->status()->attach($admin);
        $submenu4->status()->attach($manager);

        $submenu5 = new Submenu();
        $submenu5->title = 'Artículos salientes';
        $submenu5->icon = 'fa-sign-out';
        $submenu5->url = 'stock-out';
        $submenu5->id_menu = $stok;
        $submenu5->save();
        $submenu5->status()->attach($admin);
        $submenu5->status()->attach($manager);

        $submenu6 = new Submenu();
        $submenu6->title = 'Ventas';
        $submenu6->icon = 'fa-file-text';
        $submenu6->url = 'sales-report';
        $submenu6->id_menu = $laporan;
        $submenu6->save();
        $submenu6->status()->attach($admin);
        $submenu6->status()->attach($manager);
        $submenu6->status()->attach($kasir);

        $submenu7 = new Submenu();
        $submenu7->title = 'Artículos entrantes';
        $submenu7->icon = 'fa-file';
        $submenu7->url = 'in-report';
        $submenu7->id_menu = $laporan;
        $submenu7->save();
        $submenu7->status()->attach($admin);
        $submenu7->status()->attach($manager);

        $submenu8 = new Submenu();
        $submenu8->title = 'Artículos salientes';
        $submenu8->icon = 'fa-file-o';
        $submenu8->url = 'out-report';
        $submenu8->id_menu = $laporan;
        $submenu8->save();
        $submenu8->status()->attach($admin);
        $submenu8->status()->attach($manager);

        $submenu9 = new Submenu();
        $submenu9->title = 'Ganancias';
        $submenu9->icon = 'fa-file-text-o';
        $submenu9->url = 'money-report';
        $submenu9->id_menu = $laporan;
        $submenu9->save();
        $submenu9->status()->attach($manager);

        $submenu10 = new Submenu();
        $submenu10->title = 'Entradas';
        $submenu10->icon = 'fa-sign-in';
        $submenu10->url = 'input';
        $submenu10->id_menu = $repos;
        $submenu10->save();
        $submenu10->status()->attach($admin);
        $submenu10->status()->attach($manager);

        $submenu11 = new Submenu();
        $submenu11->title = 'Salidas';
        $submenu11->icon = 'fa-sign-out';
        $submenu11->url = 'output';
        $submenu11->id_menu = $repos;
        $submenu11->save();
        $submenu11->status()->attach($admin);
        $submenu11->status()->attach($manager);
    }
}
