<?php

use Illuminate\Database\Seeder;

class SeederKategori extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kategori1 = new \App\ProdukKategori();
        $kategori1->nama = 'L';
        $kategori1->keterangan = "Libreria";
        $kategori1->save();

        $kategori2 = new \App\ProdukKategori();
        $kategori2->nama = 'A';
        $kategori2->keterangan = 'Abarrotes';
        $kategori2->save();

        $kategori3 = new \App\ProdukKategori();
        $kategori3->nama = 'F';
        $kategori3->keterangan = "Ferreteria";
        $kategori3->save();

    }
}
