<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SeederTableStatus::class);
        $this->call(SeederTableUsers::class);
        $this->call(SeederTableMenu::class);
        $this->call(SeederTableSubmenu::class);
        $this->call(SeederDetailProduk::class); //js thml dany sanchez 976220093 - ing sergio caceres 
        $this->call(SeederSatuan::class);
        $this->call(SeederKategori::class);
        $this->call(VariablesTableSeeder::class);
    }
}
