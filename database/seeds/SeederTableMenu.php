<?php

use Illuminate\Database\Seeder;
use App\Menu;

class SeederTableMenu extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = \App\Status::where('status', 'ADMIN')->first();
        $manager = \App\Status::where('status', 'GERENTE')->first();
        $kasir = \App\Status::where('status', 'CAJERO')->first();

        $menu1 = new Menu();
        $menu1->title = 'Tablero';
        $menu1->icon = 'fa-dashboard';
        $menu1->url = '';
        $menu1->save();
        $menu1->status()->attach($admin);
        $menu1->status()->attach($manager);
        $menu1->status()->attach($kasir);

        $menu2 = new Menu();
        $menu2->title = 'Transacciones';
        $menu2->icon = 'fa-shopping-cart';
        $menu2->url = 'transaction';
        $menu2->save();
        $menu2->status()->attach($kasir);
        //$menu2->status()->attach($admin);

        $menu3 = new Menu();
        $menu3->title = 'Mantenimiento';
        $menu3->icon = 'fa-archive';
        $menu3->url = 'product';
        $menu3->save();
        $menu3->status()->attach($admin);
        $menu3->status()->attach($manager);
        $menu3->status()->attach($kasir);

        $menu4 = new Menu();
        $menu4->title = 'Stock';
        $menu4->icon = 'fa-exchange';
        $menu4->url = 'stock';
        $menu4->save();
        $menu4->status()->attach($admin);
        $menu4->status()->attach($manager);

        $menu5 = new Menu();
        $menu5->title = 'Cliente';
        $menu5->icon = 'fa-users';
        $menu5->url = 'customer';
        $menu5->save();
        $menu5->status()->attach($admin);
        $menu5->status()->attach($manager);
        $menu5->status()->attach($kasir);

        $menu6 = new Menu();
        $menu6->title = 'Proveedor';
        $menu6->icon = 'fa-truck';
        $menu6->url = 'supplier';
        $menu6->save();
        $menu6->status()->attach($admin);
        $menu6->status()->attach($manager);

        $menu7 = new Menu();
        $menu7->title = 'Reportes';
        $menu7->icon = 'fa-file-text';
        $menu7->url = 'report';
        $menu7->save();
        $menu7->status()->attach($admin);
        $menu7->status()->attach($manager);
        $menu7->status()->attach($kasir);

        $menu8 = new Menu();
        $menu8->title = 'Configuración';
        $menu8->icon = 'fa-gears';
        $menu8->url = 'setting';
        $menu8->save();
        $menu8->status()->attach($admin);

        $menu9 = new Menu();
        $menu9->title = 'Almacén';
        $menu9->icon = 'fa-th-large';
        $menu9->url = 'repository';
        $menu9->save();
        $menu9->status()->attach($admin);
        $menu9->status()->attach($manager);

        $menu10 = new Menu();
        $menu10->title = 'Descuentos';
        $menu10->icon = 'fa-usd';
        $menu10->url = 'discount';
        $menu10->save();
        $menu10->status()->attach($admin);
        $menu10->status()->attach($manager);
    }
}
